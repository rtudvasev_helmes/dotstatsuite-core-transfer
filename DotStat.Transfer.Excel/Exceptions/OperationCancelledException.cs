﻿using System;
using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Transfer.Excel.Exceptions
{
    [ExcludeFromCodeCoverage]
    public class OperationCancelledException : DotStatException
    {
        public OperationCancelledException() : base()
        {
        }

        public OperationCancelledException(string message) : base(message)
        {
        }

        public OperationCancelledException(string message, System.Exception inner) : base(message, inner)
        {
        }
    }
}