﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotStat.Domain;
using DotStat.Transfer.Excel.Mapping;

namespace DotStat.Transfer.Excel.Reader
{
    /// <summary>
    /// Abstract base class for all SWReaders based on an IRecordIterator and a SWMapper that specifies column names for dimensions and values
    /// All the dimension column names are assumed to be the same as the name of the external dimension
    /// </summary>
    public abstract class SWRecordIteratorReader<T> : SWReaderBase<T> where T:class
    {
        protected readonly IDictionary<string, int> ColumnIndices;

        private readonly bool _AutoDisposeIterator;

        protected SWRecordIteratorReader(IRecordIterator<T> recordIterator, Dataflow ds, bool autoDisposeIterator = true) : base(ds)
        {
            RecordIterator = recordIterator;
            _AutoDisposeIterator = autoDisposeIterator;
            ColumnIndices = new Dictionary<string, int>(ds.Dimensions.Count, StringComparer.InvariantCultureIgnoreCase);
        }

        public override V8Mapper<T> Mapper => RecordIterator.Mapper;


        protected override sealed int GetColumnIndex(string columnName)
        {
            if (ColumnIndices.Count == 0)
            {
                AssignColumnIndices();
            }
            int res;
            if (ColumnIndices.TryGetValue(columnName, out res))
            {
                return res;
            }
            //throw new ReadErrorException( "The column {0} is not defined in data source {1}".F( columnName, Name ) );
            return -1;
        }

        protected virtual void AssignColumnIndices()
        {
            AssignDefaultColumnIndexesToDefault();
        }

        protected void AssignDefaultColumnIndexesToDefault()
        {
            ColumnIndices.Clear();

            for (var ii = 0; ii < Mapper.SourceCount; ii++)
            {
                var col = Mapper.SourceNames[ii];

                var idx = RecordIterator.GetFieldIndex(col);

                if (idx < 0)
                    continue;

                ColumnIndices.Add(col, idx);
            }
        }

        public IRecordIterator<T> RecordIterator { get; protected set; }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _AutoDisposeIterator)
            {
                RecordIterator.Dispose();
            }
            base.Dispose(disposing);
        }

        public override string FetchField(string fieldName)
        {
            if (!ColumnIndices.Any())
                AssignColumnIndices();

            var idx = GetColumnIndex(fieldName);
            return idx < 0 ? null : RecordIterator.GetField(idx);
        }

        protected override string GetSource()
        {
            return string.Format("Record: {0}", CurrentRecordNo);
        }
    }
}