﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using DotStat.Domain;
using DotStat.Transfer.Excel.Mapping;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Transfer.Excel.Reader
{
    public enum ErrorPolicy
    {
        [Description("Skip data point")] Skip,

        [Description("Stop transfer")] StopTransfer,
    }

    /// <summary>
    /// Implementations of this class acts a an import/export adapter for various kinds of data sources or targets
    /// As a result, each type of data source and also sink would provide its own implementation specific to a
    /// particular data format. Various implementations are already provided for exporting from database, and 
    /// importing/exporting to CSV type flat files.
    /// </summary>
    public interface ISWReader<T> : IEnumerator<T> where T:class
    {
        V8Mapper<T> Mapper { get; }

        /// <summary>
        /// The dataset which will be the basis for SWDataRecord structure
        /// </summary>
        Dataflow Dataset { get; }

        int CurrentRecordNo { get; }

        string Name { get; }
    }
}