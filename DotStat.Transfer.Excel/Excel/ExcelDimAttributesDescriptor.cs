﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Domain;
using DotStat.Transfer.Excel.Exceptions;
using DotStat.Transfer.Excel.Util;

namespace DotStat.Transfer.Excel.Excel
{
    public abstract class ExcelCellRangeDescriptor
    {
        internal readonly CellReferenceExpression TopLeft;
        internal readonly CellReferenceExpression BottomRight;

        private readonly List<MatchPattern> _worksheets;
        private readonly List<ConditionExpression> _excludedCellConditions;
        private readonly List<CellReferenceExpression> _excludedCells;
        private readonly List<AxisReferenceExpression> _excludedRows;
        private readonly List<AxisReferenceExpression> _excludedColumns;

        protected ExcelCellRangeDescriptor(
            IEnumerable<MatchPattern> worksheets,
            CellReferenceExpression topLeft,
            CellReferenceExpression bottomRight,
            IEnumerable<ConditionExpression> excludedCellConditions = null,
            IEnumerable<CellReferenceExpression> excludedCells = null,
            IEnumerable<AxisReferenceExpression> excludedRows = null,
            IEnumerable<AxisReferenceExpression> excludedColumns = null
        )
        {
            _worksheets = new List<MatchPattern>(worksheets);

            TopLeft = topLeft;
            BottomRight = bottomRight;

            _excludedCellConditions = new List<ConditionExpression>(excludedCellConditions ?? Enumerable.Empty<ConditionExpression>());
            _excludedCells = new List<CellReferenceExpression>(excludedCells ?? Enumerable.Empty<CellReferenceExpression>());
            _excludedRows = new List<AxisReferenceExpression>(excludedRows ?? Enumerable.Empty<AxisReferenceExpression>());
            _excludedColumns = new List<AxisReferenceExpression>(excludedColumns ?? Enumerable.Empty<AxisReferenceExpression>());
        }

        internal IEnumerable<MatchPattern> Worksheets => _worksheets;
        internal IEnumerable<CellReferenceExpression> ExcludedCells => _excludedCells;
        internal IEnumerable<AxisReferenceExpression> ExcludedRows => _excludedRows;
        internal IEnumerable<AxisReferenceExpression> ExcludedColumns => _excludedColumns;
        internal IEnumerable<ConditionExpression> ExcludeCellConditions => _excludedCellConditions;
        protected IEnumerable<string> BuildIncludedWorkSheetList(IExcelDataSource dataSource)
        {
            var res = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
            foreach (var wsp in _worksheets)
            {
                if (wsp.IsRegex)
                {
                    try
                    {
                        var regex = new Regex(wsp.Pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);
                        foreach (var ws in dataSource.Worksheets)
                        {
                            if (regex.IsMatch(ws))
                            {
                                res.Add(ws);
                            }
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        //problem with regex?
                        throw new UnrecoverableAppException(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelWorksheetRegex),
                            ex.Message,
                            wsp.Pattern,
                            wsp.Source)
                        );
                    }
                }
                //else if (wsp.IsCondition)
                //{
                //    try
                //    {
                //        var func = DynamicExpression.ParseLambda<CellReferenceExpression, bool>(wsp.Pattern).Compile();
                //        foreach (var worksheet in dataSource.Worksheets)
                //        {
                //            dataSource.CurrentWorksheet = worksheet;
                //            var it = new ExcelCellObservationIterator(this, dataSource, worksheet);
                //            var cell = new CellReferenceExpression("A1", null) { Owner = it };
                //            try
                //            {
                //                if (func(cell))
                //                {
                //                    res.Add(worksheet);
                //                }
                //            }
                //            catch (ArgumentException)
                //            {
                //                //swallow argument exceptions as evaluating expression on unwanted sheet might generate them
                //                // ReSharper disable once CatchAllClause
                //            }
                //        }
                //    }
                //    catch (ArgumentException)
                //    {
                //        //swallow argument exceptions as evaluating expression on unwanted sheet might generate them
                //        // ReSharper disable once CatchAllClause
                //    }
                //    catch (System.Exception ex)
                //    {
                //        throw new UnrecoverableAppException(
                //            "There was an error parsing worksheet condition: {0}, Expression: {1}, Source: {2}".F(
                //                ex.Message, wsp.Pattern, wsp.Source));
                //    }
                //}
                //else if (wsp.IsExpression)
                //{
                //    try
                //    {
                //        var stringExpr = new StringExpression(wsp.Pattern, wsp.Source)
                //        {
                //            Owner = ExcelCellObservationIterator.GetDummyInstance(this, Name)
                //        };
                //        res.UnionWith(stringExpr.Results);
                //        // ReSharper disable once CatchAllClause
                //    }
                //    catch (System.Exception ex)
                //    {
                //        throw new UnrecoverableAppException(
                //            "There was an error parsing worksheet expression: {0}, Expression: {1}, Source: {2}".F(
                //                ex.Message,
                //                wsp.Pattern,
                //                wsp.Source));
                //    }
                //}
                else
                {
                    res.Add(wsp.Pattern);
                }
                res.IntersectWith(dataSource.Worksheets);
            }

            //if( res.Count == 0 ) {
            //    throw new ApplicationArgumentException
            //            ( "The excel data source {0} have no worksheets matching the worksheet criteria set in mapping description {1}".F
            //                      ( dataSource.Name, Name ) );
            //}
            return res;
        }
    }

    public class ExcelDimAttributesDescriptor : ExcelCellRangeDescriptor
    {
        public readonly SdmxArtifactCellReferenceExpression AttributeExpression;
        private readonly List<SdmxArtifactCellReferenceExpression> _dimensionExpressions;

        public ExcelDimAttributesDescriptor(
            IEnumerable<MatchPattern> worksheets,
            CellReferenceExpression topLeft,
            CellReferenceExpression bottomRight,
            SdmxArtifactCellReferenceExpression attributeExpression,
            IEnumerable<SdmxArtifactCellReferenceExpression> dimensionExpressions
        ) 
            : base(worksheets, topLeft, bottomRight)
        {
            AttributeExpression = attributeExpression;
            _dimensionExpressions = new List<SdmxArtifactCellReferenceExpression>(dimensionExpressions);
        }

        internal IEnumerable<SdmxArtifactCellReferenceExpression> DimensionExpressions => _dimensionExpressions;

        public IEnumerable<ExcelCellDimensionAttrIterator> BuildIterators(IExcelDataSource dataSource, Dataflow dataflow)
        {
            if (dataSource == null)
            {
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelDataSourceNotSet)
                );
            }

            return BuildIncludedWorkSheetList(dataSource).Select(ws => new ExcelCellDimensionAttrIterator(this, dataflow, dataSource, ws));
        }
    }
}
