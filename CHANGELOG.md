# HISTORY

## v11.0.0

### Database compatibility
- StructureDB v6.19
- DataDB v7.0.0
- CommonDB v3.8

### Issues
- [#91](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/91) Fix multiple contradictory values error at dataset attributes
- [#127](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/127) Introduced data DELETE operation
- [#251](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/251) References updated to match with NSI WS v8.12.0
- [#281](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/281) References updated to match with NSI WS v8.12.1
- [#285](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/285) Allow views to return higher level attribute values when there are no observation level component rows
- [#397](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/397) Implemented MERGE action for CSV v2 data imports
- [#402](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/402) Improved detection of data or metadata content of a CSV v2 file
- [#409](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/409) Fixed transaction id in time-out log message
- [#410](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/410) No fatal error is logged at init/allMappingsets method in case of no DSDs or no dataflows in data space
- [#416](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/416) Fix bug in case there are no dataset attribute values to modify
- [#420](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/420) Fix regression issues in data transfer between data spaces
- [#424](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/424) Fix issue when deleting specific years
- [#425](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/425) Fixed import of first dataset level metadata attribute


## v10.0.5

### Database compatibility
- StructureDB v6.19
- DataDB v6.2.1
- CommonDB v3.8

### Issues
- [#406](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/406) Initialize codeTranslator cache at InitDataDbObjectsOfDataflow method
- [#407](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/407) Add Message-Id header to SMTP request
- [#387](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/387) Add optional SMTP HFrom header configuration
- [#90](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/90) Minor database health check improvements


## v10.0.4

### Database compatibility
- StructureDB v6.19
- DataDB v6.2.1
- CommonDB v3.8

### Issues
- [#403](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/403) Fix creation of actual-CC


## v10.0.3

### Database compatibility
- StructureDB v6.19
- DataDB v6.2.1
- CommonDB v3.8

### Issues
- [#400](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/400) Repair permission test in CleanUpOrphanedCodelists


## v10.0.2

### Database compatibility
- StructureDB v6.19
- DataDB v6.2.1
- CommonDB v3.8

### Issues
- [#6](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-config/-/issues/6) Enhance 'DuplicatedRowsInStagingTable' error message
- [#378](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/378) Correct/complement log entry for dsd-cleanup
- [#379](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/379) Fix mail send
- [#382](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/382) Cleanup of orphaned codelists
- [#383](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/383) Fix "already added" in MappingStoreDataAccess, Add missing application name in structdbconnetionString
- [#386](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/386) Avoid re-accessing the same column


## v10.0.1

### Database compatibility
- StructureDB v6.19
- DataDB v6.2.1
- CommonDB v3.8

### Issues
- Disabled resource auto detection of Google logger
- [#380](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/380) Linked application cancellationToken and controller (client) cancellationToken


## v10.0.0

### Database compatibility
- StructureDB v6.19
- DataDB v6.2.1
- CommonDB v3.8

### Issues
- [#5](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/issues/5) Added gitlab dast scan
- [#29](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/issues/29) Added nuget dependency scanning to CI pipeline
- [#86](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/86) The method cleanup/dsd deletes the mapping sets and actual content constraint of the dataflows
- [#87](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/87) Added connection close fix and isolation level fix
- [#245](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/245) Mappingset cleanup transaction is executed in dataflow scope
- [#251](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/251) ESTAT references updated to v8.11.0
- [#267](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/267) Added log4net google appender
- [#277](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/277) Add new logs for import steps and add batchNumber to logs for staging data and metadata
- [#285](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/285) Always set the Actual CC validFrom date
- [#291](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/291) Introduced cancellation tokens and readonly db connections
- [#317](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/317) Fix for performance degradation of dataflow view when adding a value for a dataset-level attribute
- [#324](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/324) Upgraded from .NET Standard 2.1/.NET Corea 3.1 to .NET 6
- [#341](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/341) Added option to pass auth token to httpClient when importing from URL
- [#345](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/345) Fix of missing Swagger descriptions
- [#351](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/351) Import metadata from url
- [#353](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/353) Requeue requests when there is ongoing transactions for the same DSD
- [#361](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/361) Initialize dsd object of dataflows with data management properties
- [#362](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/362) Updated msd cleanup
- [#363](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/363) Fix tryNewTransaction when first request for a dsd is initdataflow
- [#365](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/365) Fix of null reference error at /cleanup/orphans method
- [#369](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/369) Fix of dataset attribute import
- [#373](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/373) Set QUEUED_DATETIME value to EXECUTION_START for transaction rows existed in db before the addition of the field QUEUED_DATETIME
- Added queue details to health check


## v9.1.2

### Database compatibility
- StructureDB v6.19
- DataDB v6.1
- CommonDB v3.8 

### Issues
- [#269](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/269) Correction of some transfer messages
- [#350](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/350) Change mappingstoredataaccess to scoped add memory cache


## v9.1.1

### Database compatibility
- StructureDB v6.19
- DataDB v6.1
- CommonDB v3.8 

### Issues
- [#342](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/342) Fix of queued transactions blocking all other transactions


## v9.1.0

### Database compatibility
- StructureDB v6.19
- DataDB v6.1
- CommonDB v3.8 

### Issues
- [#278](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/278) Mark transactions as aborted or closed 
- [#319](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/319) Import from URL made async 
- [#320](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/320) Bug fix formatting of the email subject and summary 
- [#114](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/114) Support for NaN as observation 
- [#304](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/304) Manage unhandled exceptions #304
- [#331](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/331) fix email wrong action type in email 
- [#147](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/147) add servicebus functionality 
- [#340](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/340) Swagger fix 
- [#182](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/182) Fix of dataset column mapping issue 


## v9.0.1

### Database compatibility
- StructureDB v6.19
- DataDB v6.0
- CommonDB v3.8 

### Issues
- [#321](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/321) Fix for issue of data transactions failing with dataflows supporting ref.metadata


## v9.0.0

### Database compatibility
- StructureDB v6.19
- DataDB v6.0
- CommonDB v3.8 

### Issues
- [#310](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/310) block all transactions when cleanup is in progress, Set v2 of the rest api as default
- [#316](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/316) Pretify error msg when data import from sdmx url is not accessible 
- [#315](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/315) Fix optional parameters 
- [#82](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/82) Fix metadataimport dsd with SUPPORT_DATETIME annotation
- [#309](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/309) Add missing transfer-service documentation metadata api parameters
- [#306](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/306) Fix ACC validity update at metadata imports 
- [#164](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/164) Added missing registrations related to MAAPI.NET v8.9.2 changes 
- [#234](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/234) References updated to NSI v8.9.2 
- [#130](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/130) Documentation updated 
- [#603](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-explorer/-/issues/603) Dataset Attribute parameter passed as NVarchar
- [#130](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/130) Improvements to firstNObservations and lastNObservations queries 
- [#302](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/302) Enhanced log message for init/dataflow method 
- [#305](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/305) fix Hierarchical Referential Metadata Attributes are not Imported 
- [#224](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/224) References updated to match Eurostat NSI v8.9.1 
- [#296](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/296) Correct the transfer log messages for Referential Metadata transactions
- [#82](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/82) Fix MetadataDataFlow view period_start and period_end add datetime support 
- [#293](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/287) Complete swagger documentation 
- [#293](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/293) Add a proper error description when uploading metadata file to a structure without annotation link to MSD
- [#296](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/296) Correct the transfer log messages for Referential Metadata transactions
- [#176](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/176) Add source dataspace and data source fields to transaction logs 
- [#295](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/295) Remove ORDER BY for metadata Mappingsets 
- [#81](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/81) eurostat packages update to 8.9.0
- [#80](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/80) Metadata storage implementation 


## v8.2.0 

### Database compatibility
- StructureDB v6.17
- DataDB v5.6
- CommonDB v3.8 

### Issues
- [#184](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/184) Improved management of time pre formatted entries and deletion of mapping set relates objects from database 
- [#284](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/284) Update creation time of zip entry on extract
- [#228](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/228) DataAccess NuGet references amended 
- [#230](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/230) References updated to NSI v8.8.0 
- [#280](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/280) Analyse issue "Expecting the instance to be stored in singleton scope, but...


## v8.1.0 

### Database compatibility
- StructureDB v6.17
- DataDB v5.6
- CommonDB v3.7 

### Issues
- [#39](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/39) Readme appended with description of changing authentication token claims mapping
- [#174](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/174) References updated to match Eurostat NSI v8.7.1
- [#189](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/189) Manage Time in group attributes - part 2
- [#275](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/275) Fix for zip file extraction not cleaned from temp folder and "filesCount" added to health page

## v8.0.1 

### Database compatibility
- StructureDB v6.16
- DataDB v5.6
- CommonDB v3.7 

### Issues
- [#76](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/76) Errors found in the AllowMissingComponents SQL migration script for the recreation of the DSD/DF views
- [#77](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/77) Errors found in the ChangeFactTableIndexes SQL migration script for the recreation of the Fact table indexes


## v8.0.0 

### Database compatibility
- StructureDB v6.16
- DataDB v5.6
- CommonDB v3.7 

### Issues
- [#33](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/33) Added TokenUrl param to auth config
- [#75](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/75) Change primary key from PERIOD_SDMX to PERIOD_START and PERIOD_END, fix of semantic error displayed when time period is invalid
- [#113](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/113) Support REPORTING_YEAR_START_DAY attribute, Time range format, date + time format, fixed NumberStyles used at float and double, usage of SUPPORT_DATETIME annotation added
- [#161](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/161) References updated to match Eurostat NSI v8.5.0
- [#178](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/178) Stream import files
- [#210](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/210) New date format applied on PIT release and restoration dates
- [#212](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/212) Added missing Primary measure representation warning, added logs to returned response for init/dataflow method
- [#214](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/214) Fix of codelist item ids not in consecutive order issue
- [#215](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/215) Allow non reported components
- [#223](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/223) Changed localization text for bulk copy notifications and improved merge performance with full details
- [#226](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/226) Added number of obs to pre-generated actual content constraints
- [#231](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/231) Fill order_by information for mappingsets, recreate DSD and DataFlow views include SID column
- [#236](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/236) Fixes for file validations
- [#237](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/237) Updated mapping set creation function to use new TIME_PRE_FORMATED mapping
- [#242](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/242) New ApplyEmptyDataDbConnectionStringInMSDB configuration parameter added
- [#245](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/245) Fix of column mapping during data transfer; fix missing attributes in destination during transfer
- [#251](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/251) Corrected the creation of staging table, use fact table info to create time columns, when the fact table exists
- Added added 'disk size' and 'disk space free %' to health check


## v7.2.0 
### Description

### Issues
[#72](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/72) Added support for creation of read-only user to DbUp
[#116](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/116) Added support for Azure SQL Database and Azure SQL Managed Instance to DbUp


## v7.1.0 

### Database compatibility
- StructureDB v6.12 
- DataDB v5.2 
- CommonDB v3.7 

### Issues
[#130](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/130) added temp file cleaning service
[#64](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/64) Skip codes in allowed constraint not part of codelist 
[#44](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/44) Generate empty ACC for live version when first upload targets PIT 
[#170](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/170) remove the requirement that the action can only be performed if the DSD is already deleted from the Mapping Store DB for method /cleanup/dsd  
[#70](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/70) Password change added to DbUp 
[#71](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/71) sql server compatible issues 
[#160](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/160) adjusted attribute validation message for edd import
[#132](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/123) Allow imports with basic and full validations. New validation only functions 
[#174](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/174) Create transaction item for init/allMappingsets method 
[#201](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/201) Fix of transfer between two different data versions 
[#4](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-config/-/issues/4) Update Transfer service error messages 
[#123](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/123) remove primary key in staging table 
[#198](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/198) Initialize empty mappingsets and actual content constraints for the first dataload 
[#205](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/205) Fix bug, actual content constraint not updated after data transfer 
[#202](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/202) Fix of wrong content constraint start date update issue 


## v7.0.0 

### Database compatibility
- StructureDB v6.12 
- DataDB v5.0 
- CommonDB v3.6 

### Issues
[#16](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/16) Bugfix, 4.0 Time dimension without codelist.

## v6.3.0 
### Description

### Issues
[#69](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/69) Bugfix for issue, dbup changes target db to single user mode at start and sets back to multiuser on finishs. 

## v6.1.0 
### Description

### Database compatibility
- StructureDB v6.12 
- DataDB v5.0 
- CommonDB v3.6 

### Issues

[#1](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/issues/1) Remove obsolete/unused ci variables. 
<BR>[#96](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/96) Cleanup mappingsets updated. 
<BR>[#168](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/168) Fix codelist mapping identification of DSD components. 
<BR>[#60](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/60) Csv reader validation error messages.
<BR>[#124](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/124) Non-numeric measure type implementation related changes. 
<BR>[#67](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/67) Add ExecutionTimeout in DbUp.
<BR>[#159](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/159) Manage Time in group attribute.
<BR>[#50](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/50) Storage of non-observation attributes at series and observation level.
<BR>[#51](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/51) Performance improvement � Remove ROW_ID from DSDs with less than 34 dimensions.
<BR>[#84](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/84) Support for DSD without Time dimension. 
<BR>[#16](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/16) Time dimension storage without codelist. 
<BR>[#57](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/57) Change producers to report all non-dataset attributes at obs level.
<BR>[#167](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/167) Validation of data database version.


## v6.0.5 (MSDB v6.12, DataDB v3.5) 
### Description

`WARNING`
Note that if the Data database is in a corrupted state (missing tables) before the migration to a new version, the migration tool (dbup tool) can potentially fail to update the database. We recommend the following steps during the migration:
1.  Backup all databases
2. When the dbup tool is run, please look carefully in the result of the execution (It will prompt you if there were issues during the update).
     - One known issue is that it will skip the creation of internal views when the required tables are not found.
3. If in the previous steps, there are errors for specific DSDs/Dataflows, **Before you do any other action, first use the method */init/dataflow* for the failing dataflows.**
     - If the previous doesn't help/work then the you must delete these dsd/dfs and run the */cleanup/dsd* function for those.
--------
After a successful migration, **Before any other action** you must first run the function */init/allMappingSets* for all the configured data spaces.

### Issues
- [#173](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/173) Added support to get logs of requests not creating transaction entry in db via status request method.


## v6.0.0 (MSDB v6.12, DataDB v3.5) 
### Description

==Important== Before using this software, sure to have backed up databases.
This version is a major update with breaking changes. 

This release contains upgrades of the Eurostat nuget package(s), the changelog for this can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/maapi.net.mirrored/-/blob/master/CHANGELOG.md)

- [#173](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/173) Added support to get logs of requests not creating transaction entry in db via status request method.
- [#103](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/103) References updated to Eurostat NSI v8.1.2.
- [#110](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/110) Add feature to validate the allowed content constraint for coded-attributes transfer-service.
- [#160](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/160) Fix issues with Mappingsets initialisation in transfer service.
- [#63](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/63) Bugfix for issue Exception of type 'DotStat.Db.Exception.KeyValueReadException' in Excel+EDD upload response.
- [#N/A]() Increase the threshold of the performance tests for small imports.
- [#N/A]() Update the documentation of the PIT restoration function.
- [#165](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/165) Bugfix allDataflows method fails with a Timed Out Error.
- [#161](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/161) Bugfix dataflow views with non mandatory dsd lvl attributes.
- [#120](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/120) Feature to consult the status of the data imports/transactions.
- [#49](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/49) Automatically create mapping sets in the mapping store database.
- [#154](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/154) For transfer, check existence of target dataflow and necessary permissions before responding with transaction ID.
- [#157](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/157) Validate when mappingset valid to date is datemax, set it to datemax - 1 second.
- [#63](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/63) remove check if dataflow already exists on /init/dataflow method, 
- [#N/A]() Added a SourceVersion to the Transfer endpoint. Involved adding an intermediate param, IFromSqlTransferParam, as ISqlTransferParam is used for Sdmx file to Sql as well.
- [#149](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/149) Bugfix in excel upload .
- [#108](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/108) Use Estat PermissionType .
- [#N/A](https://gitlab.com/sis-cc/.stat-suite/keycloak/-/issues/7) Change from implicit flow to Authorization code flow.
- [#143](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/143) Detailed import summary added.
- [#48](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/48) Automatically create data database sql views for dsd and dataflows.
- [#53](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/53) Grant permision to create view to dotstatwriter role.
- [#105](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/105) Use dataReaderManager, fix double unzip.


## 5.0.0 (MSDB v6.9, DataDB v2.1)
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/93
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/83
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/79
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/95
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/29
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/78
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/94
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-config/-/issues/1
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/96
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/101
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/106
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/111
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/46
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/117
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/100
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/49
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/107
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/101
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/102

## v4.2.4 2020-04-18 (MSDB v6.7, DataDB v2.1)
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/65


## v4.2.3 2020-04-18 (MSDB v6.7, DataDB v2.1)

- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/97
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/47
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/84
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/3
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/62
- https://gitlab.com/sis-cc/dotstatsuite-documentation/-/issues/65
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/58
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/65


## v4.1.2 2020-03-30 (MSDB v6.7, DataDB v2.1)

https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/issues/42
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/54
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/75
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/71
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/80
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/88
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/85
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/77
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/86
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/76
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/82
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/91
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/92


##  v4.0.3 2020-01-29 (MSDB v6.7, DataDB v2.1)
This release contains breaking changes with changes to the authentication management.

- /sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/66
- /sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/52


##  v3.0.1 2020-01-22 (MSDB v6.7, DataDB v2.1)
This release contains breaking changes with a new entry in the dataspaces.private.json and the introduction of localization.json via the Dotstat.config nuget package.     

- sis-cc/.stat-suite/dotstatsuite-core-common/issues/102