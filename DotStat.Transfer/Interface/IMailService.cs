
using System.Threading.Tasks;
using DotStat.Transfer.Utils;

namespace DotStat.Transfer.Interface
{
    /// <summary>
    /// Mail service interface.
    /// </summary>
    public interface IMailService
    {
        /// <summary>
        /// Sends the mail.
        /// </summary>
        /// <param name="transferResult">The transfer result.</param>
        /// <param name="languageCode">The language code.</param>
        /// <param name="isAdmin">The is admin flag.</param>
        /// <returns></returns>
        Task SendMail(TransactionResult transferResult, string languageCode, bool isAdmin);
    }
}