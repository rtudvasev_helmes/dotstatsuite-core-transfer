﻿using DotStat.Common.Configuration.Dto;
using DotStat.Transfer.Model;
using Microsoft.AspNetCore.Http;

namespace DotStat.Transfer.Param
{
    public class SdmxMetadataUrlToSqlTransferParam : TransferParamWithUrl
    {
        public SdmxMetadataUrlToSqlTransferParam(InternalSdmxParameters internalSdmxParameters, DataspaceInternal destinationDataspace, string language, IHeaderDictionary headerDictionary) 
            : base(internalSdmxParameters, destinationDataspace, language, headerDictionary)
        {
        }
    }
}
