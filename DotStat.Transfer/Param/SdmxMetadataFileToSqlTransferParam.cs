﻿using DotStat.Common.Configuration.Dto;
using DotStat.Transfer.Model;

namespace DotStat.Transfer.Param
{
    public class SdmxMetadataFileToSqlTransferParam : TransferParamWithFilePath
    {
        public SdmxMetadataFileToSqlTransferParam(InternalSdmxParameters internalSdmxParameters, DataspaceInternal destinationDataspace, string language) : base(internalSdmxParameters, destinationDataspace, language)
        {
        }
    }
}

