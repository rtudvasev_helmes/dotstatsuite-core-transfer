﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Auth;
using DotStat.Common.Localization;
using DotStat.Domain;
using DotStat.Common.Logger;
using DotStat.Db.Exception;
using DotStat.DB.Repository;
using DotStat.Db.Service;
using DotStat.Db.Util;
using DotStat.MappingStore;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using Estat.Sdmxsource.Extension.Constant;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using DotStat.Db;

namespace DotStat.Transfer.Producer
{
    ///TODO: Once used, include in unit tests
    [ExcludeFromCodeCoverage]
    public class SqlProducer<T> : IProducer<T> where T: TransferParam, IFromSqlTransferParam
    {
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly UnitOfWorkResolver _unitOfWorkResolver;
        private readonly DotStatDbServiceResolver _dotStatDbServiceResolver;

        public SqlProducer(
            IAuthorizationManagement authorizationManagement,
            IMappingStoreDataAccess mappingStoreDataAccess,
            UnitOfWorkResolver unitOfWorkResolver,
            DotStatDbServiceResolver dotStatDbServiceResolver
        )
        {
            _authorizationManagement = authorizationManagement;
            _mappingStoreDataAccess = mappingStoreDataAccess;
            _unitOfWorkResolver = unitOfWorkResolver;
            _dotStatDbServiceResolver = dotStatDbServiceResolver;
        }

        public Dataflow GetDataflow(T transferParam, bool throwErrorIfNotFound = true)
        {
            var dataflow = _mappingStoreDataAccess.GetDataflow(
                transferParam.SourceDataspace.Id,
                transferParam.SourceDataflow.AgencyId,
                transferParam.SourceDataflow.Id,
                transferParam.SourceDataflow.Version,
                throwErrorIfNotFound
           );

            if (transferParam.DestinationDataspace != null)
            {
                Log.Notice(string.Format(
                        LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.DataflowLoaded,
                            transferParam.CultureInfo.TwoLetterISOLanguageName),
                        dataflow.FullId,
                        transferParam.SourceDataspace.Id));
            }

            return dataflow;
        }

        public async Task<TransferContent> Process(T transferParam, Dataflow dataflow, CancellationToken cancellationToken)
        {
            if (!IsAuthorized(transferParam, dataflow))
            {
                throw new TransferUnauthorizedException();
            }

            var dotStatDbService = _dotStatDbServiceResolver(transferParam.SourceDataspace?.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.SourceDataspace?.Id);

            DataQueryImpl dataQuery;
            if (string.IsNullOrEmpty(transferParam.SourceQuery))
            {
                dataQuery = new DataQueryImpl(
                    dataStructure: dataflow.Dsd.Base,
                    lastUpdated: transferParam.UpdatedAfter,
                    dataQueryDetail: null,
                    firstNObs: null,
                    lastNObs: null,
                    dataProviders: null,
                    dataflow: dataflow.Base,
                    dimensionAtObservation: null,
                    selectionGroup: null);
            }
            else
            {
                var dataQueryFiltered = new RESTDataQueryCore(string.Format("data/{0},{1},{2}/{3}",
                            transferParam.SourceDataflow.AgencyId,
                            transferParam.SourceDataflow.Id,
                            transferParam.SourceDataflow.Version,
                            transferParam.SourceQuery));
                dataQueryFiltered.SetUpdatedAfter(transferParam.UpdatedAfter?.DateInSdmxFormat);

                dataQuery = new DataQueryImpl( dataQueryFiltered,
                    new InMemoryRetrievalManager(new SdmxObjectsImpl(dataflow.Dsd.Base, dataflow.Base)));
            }
            dataQuery.IncludeHistory = true;//Include deleted values

            await dotStatDbService.FillIdsFromDisseminationDb(dataflow, cancellationToken);

            var codeTranslator = new CodeTranslator(unitOfWork.CodeListRepository);
            await codeTranslator.FillDict(dataflow, cancellationToken);

            var content = new TransferContent();
            var sourceDbTableVersion = transferParam.SourceVersion == TargetVersion.Live
                ? dotStatDbService.GetDsdLiveVersion(dataflow.Dsd)
                : dotStatDbService.GetDsdPITVersion(dataflow.Dsd);

            if (sourceDbTableVersion == null)
                throw new TableVersionException($"The dataFlow {dataflow.FullId} does not contain a {transferParam.SourceVersion} version.");

            var hasMetadata = dataflow.Dsd?.Msd != null;

            switch (transferParam.TransferType)
            {
                case TransferType.DataAndMetadata:
                {
                    content.ReportedComponents = new ReportedComponents
                    {
                        DatasetAttributes = dataflow.Dsd.Attributes
                            .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet || a.Base.AttachmentLevel == AttributeAttachmentLevel.Null).ToList(),

                        //Attributes not attached to the time dimension
                        SeriesAttributesWithNoTimeDim = dataflow.Dsd.Attributes
                            .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Group || a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                            .Where(a => !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                            .ToList(),

                        //Attributes attached to the time dimension
                        ObservationAttributes = dataflow.Dsd.Attributes
                            .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation
                            || a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList(),
                        Dimensions = dataflow.Dsd.Dimensions.ToList(),
                        TimeDimension = dataflow.Dsd.TimeDimension,
                        IsPrimaryMeasureReported = true,
                        MetadataAttributes =
                            hasMetadata ? dataflow.Dsd.Msd.MetadataAttributes : new List<MetadataAttribute>()
                    };

                    content.DataObservations = unitOfWork.ObservationRepository.GetDataObservations(
                        dataQuery,
                        dataflow,
                        (DbTableVersion)sourceDbTableVersion,
                        cancellationToken
                    );

                    if (hasMetadata) {
                        content.MetadataObservations = unitOfWork.ObservationRepository.GetMetadataObservations(
                            dataQuery,
                            dataflow,
                            (DbTableVersion)sourceDbTableVersion,
                            cancellationToken
                        ); 
                    }

                    //DataSet Level attributes already extracted in observations
                    content.DatasetAttributes = new List<DataSetAttributeRow>();

                    break;
                }
                case TransferType.DataOnly:
                {
                    content.ReportedComponents = new ReportedComponents
                    {
                        DatasetAttributes = dataflow.Dsd.Attributes
                            .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet || a.Base.AttachmentLevel == AttributeAttachmentLevel.Null).ToList(),

                        //Attributes not attached to the time dimension
                        SeriesAttributesWithNoTimeDim = dataflow.Dsd.Attributes
                            .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Group || a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                            .Where(a => !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                            .ToList(),

                        //Attributes attached to the time dimension
                        ObservationAttributes = dataflow.Dsd.Attributes
                            .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation
                            || a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList(),
                        Dimensions = dataflow.Dsd.Dimensions.ToList(),
                        TimeDimension = dataflow.Dsd.TimeDimension,
                        IsPrimaryMeasureReported = true,
                    };

                    content.DataObservations = unitOfWork.ObservationRepository.GetDataObservations(
                        dataQuery,
                        dataflow,
                        (DbTableVersion)sourceDbTableVersion,
                        cancellationToken
                    );

                    //DataSet Level attributes already extracted in observations
                    content.DatasetAttributes = new List<DataSetAttributeRow>();
                    
                    break;
                }
                case TransferType.MetadataOnly:
                {
                    content.ReportedComponents = new ReportedComponents
                    {
                        Dimensions = dataflow.Dsd.Dimensions.ToList(),
                        MetadataAttributes =
                            hasMetadata ? dataflow.Dsd.Msd.MetadataAttributes : new List<MetadataAttribute>()
                    };

                    if (hasMetadata)
                        content.MetadataObservations = unitOfWork.ObservationRepository.GetMetadataObservations(
                            dataQuery,
                            dataflow,
                            (DbTableVersion)sourceDbTableVersion,
                            cancellationToken
                        );

                    break;
                }
            }

            return content;
        }

        public bool IsAuthorized(T transferParam, Dataflow dataflow)
        {
            return _authorizationManagement.IsAuthorized(
                transferParam.Principal, 
                transferParam.SourceDataspace.Id, 
                dataflow.AgencyId, 
                dataflow.Base.Id, 
                dataflow.Version.ToString(), 
                PermissionType.CanReadData
            );
        }

        public void Dispose()
        {
            //Nothing to do, sql connection  is within a "using" clause
        }
    }
}