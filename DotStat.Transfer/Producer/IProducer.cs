﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Domain;
using DotStat.Transfer.DataflowManager;
using DotStat.Transfer.Param;

namespace DotStat.Transfer.Producer
{
    public interface IProducer<in T>: IDataflowManager<T>, IDisposable where T : ITransferParam
    {
        Task<TransferContent> Process(T transferParam, Dataflow dataflow, CancellationToken cancellationToken);

        bool IsAuthorized(T transferParam, Dataflow dataflow);
    }
}