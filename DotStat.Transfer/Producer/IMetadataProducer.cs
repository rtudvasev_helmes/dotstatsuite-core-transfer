﻿using System.Threading.Tasks;
using DotStat.Transfer.Param;

namespace DotStat.Transfer.Producer
{
    public interface IMetadataProducer<in T> where T : ITransferParam
    {
        Task<bool> IsMetadataOnly(T transferParam);
    }
}