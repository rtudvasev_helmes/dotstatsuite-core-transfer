﻿using DotStat.Common.Auth;
using DotStat.Common.Configuration.Interfaces;
using DotStat.MappingStore;
using DotStat.Transfer.Param;
using System.Net.Http;

namespace DotStat.Transfer.Producer
{
    public class SdmxMetadataUrlProducer : UrlProducer<SdmxMetadataUrlToSqlTransferParam>
    {
        public SdmxMetadataUrlProducer(IAuthorizationManagement authorizationManagement, IMappingStoreDataAccess dataAccess, IHttpClientFactory httpClientFactory, IGeneralConfiguration generalConfiguration, ITempFileManagerBase tempFileManager)
            : base(authorizationManagement, tempFileManager, httpClientFactory, generalConfiguration, dataAccess)
        {
        }
    }
}