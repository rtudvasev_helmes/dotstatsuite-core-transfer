﻿using DotStat.Common.Auth;
using DotStat.MappingStore;
using DotStat.Transfer.Param;

namespace DotStat.Transfer.Producer
{
    public class SdmxMetadataFileProducer : FileProducer<SdmxMetadataFileToSqlTransferParam>
    {
        public SdmxMetadataFileProducer(IAuthorizationManagement authorizationManagement, IMappingStoreDataAccess dataAccess, ITempFileManagerBase tempFileManager) 
            : base(authorizationManagement, dataAccess, tempFileManager)
        {
        }
    }
}