﻿using DotStat.Common.Auth;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using DotStat.Transfer.Utils;
using Estat.Sdmxsource.Extension.Constant;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.DataParser.Engine.Csv;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Org.Sdmxsource.Util.Io;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace DotStat.Transfer.Producer
{
    public abstract class BaseFileProducer<T> : IProducer<T> where T : TransferParam, ITransferParamWithFilePath
    {
        protected readonly IAuthorizationManagement AuthorizationManagement;
        protected IMappingStoreDataAccess DataAccess { get; }
        protected ReadableDataLocationFactory DataLocationFactory { get; }
        private Dataflow _dataFlow;
        private ReportedComponents _reportedComponents;
        private readonly ITempFileManagerBase _tempFileManager;

        protected BaseFileProducer(
            IAuthorizationManagement authorizationManagement, 
            IMappingStoreDataAccess dataAccess, 
            ITempFileManagerBase tempFileManager)
        {
            AuthorizationManagement = authorizationManagement;
            DataAccess = dataAccess;
            DataLocationFactory = new ReadableDataLocationFactory();
            _tempFileManager = tempFileManager;
        }

        public bool IsAuthorized(T transferParam, Dataflow dataflow)
        {
            //As the Source is a file there is no need to check if the User can read Data.
            return true;
        }

        public Dataflow GetDataflow(T transferParam, bool throwErrorIfNotFound = true)
        {
            if (_dataFlow != null)
            {
                return _dataFlow;
            }

            GetFileFromSource(transferParam);
            _dataFlow = GetDataflowFromSdmxFile(transferParam);

            if (_dataFlow == null)
            {
                throw new DotStatException(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoDataflowReferenceInputDataset, transferParam.CultureInfo.TwoLetterISOLanguageName));
            }

            return _dataFlow;
        }

        public Task<TransferContent> Process(T transferParam, Dataflow dataflow, CancellationToken cancellationToken)
        {
            var fileInfo = new FileInfo(transferParam.FilePath);

            if (!fileInfo.Exists)
            {
                throw new DotStatException(string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.FileNotFound), transferParam.FilePath));
            }

            var canMergeData = IsAuthorized(StagingRowActionEnum.Merge, transferParam, dataflow);
            var canReplaceData = IsAuthorized(StagingRowActionEnum.Replace, transferParam, dataflow);
            var canDeleteData = IsAuthorized(StagingRowActionEnum.Delete, transferParam, dataflow);
            if (!canMergeData && !canDeleteData)
            {
                throw new UnauthorizedAccessException();
            }

            try
            {
                var transferContent = new TransferContent
                {
                    ReportedComponents = GetReportedComponents(transferParam.FilePath, GetDataflow(transferParam)),
                    DatasetAttributes = new List<DataSetAttributeRow>()
                };

                if (transferParam.TransferType == TransferType.MetadataOnly)
                {
                    transferContent.MetadataObservations = ProcessSdmxFile(transferParam.FilePath, dataflow, transferContent.DatasetAttributes, canMergeData, canDeleteData, canReplaceData, cancellationToken);
                }
                else
                {
                    transferContent.DataObservations = ProcessSdmxFile(transferParam.FilePath, dataflow, transferContent.DatasetAttributes, canMergeData, canDeleteData, canReplaceData, cancellationToken);
                }

                return Task.FromResult(transferContent);
            }
            catch (SdmxNotImplementedException ex)
            {
                throw new TransferFailedException(ex.Message);
            }
        }

        protected async IAsyncEnumerable<ObservationRow> ProcessSdmxFile(string filename, Dataflow dataflow, IList<DataSetAttributeRow> datasetAttributes, bool canMerge, bool canDelete, bool canReplaceData, [EnumeratorCancellation] CancellationToken cancellation)
        {
            using var sourceData = DataLocationFactory.GetReadableDataLocation(new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
            using var dataReaderEngine = new DataReaderManager().GetDataReaderEngine(sourceData, dataflow.Dsd.Base, null, dataflow.Dsd.Msd?.Base);

            foreach (var observationRow in await Task.Run(() => ReadSdmxFile(dataReaderEngine, dataflow, datasetAttributes, dataflow.Dsd.Base.TimeDimension?.Id, canMerge, canDelete, canReplaceData), cancellation))
            {
                cancellation.ThrowIfCancellationRequested();
                yield return observationRow;
            }
        }

        private IEnumerable<ObservationRow> ReadSdmxFile(IDataReaderEngine reader, Dataflow dataflow, IList<DataSetAttributeRow> datasetAttributes, string timeDimension, bool canMergeData, bool canDeleteData, bool canReplaceData)
        {
            var groupAttributes = new Dictionary<HashSet<string>, List<IKeyValue>>();
            var hasTimeDimension = !string.IsNullOrEmpty(timeDimension);
            var finishedProcessingGroups = false;

            var isXml = reader.IsXml();
            var checkAuthorizedEveryLevel = !isXml;
            var dataSetNumber = 1;
            var batchNumber = 1;
            var rowNumber = 1;
            
            //DataSet Level 
            if (reader.MoveNextDataset())
            {
                do
                {
                    var currentAction = reader.GetStagingRowActionEnum();
                    currentAction.CheckAuthorized(canMergeData, canDeleteData, canReplaceData, dataSetNumber, isXml);

                    StagingRowActionEnum? previousAction = null;
                    var previousCoordinateWildcarded = false;

                    //Attributes reported at DataSet level
                    var allDsdLvlComponentsOmitted = true;
                    DataSetAttributeRow dataSetAttributeRow = null;

                    //XML reader reports DataSet attributes only once at DataSet level
                    //CSV reader reports DataSet attributes every observation
                    if (reader is not CsvDataReaderEngine && reader is not CsvDataReaderEngineV2)
                    {
                        var reportedAttributesAtDataSetLevel = reader.DatasetAttributes.GetPresentComponents(currentAction);

                        if (reportedAttributesAtDataSetLevel.Any())
                        {
                            dataSetAttributeRow = new DataSetAttributeRow(batchNumber, currentAction);
                            foreach (var dataSetAttribute in reportedAttributesAtDataSetLevel)
                            {
                                dataSetAttributeRow.Attributes.Add(dataSetAttribute);
                                allDsdLvlComponentsOmitted = false;
                            }

                            datasetAttributes.Add(dataSetAttributeRow);
                        }
                    }

                    //Group/Series Level
                    if (reader.MoveNextKeyable())
                    {
                        do
                        {
                            if (checkAuthorizedEveryLevel)
                            {
                                reader.GetStagingRowActionEnum().CheckAuthorized(canMergeData, canDeleteData, canReplaceData, dataSetNumber, isXml);
                            }

                            //group found after processing series
                            if (finishedProcessingGroups && !reader.CurrentKey.Series)
                                throw new DotStatException(
                                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                        .SDMXMLErrorGropsAfterSeries));

                            var currentKeyable = reader.CurrentKey;
                            var allDimensionsOmitted = currentKeyable is null || currentKeyable.Key.All(d => string.IsNullOrEmpty(d.Code));
                            
                            var timeKeyValue = hasTimeDimension
                                ? reader.CurrentKey.Key.FirstOrDefault(k =>
                                    k.Concept.Equals(timeDimension, StringComparison.CurrentCultureIgnoreCase))
                                : null;
                            var obsTime = timeKeyValue?.Code;
                             
                            var currentKeyableCoordinate =
                                new HashSet<string>(currentKeyable?.Key?.Select(k => $"{k.Concept}:{k.Code}"));

                            //Process group keys
                            if (!reader.CurrentKey.Series)
                            {
                                groupAttributes.Add(currentKeyableCoordinate, currentKeyable?.Attributes?.ToList());
                                continue;
                            }
                            //Process series keys and observations
                            else
                            {
                                finishedProcessingGroups = true;
                            }

                            var currentCoordinateWildcarded = currentKeyable.Key.Any(kv => string.IsNullOrWhiteSpace(kv.Code)) || currentKeyable.Key.Any(kv => kv.Code.Equals(DbExtensions.DimensionWildCarded));

                            // Check if batch number should be incremented due to wildcarded coordinate
                            if (previousAction is null)
                            {
                                // do not increase the batch number in case the first coordinate is wildcarded
                                previousAction = currentAction;
                            }
                            else
                            {
                                if (currentCoordinateWildcarded || previousCoordinateWildcarded)
                                {
                                    batchNumber++;
                                }
                            }

                            //add series attributes
                            var seriesAttributes = new List<IKeyValue>(
                                currentKeyable?.Attributes?.GetPresentComponents(currentAction));

                            //add group attributes (merge do not have no time dim ref, deletions can have dim ref at seriesKey)
                            var dimGroupAttributesWithNoTimeDim = groupAttributes
                                .Where(dimGroupSerie => dimGroupSerie.Key.IsSubsetOf(currentKeyableCoordinate))
                                .SelectMany(item => item.Value).ToList();

                            seriesAttributes.AddRange(dimGroupAttributesWithNoTimeDim?.GetPresentComponents(currentAction));

                            //Observation Level 
                            if (reader.MoveNextObservation())
                            {
                                do
                                {
                                    if (checkAuthorizedEveryLevel)
                                    {
                                        currentAction = reader.GetStagingRowActionEnum();
                                        currentAction.CheckAuthorized(canMergeData, canDeleteData, canReplaceData, rowNumber, isXml);

                                        if (previousAction != currentAction)
                                        {
                                            // Increase batch no if current action is different but the batch no was not incremented due to wildcarded coords
                                            if (!currentCoordinateWildcarded && !previousCoordinateWildcarded)
                                                batchNumber++;

                                            previousAction = currentAction;
                                        }
                                    }

                                    var currentObservation = reader.CurrentObservation;
                                    obsTime = currentObservation?.ObsTime ?? obsTime;
                                    var timeDimensionOmitted = hasTimeDimension && string.IsNullOrEmpty(obsTime);

                                    //Include group attributes referencing the time dimension
                                    var dimGroupAttributesWithTimeDim = new List<IKeyValue>();
                                    if (hasTimeDimension)
                                    {
                                        var currentObsCoordinate = currentObservation is null ?
                                            new HashSet<string>() : new HashSet<string>(
                                            currentObservation.SeriesKey?.Key?.Select(k => $"{k.Concept}:{k.Code}"));
                                        
                                        dimGroupAttributesWithTimeDim = groupAttributes
                                            .Where(dimGroupSerie => dimGroupSerie.Key.IsSubsetOf(currentObsCoordinate))
                                            .SelectMany(item => item.Value)
                                            .Where(attribute => attribute.Concept.Equals(timeDimension, StringComparison.CurrentCultureIgnoreCase))
                                            .ToList();
                                    }

                                    var allAttributes = new List<IKeyValue>();

                                    //CSV readers modify the value of the dataset level attributes, every row
                                    if (reader is CsvDataReaderEngine or CsvDataReaderEngineV2)
                                    {
                                        //add dataSet Level attributes
                                        allAttributes.AddRange(
                                            reader.DatasetAttributes.GetPresentComponents(currentAction));
                                    }

                                    //add series Level attributes
                                    allAttributes.AddRange(seriesAttributes);

                                    //add group attributes with  time dim ref
                                    allAttributes.AddRange(dimGroupAttributesWithTimeDim?.GetPresentComponents(currentAction));

                                    //add obs attributes
                                    if (currentObservation is not null)
                                    {
                                        allAttributes.AddRange(
                                            currentObservation?.Attributes?.GetPresentComponents(currentAction));
                                    }

                                    var obsVal = _reportedComponents.IsPrimaryMeasureReported ? currentObservation?.GetObservationValue(currentAction) : null;
                                    var obsIsPresent = obsVal is not null;

                                    if (currentAction is StagingRowActionEnum.Delete || currentAction is StagingRowActionEnum.Replace)
                                    {

                                        //Avoid calculating if other components are present, when there is at least one dimension reported
                                        //If at least one dimension is Present then it is not a delete all action.
                                        if (allDimensionsOmitted && timeDimensionOmitted)
                                        {
                                            var allNonDsdLvlComponentsOmitted =
                                                !allAttributes.Any() && !obsIsPresent;

                                            if (dataSetAttributeRow is not null)
                                                allAttributes.AddRange(dataSetAttributeRow.Attributes);
                                            
                                            var allAttributesPresent = dataflow.Dsd.Attributes.All(attribute =>
                                                allAttributes.Any(a => a.Concept.Equals(attribute.Code,
                                                    StringComparison.CurrentCultureIgnoreCase)));
                                            var allComponentsPresent = allAttributesPresent && obsIsPresent;

                                            var allComponentsOmitted =
                                                allDsdLvlComponentsOmitted && allNonDsdLvlComponentsOmitted;

                                            //Either all components are reported or none are reported & no dimensions reported
                                            var isDeleteAll = allComponentsPresent || allComponentsOmitted;
                                            if (isDeleteAll)
                                            {
                                                //Special case delete all operation
                                                //TODO: when isDeleteAll a truncate call could be made instead
                                                yield return new ObservationRow(batchNumber: batchNumber, action: StagingRowActionEnum.DeleteAll, observation: null);
                                                continue;
                                            }

                                            //Nothing to process at observation level in this row
                                            if (allNonDsdLvlComponentsOmitted)
                                                continue;
                                        }
                                    }

                                    //TODO: We could use a lighter object (Not IObservation) to improve performance
                                    yield return new ObservationRow(batchNumber: batchNumber, action: currentAction, 
                                        observation: new ObservationImpl(currentKeyable, obsTime, obsVal, allAttributes,
                                            crossSectionValue: null)
                                    );

                                    rowNumber++;

                                    previousCoordinateWildcarded = currentCoordinateWildcarded;

                                } while (reader.MoveNextObservation());
                            }
                            else
                            {
                                //All observation level components have been omitted, therefore no need to check if all components are present
                                var allNonDsdLvlComponentsOmitted = !seriesAttributes.Any();

                                if (currentAction is StagingRowActionEnum.Delete || currentAction is StagingRowActionEnum.Replace)
                                {
                                    var isDeleteAll = allDimensionsOmitted && allDsdLvlComponentsOmitted &&
                                                      allNonDsdLvlComponentsOmitted;
                                    if (isDeleteAll)
                                    {
                                        //Special case delete all operation
                                        //TODO: when isDeleteAll a truncate call could be made instead
                                        yield return new ObservationRow(batchNumber: batchNumber, action: StagingRowActionEnum.DeleteAll, observation: null);
                                        continue;
                                    }
                                }

                                yield return new ObservationRow(batchNumber: batchNumber, action: currentAction, 
                                    observation: new ObservationImpl(currentKeyable, obsTime, null, seriesAttributes,
                                        crossSectionValue: null)
                                );
                            }

                            previousCoordinateWildcarded = currentCoordinateWildcarded;

                        } while (reader.MoveNextKeyable());

                    }
                    else
                    {
                        if (currentAction is StagingRowActionEnum.Delete || currentAction is StagingRowActionEnum.Replace)
                        {
                            //All observation and series level components have been omitted,
                            //therefore no need to check if all components are present
                            //No dimensions are reported at dataset level
                            var isDeleteAll = allDsdLvlComponentsOmitted;
                            //Special case delete all operation
                            //TODO: when isDeleteAll a truncate call could be made instead
                            if (isDeleteAll)
                                yield return new ObservationRow(batchNumber: batchNumber, action: StagingRowActionEnum.DeleteAll, observation: null);
                        }
                    }

                    dataSetNumber++;
                } while (reader.MoveNextDataset());
            }
        }

        protected bool ContainsMetadataOnly(T transferParam)
        {
            GetDataflow(transferParam);

            var reportedComponents = GetReportedComponents(transferParam.FilePath, _dataFlow);

            if (reportedComponents.IsMetadataReported() && _dataFlow.Dsd.Msd == null)
            {
                throw new TransferFailedException(string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MissingMsdReference), _dataFlow.FullId));
            }

            // If a measure or data attribute is reported, it is treated as data import (even if there are metadata attributes.
            var containsMetadataOnly = !reportedComponents.IsDataReported() && reportedComponents.IsMetadataReported();

            if (containsMetadataOnly)
            {
                transferParam.TransferType = TransferType.MetadataOnly;
            }

            return containsMetadataOnly;
        }

        protected void GetFileFromSource(T transferParam)
        {
            var fileInfo = new FileInfo(transferParam.FilePath);

            if (!fileInfo.Exists)
            {
                throw new DotStatException(string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.FileNotFound), transferParam.FilePath));
            }

            if (transferParam.IsZipProcessed ||
                (!(transferParam.FilePath ?? string.Empty).EndsWith(".zip", StringComparison.OrdinalIgnoreCase) &&
                 !(transferParam.DataSource ?? string.Empty).EndsWith(".zip", StringComparison.OrdinalIgnoreCase)))
            {
                return;
            }

            var tempFile = _tempFileManager.GetTempFileName(fileInfo.Name + ".unzipped");

            // unzip only once
            if (!File.Exists(tempFile))
            {
                using (var zip = ZipFile.OpenRead(fileInfo.FullName))
                {
                    var entry = zip.Entries.First();
                    entry.ExtractToFile(tempFile, true);
                }

                File.SetCreationTime(tempFile, DateTime.Now);
            }

            if (transferParam.FilePath != tempFile)
            {
                transferParam.FilesToDelete.Add(tempFile);
                transferParam.FilePath = tempFile;
            }

            transferParam.IsZipProcessed = true;
        }

        protected ReportedComponents GetReportedComponents(string filename, Dataflow dataflow)
        {
            //Avoid reading file 3 times 
            if (_reportedComponents != null)
            {
                return _reportedComponents;
            }

            _reportedComponents = new ReportedComponents
            {
                Dimensions = dataflow.Dsd.Dimensions.ToList()
            };

            using var sourceData = DataLocationFactory.GetReadableDataLocation(new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
            using var dataReaderEngine = new DataReaderManager().GetDataReaderEngine(sourceData, dataflow.Dsd.Base, null, dataflow.Dsd.Msd?.Base);

            //In XML there is no way to know in advance which components are reported, therefore we mark all measures and attributes as reported
            //In XML we do not support referential metadata
            if (dataReaderEngine is not CsvDataReaderEngineV2 and not CsvDataReaderEngine)
            {
                _reportedComponents.Dimensions = dataflow.Dsd.Dimensions.ToList();
                _reportedComponents.TimeDimension = dataflow.Dsd.TimeDimension;
                _reportedComponents.DatasetAttributes = dataflow.Dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet || a.Base.AttachmentLevel == AttributeAttachmentLevel.Null).ToList();

                //attributes not attached to the time dimension
                _reportedComponents.SeriesAttributesWithNoTimeDim = dataflow.Dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Group || a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                    .Where(a => !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                    .ToList();

                //attributes attached to the time dimension
                _reportedComponents.ObservationAttributes = dataflow.Dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation
                                || a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

                _reportedComponents.IsPrimaryMeasureReported = true;
                _reportedComponents.IntentionallyMissingValue =
                    dataflow.Dsd.Base.PrimaryMeasure?.Representation?.TextFormat?.TextType?.IsNumericTextType != null &&
                    dataflow.Dsd.Base.PrimaryMeasure.Representation.TextFormat.TextType.IsNumericTextType
                        ? IntentionallyMissingValue.NumericalXml
                        : IntentionallyMissingValue.TextualXml;

                return _reportedComponents;
            }

            //CSV readers
            if (!dataReaderEngine.MoveNextDataset() || !dataReaderEngine.MoveNextKeyable() || !dataReaderEngine.MoveNextObservation())
            {
                return _reportedComponents;
            }

            var currentObservation = dataReaderEngine.CurrentObservation;
            //Dimensions' found in csv
            _reportedComponents.Dimensions = dataflow.Dsd.Dimensions
                .Where(d => currentObservation.SeriesKey.Key.Any(sk => sk.Concept.Equals(d.Code, StringComparison.InvariantCultureIgnoreCase))).ToList();

            //Time dimension found in csv
            if (currentObservation.ObsTime != null)
            {
                _reportedComponents.TimeDimension = dataflow.Dsd.TimeDimension;
                _reportedComponents.Dimensions.Add(dataflow.Dsd.TimeDimension);
            }

            //Measure column found in csv
            if (currentObservation.ObservationValue != null)
            {
                _reportedComponents.IsPrimaryMeasureReported = true;
            }

            //Dataset attributes' columns found in csv
            _reportedComponents.DatasetAttributes = dataflow.Dsd.Attributes
                .Where(a => dataReaderEngine.DatasetAttributes.Any(ar =>
                    ar.Concept.Equals(a.Code, StringComparison.InvariantCultureIgnoreCase))).ToList();

            //Series attributes' columns found in csv (attributes not attached to the time dimension)
            _reportedComponents.SeriesAttributesWithNoTimeDim = dataflow.Dsd.Attributes
                    .Where(a => currentObservation.Attributes.Any(ar => ar.Concept.Equals(a.Code, StringComparison.InvariantCultureIgnoreCase)))
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Group || a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                    .Where(a => !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                    .ToList();

            //Observation attributes' columns found in csv (attributes attached to the time dimension)
            _reportedComponents.ObservationAttributes = dataflow.Dsd.Attributes
                .Where(a => currentObservation.Attributes.Any(ar => ar.Concept.Equals(a.Code, StringComparison.InvariantCultureIgnoreCase)))
                .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation
                || a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

            //Referential metadata components
            if (dataflow.Dsd.Msd != null)
            {
                var attributes = dataflow.Dsd.Msd.MetadataAttributes
                    .Where(x => currentObservation.Attributes.Any(ar =>
                        ar.Concept.Equals(x.Base.GetFullIdPath(false), StringComparison.InvariantCultureIgnoreCase)));

                _reportedComponents.MetadataAttributes = new List<MetadataAttribute>();
                _reportedComponents.MetadataAttributes.AddRange(attributes.Select(x => new MetadataAttribute(x.Base) { Dsd = dataflow.Dsd, HierarchicalId = x.Base.GetFullIdPath(false) }));
            }

            _reportedComponents.IntentionallyMissingValue = IntentionallyMissingValue.Csv;

            return _reportedComponents;
        }

        private Dataflow GetDataflowFromSdmxFile(T transferParam)
        {
            try
            {
                var sdmxObjectRetrievalManager = DataAccess.GetRetrievalManager(transferParam.DestinationDataspace.Id);

                using var sourceData = DataLocationFactory.GetReadableDataLocation(new FileStream(transferParam.FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
                using var dataReaderEngine = new DataReaderManager().GetDataReaderEngine(sourceData, sdmxObjectRetrievalManager);

                if (!dataReaderEngine.MoveNextDataset())
                {
                    return null;
                }

                // Check that the dataflow exists in the mapping store database
                var dataflow = DataAccess.GetDataflow(transferParam.DestinationDataspace.Id,
                    dataReaderEngine.Dataflow.AgencyId,
                    dataReaderEngine.Dataflow.Id,
                    dataReaderEngine.Dataflow.Version);

                if (dataReaderEngine is CsvDataReaderEngineV2 csvDataReaderEngineV2 && csvDataReaderEngineV2.MetadataStructure != null)
                {
                    dataflow.Dsd.Msd = new Msd(csvDataReaderEngineV2.MetadataStructure);
                }

                return dataflow;
            }
            catch (SdmxSemmanticException e) when (Equals(e.Code, ExceptionCode.SdmxCsvInvalidDelimiter))
            {
                throw new SdmxSemmanticException(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CsvInvalidDelimiter), e);
            }
            catch (SdmxSemmanticException e) when (Equals(e.Code, ExceptionCode.SdmxCsvMissingDimensions))
            {
                if (e.InnerException != null && !string.IsNullOrEmpty(e.InnerException.Message) &&
                    e.InnerException.Message.Contains(';'))
                {
                    var dimensionAndAttributeList = e.InnerException.Message.Split(';');
                    throw new SdmxSemmanticException(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CsvMissingDimensions),
                            dimensionAndAttributeList[0],
                            dimensionAndAttributeList[1]),
                        e);
                }

                throw;
            }
        }

        public void Dispose()
        {
            //Nothing to do, IDataReaderEngine is within a "using" clause
        }
    
        protected bool IsAuthorized(StagingRowActionEnum action, T transferParam, Dataflow dataflow)
        {
            return action switch
            {
                StagingRowActionEnum.Merge => AuthorizationManagement.IsAuthorized(transferParam.Principal,
                    transferParam.DestinationDataspace.Id, dataflow.AgencyId, dataflow.Base.Id,
                    dataflow.Version.ToString(), PermissionType.CanImportData) || AuthorizationManagement.IsAuthorized(
                    transferParam.Principal, transferParam.DestinationDataspace.Id, dataflow.AgencyId, dataflow.Base.Id,
                    dataflow.Version.ToString(), PermissionType.CanUpdateData),
                StagingRowActionEnum.Replace => AuthorizationManagement.IsAuthorized(transferParam.Principal,
                    transferParam.DestinationDataspace.Id, dataflow.AgencyId, dataflow.Base.Id,
                    dataflow.Version.ToString(), PermissionType.CanImportData) || AuthorizationManagement.IsAuthorized(
                    transferParam.Principal, transferParam.DestinationDataspace.Id, dataflow.AgencyId, dataflow.Base.Id,
                    dataflow.Version.ToString(), PermissionType.CanUpdateData),
                StagingRowActionEnum.Delete => AuthorizationManagement.IsAuthorized(transferParam.Principal,
                    transferParam.DestinationDataspace.Id, dataflow.AgencyId, dataflow.Base.Id,
                    dataflow.Version.ToString(), PermissionType.CanDeleteData),
                _ => false
            };
        }
    }
}
