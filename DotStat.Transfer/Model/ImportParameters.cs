using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Configuration;
using DotStat.Domain;
using DotStat.Transfer.Param;

namespace DotStat.Transfer.Model
{
    public interface IFileParameter
    {
        public string GetFileFieldName(string fieldName);
        public string GetPathFieldName(string fieldName);
        public string[] GetFieldAllowedExtensions(BaseConfiguration configuration, string fieldName);
        public string[] GetFieldAllowedContentTypes(BaseConfiguration configuration, string fieldName);
    }

    [SuppressMessage("Style", "IDE1006:Naming Styles")]
    public class BaseSdmxParameters : IFileParameter
    {
        /// <summary>
        /// The ID of the dataspace to where the values will be stored/validated. <br></br> Example: <code>design</code>
        /// </summary>
        [Display(Order = 1)]
        [Required]
        public string dataspace { get; set; } = "default";
        
        /// <summary>
        /// Obsolete - The artefact value is now directly read from the input file
        /// </summary>
        [Display(Order = 2)]
        public string dataflow { get; set; }

        /// <summary>
        /// Optional - Two letter (ISO Alpha-2) language code to produce the response message/email<br></br>Example: <code>en</code>
        /// </summary>
        [Display(Order = 3)]
        public string lang { get; set; }

        /// <summary>
        /// Optional - The remote location (path/url) of the input file, containing the data or referential metadata to be imported. <br></br>Make sure that the remote file is accessible by the transfer-service
        /// </summary>
        [Display(Order = 4)]
        public string filepath { get; set; }

        /// <summary>
        /// Optional - When no filepath is given, The input SDMX data/referential metadata file to be imported/validated. <br></br>A maximum size file limit might apply
        /// </summary>
        [Display(Order = 5)]
        public byte[] file { get; set; }

        /// <summary>
        /// Optional - The target version of the data and referential metadata to be used during the import/validation. <br></br>Possible values: Live (0), Point in Time PIT (1), Default (Live)
        /// </summary>
        [Display(Order = 6)]
        public TargetVersion? targetVersion { get; set; } = TargetVersion.Live;

        public string GetFileFieldName(string fieldName)
        {
            return fieldName == "file" ? "OriginalFileName" : string.Empty;
        }

        public string GetPathFieldName(string fieldName)
        {
            return fieldName == "file" ? "FileLocalPath" : string.Empty;
        }

        public string[] GetFieldAllowedExtensions(BaseConfiguration configuration, string fieldName)
        {
            return fieldName == "file" ? configuration.SDMXFileAllowedExtensions : null;
        }

        public string[] GetFieldAllowedContentTypes(BaseConfiguration configuration, string fieldName)
        {
            return fieldName == "file" ? configuration.SDMXFileAllowedContentTypes : null;
        }
    }

    [SuppressMessage("Style", "IDE1006:Naming Styles")]
    public class SdmxImportParameters : BaseSdmxParameters
    {
        /// <summary>
        /// Optional - Point in time release date <br></br> Format (YYYY-MM-DDThh:mm:ss.sTZD) <br></br>Example: <code>2022-06-04T10:16:01</code>, <code>2022-06-04T08:16:01Z</code>, <code>2022-06-04T10:16:01+02:00</code>, <code>2022-06-04T10:16:01.021+02:00</code>
        /// </summary>
        public string PITReleaseDate { get; set; }

        /// <summary>
        /// Optional - Indicate if the current LIVE version should be kept for restoration purposes when PIT release becomes active
        /// </summary>
        public bool? restorationOptionRequired { get; set; } = false;

        /// <summary>
        /// Optional - The type of validation to use during import: <br></br> Possible values: Import With <b>Basic Validation (0)</b>, Import With <b>Advanced Validation (1)</b>, Default (Import With Basic Validation)
        /// </summary>
        public ImportValidationType? validationType { get; set; } = ImportValidationType.ImportWithBasicValidation;

        /// <summary>
        /// When using a remote URL that is a .Stat Suite data space using the same identity provider instance than this transfer service, set this parameter to true to authenticate you against that source data space.
        /// </summary>
        public bool? authenticateToRemoteURL { get; set; } = false;
    }

    public class InternalSdmxParameters : SdmxImportParameters
    {
        public string OriginalFileName { get; set; }
        public string FileLocalPath { get; set; }
        public ValidationType ImportValidationType { get; set; }
    }

    [SuppressMessage("Style", "IDE1006:Naming Styles")]
    public class BaseExcelParameters : IFileParameter
    {
        /// <summary>
        /// The ID of the dataspace to where the values will be stored/validated. <br></br> Example: <code>design</code>
        /// </summary>
        [Display(Order = 1)]
        [Required]
        public string dataspace { get; set; } = "default";

        /// <summary>
        /// Optional - Two letter (ISO Alpha-2) language code to produce the response message/email<br></br>Example: <code>en</code>
        /// </summary>
        [Display(Order = 2)]
        public string lang { get; set; }

        /// <summary>
        /// A XML edd file containing the description of the Excel file to be imported. <br></br>A maximum size file limit might apply
        /// </summary>
        [Display(Order = 3)]
        [Required]
        public byte[] eddFile { get; set; } = {0x00};

        /// <summary>
        /// An Excel file containing the data/referential metadata values to be imported. <br></br>A maximum size file limit might apply
        /// </summary>
        [Display(Order = 4)]
        [Required]
        public byte[] excelFile { get; set; } = {0x00};

        /// <summary>
        /// Optional - The target version of the data and referential metadata to be used during the import/validation. <br></br>Possible values: Live (0), Point in Time PIT (1), Default (Live)
        /// </summary>
        [Display(Order = 5)]
        public TargetVersion? targetVersion { get; set; } = TargetVersion.Live;

        public string GetFileFieldName(string fieldName)
        {
            return fieldName switch
            {
                "eddFile" => "OriginalEddFileName",
                "excelFile" => "OriginalExcelFileName",
                _ => null
            };
        }

        public string GetPathFieldName(string fieldName)
        {
            return fieldName switch
            {
                "eddFile" => "EddFileLocalPath",
                "excelFile" => "ExcelFileLocalPath",
                _ => null
            };
        }

        public string[] GetFieldAllowedExtensions(BaseConfiguration configuration, string fieldName)
        {
            return fieldName switch
            {
                "eddFile" => configuration.EddFileAllowedExtensions,
                "excelFile" => configuration.ExcelFileAllowedExtensions,
                _ => null
            };
        }

        public string[] GetFieldAllowedContentTypes(BaseConfiguration configuration, string fieldName)
        {
            return fieldName switch
            {
                "eddFile" => configuration.EddFileAllowedContentTypes,
                "excelFile" => configuration.ExcelFileAllowedContentTypes,
                _ => null
            };
        }
    }

    [SuppressMessage("Style", "IDE1006:Naming Styles")]
    public class ExcelImportParameters : BaseExcelParameters
    {
        /// <summary>
        /// Optional - Point in time release date <br></br> Format (YYYY-MM-DDThh:mm:ss.sTZD) <br></br>Example: <code>2022-06-04T10:16:01</code>, <code>2022-06-04T08:16:01Z</code>, <code>2022-06-04T10:16:01+02:00</code>, <code>2022-06-04T10:16:01.021+02:00</code>
        /// </summary>
        public string PITReleaseDate { get; set; }

        /// <summary>
        /// Optional - Indicate if the current LIVE version should be kept for restoration purposes when PIT release becomes active
        /// </summary>
        public bool? restorationOptionRequired { get; set; } = false;

        /// <summary>
        /// Optional - The type of validation to use during import:  <br></br>Possible values: Import With <b>Basic Validation (0)</b>, Import With <b>Advanced Validation (1)</b>, Default (Import With Basic Validation)
        /// </summary>
        public ImportValidationType? validationType { get; set; } = ImportValidationType.ImportWithBasicValidation;
    }

    public class InternalExcelParameters : ExcelImportParameters
    {
        public string OriginalEddFileName { get; set; }
        public string EddFileLocalPath { get; set; }
        public string OriginalExcelFileName { get; set; }
        public string ExcelFileLocalPath { get; set; }
        public ValidationType ImportValidationType { get; set; }
    }
}
