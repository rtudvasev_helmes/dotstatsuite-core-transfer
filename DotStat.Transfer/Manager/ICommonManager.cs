using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.Repository;
using DotStat.Domain;
using DotStat.Transfer.Param;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;

namespace DotStat.Transfer.Manager
{
    public interface ICommonManager
    {
        public Task<JObject> GetPITInfo(ITransferParam transferParam, IDataflowMutableObject sourceDataFlow, CancellationToken cancellationToken);

        public Task Rollback(ITransferParam transferParam, Dataflow dataflow, CancellationToken cancellationToken);

        public Task Restore(
            TransferParam transferParam,
            Transaction transaction,
            Dataflow dataflow,
            CancellationToken cancellationToken);


        public Task<bool> InitDataDbObjectsOfDataflow(
            TransferParam transferParam,
            IDataflowMutableObject targetDataflow,
            CancellationToken cancellationToken,
            bool offline = false);


        public Task<bool> DeleteMappingSets(TransferParam transferParam, IDataflowMutableObject targetDataflow,
            CancellationToken cancellationToken);


        public Task<bool> CleanUpDsd(TransferParam transferParam, IDataStructureMutableObject targetDsd,
            bool checkExistingDataStructure, bool cleanupMsdOnly,
            CancellationToken cancellationToken);


        public Task<bool> CleanUpAllOrphans(TransferParam transferParam, Transaction transaction, CancellationToken cancellationToken);


        public bool IsAuthorized(ITransferParam transferParam, Dataflow dataflow);


        public Dataflow GetDataflow(string sourceDataSpaceId, IDataflowMutableObject sourceDataflow,
            ResolveCrossReferences resolveCrossReferences = ResolveCrossReferences.ResolveExcludeAgencies);
    }
}
