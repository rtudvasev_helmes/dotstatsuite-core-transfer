using DotStat.Common.Configuration;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;

namespace DotStat.Transfer.Manager
{
    public class SdmxMetadataUrlToSqlTransferManager : UrlTransferManager<SdmxMetadataUrlToSqlTransferParam>
    {
        public SdmxMetadataUrlToSqlTransferManager(BaseConfiguration configuration, IProducer<SdmxMetadataUrlToSqlTransferParam> observationProducer, IConsumer<SdmxMetadataUrlToSqlTransferParam> observationConsumer) 
            : base(configuration, observationProducer, observationConsumer)
        {
        }
    }
}
