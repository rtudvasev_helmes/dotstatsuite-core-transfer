﻿using DotStat.Common.Configuration;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;
using System;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Domain;

namespace DotStat.Transfer.Manager
{
    public class FileTransferManager<T> : TransferManager<T> where T : ITransferParam
    {
        public FileTransferManager(BaseConfiguration configuration, IProducer<T> observationProducer, IConsumer<T> observationConsumer) : base(configuration, observationProducer, observationConsumer)
        {
        }

        public override async Task Transfer(T transferParam, Transaction transaction, CancellationToken cancellationToken)
        {
            if (transferParam == null)
            {
                throw new ArgumentNullException(nameof(transferParam));
            }

            var destinationDataflow = Producer.GetDataflow(transferParam);

            if (!Consumer.IsAuthorized(transferParam, destinationDataflow))
            {
                throw new TransferUnauthorizedException();
            }

            var transferContent = await Producer.Process(transferParam, destinationDataflow, cancellationToken);

            if (!HasComponentsToProcess(destinationDataflow, transferContent))
            {
                return;
            }

            if (!await Consumer.Save(transferParam, transaction, destinationDataflow, transferContent, cancellationToken))
            {
                throw new TransferFailedException();
            }

            Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.StreamingFinished, transferParam.CultureInfo.TwoLetterISOLanguageName));
        }
    }
}
