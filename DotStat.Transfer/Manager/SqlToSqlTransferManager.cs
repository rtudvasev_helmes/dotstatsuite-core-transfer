﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Domain;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.DataflowManager;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using DotStat.Transfer.Processor;
using DotStat.Transfer.Producer;

namespace DotStat.Transfer.Manager
{
    public class SqlToSqlTransferManager : TransferManager<SqlToSqlTransferParam>
    {
        private readonly ITransferProcessor _transferProcessor;

        public SqlToSqlTransferManager(
            BaseConfiguration configuration, 
            IProducer<SqlToSqlTransferParam> observationProducer, 
            IConsumer<SqlToSqlTransferParam> observationConsumer, 
            ITransferProcessor transferProcessor
        ) : base(configuration, observationProducer, observationConsumer)
        {
            _transferProcessor = transferProcessor;
        }

        public override async Task Transfer(SqlToSqlTransferParam transferParam, Transaction transaction, CancellationToken cancellationToken)
        {
            if (transferParam == null)
            {
                throw new ArgumentNullException(nameof(transferParam));
            }

            var sourceDataflow = Producer.GetDataflow(transferParam);
            var destinationDataflow = ((IDataflowManager<SqlToSqlTransferParam>)Consumer).GetDataflow(transferParam);

            if (!Producer.IsAuthorized(transferParam, sourceDataflow))
            {
                throw new TransferUnauthorizedException();
            }

            //Check that the user is authorized to read data from the source dataspace
            if (!Consumer.IsAuthorized(transferParam, destinationDataflow))
            {
                throw new TransferUnauthorizedException();
            }

            var transferContent = await Producer.Process(transferParam, sourceDataflow, cancellationToken);

            //TODO should the validations be done in the processor?
            var verifiedTransferContent = _transferProcessor.Process(sourceDataflow, destinationDataflow, transferContent);

            Log.Notice(
                LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.PluginProcessFinished,
                    transferParam.CultureInfo.TwoLetterISOLanguageName));

            if (!await Consumer.Save(transferParam, transaction, destinationDataflow, verifiedTransferContent, cancellationToken))
            {
                throw new TransferFailedException();
            }

            Log.Notice(
                LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.StreamingFinished,
                    transferParam.CultureInfo.TwoLetterISOLanguageName));
        }
    }
}
