﻿using DotStat.Domain;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;
using System.Threading;
using System.Threading.Tasks;

namespace DotStat.Transfer.Manager
{
    public interface ITransferManager<T> where T : ITransferParam
    {
        Task Transfer(T transferParam, Transaction transaction, CancellationToken cancellationToken);
        IProducer<T> Producer { get; set; }
        IConsumer<T> Consumer { get; set; }
        bool HasComponentsToProcess(Dataflow dataflow, TransferContent transferConten);
    }
}