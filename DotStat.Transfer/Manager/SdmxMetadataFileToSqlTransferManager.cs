﻿using DotStat.Common.Configuration;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;

namespace DotStat.Transfer.Manager
{
    public class SdmxMetadataFileToSqlTransferManager : FileTransferManager<SdmxMetadataFileToSqlTransferParam>
    {
        public SdmxMetadataFileToSqlTransferManager(BaseConfiguration configuration, IProducer<SdmxMetadataFileToSqlTransferParam> observationProducer, IConsumer<SdmxMetadataFileToSqlTransferParam> observationConsumer) 
            : base(configuration, observationProducer, observationConsumer)
        {
        }
    }
}
