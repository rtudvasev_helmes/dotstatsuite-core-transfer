﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Domain;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;

namespace DotStat.Transfer.Manager
{
    public class ExcelToSqlTransferManager : TransferManager<ExcelToSqlTransferParam>
    {
        public ExcelToSqlTransferManager(BaseConfiguration configuration, IProducer<ExcelToSqlTransferParam> observationProducer, 
            IConsumer<ExcelToSqlTransferParam> observationConsumer) : base(configuration, observationProducer, observationConsumer)
        {
        }

        public override async Task Transfer(ExcelToSqlTransferParam transferParam, Transaction transaction, CancellationToken cancellationToken)
        {
            if (transferParam == null)
            {
                throw new ArgumentNullException(nameof(transferParam));
            }

            using (Producer)
            {
                var destinationDataflow = Producer.GetDataflow(transferParam);

                if (!Consumer.IsAuthorized(transferParam, destinationDataflow))
                {
                    throw new TransferUnauthorizedException();
                }

                var transferContent = await Producer.Process(transferParam, destinationDataflow, cancellationToken);

                if (!HasComponentsToProcess(destinationDataflow, transferContent))
                    return;

                if (!await Consumer.Save(transferParam, transaction, destinationDataflow, transferContent, cancellationToken))
                {
                    throw new TransferFailedException();
                }
            }

            Log.Notice(LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.StreamingFinished,
                    transferParam.CultureInfo.TwoLetterISOLanguageName));
        }
    }
}
