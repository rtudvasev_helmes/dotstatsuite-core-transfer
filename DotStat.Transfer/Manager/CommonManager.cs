using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Auth;
using DotStat.Common.Enums;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Exception;
using DotStat.DB.Repository;
using DotStat.Db.Service;
using DotStat.Db.Util;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using Estat.Sdmxsource.Extension.Constant;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;

namespace DotStat.Transfer.Manager
{
    public class CommonManager : ICommonManager
    {
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly UnitOfWorkResolver _unitOfWorkResolver;
        private readonly DotStatDbServiceResolver _dotStatDbServiceResolver;

        public CommonManager(
            IAuthorizationManagement authorizationManagement,
            IMappingStoreDataAccess mappingStoreDataAccess,
            UnitOfWorkResolver unitOfWorkResolver,
            DotStatDbServiceResolver dotStatDbServiceResolver)
        {
            _authorizationManagement = authorizationManagement;
            _mappingStoreDataAccess = mappingStoreDataAccess;
            _unitOfWorkResolver = unitOfWorkResolver;
            _dotStatDbServiceResolver = dotStatDbServiceResolver;
        }

        public async Task<JObject> GetPITInfo(
            ITransferParam transferParam, 
            IDataflowMutableObject sourceDataFlow, 
            CancellationToken cancellationToken)
        {
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace?.Id);

            var dataflow = GetDataflow(transferParam.DestinationDataspace?.Id, sourceDataFlow);

            if (!IsAuthorized(transferParam, dataflow))
            {
                throw new TransferUnauthorizedException();
            }
            
            return await unitOfWork.ArtefactRepository.GetDSDPITInfo(dataflow, cancellationToken);
        }

        public async Task Rollback(ITransferParam transferParam, 
            Dataflow dataflow,
            CancellationToken cancellationToken)
        {
            var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace?.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace?.Id);
            try
            {
                var transaction = await unitOfWork.TransactionRepository.CreateTransactionItem(
                    transferParam.Id, dataflow.FullId,
                    transferParam.Principal, sourceDataSpace:null, dataSource:null,
                    TransactionType.Rollback, transferParam.TargetVersion, cancellationToken);

                transaction.FinalTargetVersion = transaction.RequestedTargetVersion;
                if (!(await dotStatDbService.TryNewTransaction(transaction, dataflow, transferParam.Principal, _mappingStoreDataAccess, cancellationToken)).Success)
                {
                    await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);
                    return;
                }

                //POINT OF NO RETURN
                cancellationToken = CancellationToken.None;

                await dotStatDbService.Rollback(dataflow, _mappingStoreDataAccess, cancellationToken);
                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, true);
                Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TransactionSuccessful));
            }
            catch (System.Exception)
            {
                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);
                throw;
            }
        }

        public async Task Restore(
            TransferParam transferParam, 
            Transaction transaction,
            Dataflow dataflow,
            CancellationToken cancellationToken)
        {
            var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace?.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace?.Id);
            try
            {
                transaction.FinalTargetVersion = transaction.RequestedTargetVersion;
                if (!(await dotStatDbService.TryNewTransaction(transaction, 
                    dataflow, transferParam.Principal, _mappingStoreDataAccess, cancellationToken)).Success)
                {
                    await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);
                    return;
                }

                //POINT OF NO RETURN
                cancellationToken = CancellationToken.None;

                await dotStatDbService.Restore(dataflow, _mappingStoreDataAccess, cancellationToken);
                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, true);
                Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TransactionSuccessful));
            }
            catch (System.Exception)
            {
                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transferParam"></param>
        /// <param name="targetDataflow"></param>
        /// <param name="managementRepository"></param>
        /// <param name="transactionRepository"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="offline">If authentication already been verified, e.g. using a message queue</param>
        /// <returns></returns>
        public async Task<bool> InitDataDbObjectsOfDataflow(
            TransferParam transferParam, 
            IDataflowMutableObject targetDataflow,
            CancellationToken cancellationToken,
            bool offline = false)
        {
            // Check of permission
            if (!offline && !_authorizationManagement.IsAuthorized(
                transferParam.Principal,
                transferParam.DestinationDataspace.Id,
                targetDataflow.AgencyId,
                targetDataflow.Id,
                targetDataflow.Version,
                PermissionType.CanImportData))
            {
                // Not authorized to manage data of dataflow
                throw new TransferUnauthorizedException();
            }

            var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace?.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace?.Id);

            // Check if dataflow exists in mappingstore database
            var dataflow = GetDataflow(transferParam.DestinationDataspace.Id, targetDataflow);

            if (dataflow == null)
            {
                // Dataflow not found in mappingstore database
                throw new ArtefactNotFoundException();
            }

            transferParam.Id = await unitOfWork.TransactionRepository.GetNextTransactionId(cancellationToken);

            LogHelper.RecordNewTransaction(transferParam.Id, transferParam.DestinationDataspace);
            Log.SetTransactionId(transferParam.Id);
            Log.SetDataspaceId(transferParam.DestinationDataspace.Id);

            try
            {
                var transaction = await unitOfWork.TransactionRepository.CreateTransactionItem(
                    transferParam.Id, dataflow.FullId, transferParam.Principal,
                    null, null, TransactionType.InitDataFlow, transferParam.TargetVersion, cancellationToken);

                if (!(await dotStatDbService.TryNewTransaction(transaction, dataflow, transferParam.Principal, _mappingStoreDataAccess, cancellationToken)).Success)
                {
                    // Transaction creation attempt failed due to an ongoing concurrent transaction
                    throw new ConcurrentTransactionException();
                }

                //Force to synchronize mappingsets
                // Closure of transaction. Includes (re-)calculation of actual content constraint
                await dotStatDbService.CloseTransaction(transaction, dataflow, false, calculateActualContentConstraint: true, includeRelatedDataFlows: false,
                        dsdSynchronizeMappingsets: true, msdSynchronizeMappingsets: true, _mappingStoreDataAccess, cancellationToken);

                return true;
            }
            catch
            {
                // If there is an unexpected error do the cleanup transaction item before passing the exception
                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);

                throw;
            }
        }

        public async Task<bool> DeleteMappingSets(
            TransferParam transferParam, 
            IDataflowMutableObject targetDataflow,
            CancellationToken cancellationToken)
        {
            // Check of permission
            if (!_authorizationManagement.IsAuthorized(
                transferParam.Principal,
                transferParam.DestinationDataspace.Id,
                targetDataflow.AgencyId,
                targetDataflow.Id,
                targetDataflow.Version,
                PermissionType.CanImportStructures))
            {
                // Not authorized to manage structures of dataflow
                throw new TransferUnauthorizedException();
            }

            var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace?.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace?.Id);

            // Check if dataflow exists in mappingstore database
            var dataflow = GetDataflow(transferParam.DestinationDataspace.Id, targetDataflow, ResolveCrossReferences.DoNotResolve);

            if (dataflow == null)
            {
                throw new ArgumentNullException(nameof(dataflow));
            }

            // Check if dataflow exists in datastore database
            dataflow.DbId = await unitOfWork.ArtefactRepository.GetArtefactDbId(dataflow, cancellationToken);

            try
            {
                //Lock with transaction only when the dataflow exists in the data database
                if (dataflow.DbId != -1)
                {
                    if (dataflow.Dsd == null)
                    {
                        throw new DotStatException(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowParameterWithoutDsd),
                            dataflow.FullId)
                        );
                    }

                    dataflow.Dsd.DbId = await unitOfWork.ArtefactRepository.GetArtefactDbId(dataflow.Dsd, cancellationToken);

                    var transaction = await unitOfWork.TransactionRepository.CreateTransactionItem(
                        transferParam.Id, 
                        dataflow.FullId, 
                        transferParam.Principal, 
                        sourceDataSpace:null, 
                        dataSource:null,
                        TransactionType.CleanupMappingSets, 
                        requestedTargetVersion:null, 
                        cancellationToken
                    );

                    if (!await dotStatDbService.TryNewTransactionForCleanup(
                            transaction,
                            dataflow.Dsd.DbId,
                            dataflow.DbId,
                            dataflow.Dsd.AgencyId, 
                            dataflow.Dsd.Code, 
                            dataflow.Dsd.Version, 
                            cancellationToken
                    ))
                    {
                        // Transaction creation attempt failed due to an ongoing concurrent transaction
                        throw new ConcurrentTransactionException();
                    }
                }

                //Delete mapping sets and data sets
                var deletedEntities =
                    _mappingStoreDataAccess.DeleteDataSetsAndMappingSets(transferParam.DestinationDataspace.Id, dataflow, deleteMsdObjectsOnly: false);

                //Close transaction only when the dataflow exists in the data database
                if (dataflow.DbId != -1)
                {
                    await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, true);
                }

                return deletedEntities;
            }
            catch
            {
                //Close transaction only when the dataflow exists in the data database
                if (dataflow.DbId != -1)
                {
                    // If there is an unexpected error do the cleanup transaction item before passing the exception
                    await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);
                }
                throw;
            }
        }

        public async Task<bool> CleanUpDsd(
            TransferParam transferParam, 
            IDataStructureMutableObject targetDsd,
            bool checkExistingDataStructure, 
            bool cleanupMsdOnly, 
            CancellationToken cancellationToken)
        {
            // Check of permission
            if (!_authorizationManagement.IsAuthorized(
                transferParam.Principal,
                transferParam.DestinationDataspace.Id,
                targetDsd.AgencyId,
                targetDsd.Id,
                targetDsd.Version,
                PermissionType.CanDeleteStructuralMetadata))
            {
                // Not authorized to manage data of dataflow
                throw new TransferUnauthorizedException();
            }

            if (checkExistingDataStructure)
            {
                var mappingStoreObjects = _mappingStoreDataAccess.GetDsd(
                    transferParam.DestinationDataspace.Id,
                    targetDsd.AgencyId,
                    targetDsd.Id,
                    targetDsd.Version);

                if (mappingStoreObjects.DataStructures.Any())
                {
                    throw new DatastructureStillExistsException();
                }
            }

            var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace?.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace?.Id);

            var dsdDbId = await unitOfWork.ArtefactRepository.GetArtefactDbId(targetDsd?.AgencyId, targetDsd?.Id, new SdmxVersion(targetDsd.Version), SDMXArtefactType.Dsd, cancellationToken);

            //If no such DSD
            if (dsdDbId == -1)
            {
                return false;
            }

            try
            {
                var transaction = await unitOfWork.TransactionRepository.CreateTransactionItem(
                    transferParam.Id, $"{targetDsd?.AgencyId}:{targetDsd?.Id}({targetDsd?.Version})", transferParam.Principal,
                    sourceDataSpace:null, dataSource:null,
                    TransactionType.CleanupDsd, requestedTargetVersion:null, cancellationToken);

                if (!await dotStatDbService.TryNewTransactionForCleanup(transaction, dsdDbId, null, targetDsd.AgencyId, targetDsd.Id, new SdmxVersion(targetDsd.Version), cancellationToken))
                {
                    // Transaction creation attempt failed due to an ongoing concurrent transaction
                    throw new ConcurrentTransactionException();
                }

                var allDbComponents = (await unitOfWork.ComponentRepository.GetAllComponents(cancellationToken)).ToList();
                var allMsdDbComponents = (await unitOfWork.MetaMetadataComponentRepository.GetAllMetadataAttributeComponents(cancellationToken)).ToList();

                await dotStatDbService.CleanUpDsd(dsdDbId, deleteMsdObjectsOnly: cleanupMsdOnly, _mappingStoreDataAccess, allDbComponents, allMsdDbComponents, cancellationToken);

                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, true);

                return true;
            }
            catch
            {
                // If there is an unexpected error do the cleanup transaction item before passing the exception
                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);

                throw;
            }
        }

        public async Task<bool> CleanUpAllOrphans(TransferParam transferParam, Transaction transaction, CancellationToken cancellationToken)
        {
            var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace?.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace?.Id);
            try
            {
                if (!await dotStatDbService.TryNewTransactionWithNoDsd(transaction, cancellationToken))
                {
                    // Transaction creation attempt failed due to an ongoing concurrent transaction
                    throw new ConcurrentTransactionException();
                }

                var targetDsds = await unitOfWork.ArtefactRepository.GetListOfArtefacts(cancellationToken, "DSD");

                var orphansFound = false;

                var allDbComponents = (await unitOfWork.ComponentRepository.GetAllComponents(cancellationToken)).ToList();
                var allMsdDbComponents = (await unitOfWork.MetaMetadataComponentRepository.GetAllMetadataAttributeComponents(cancellationToken)).ToList();

                foreach (var dsdItm in targetDsds)
                {
                    try
                    {
                        // Check of permission
                        if (!_authorizationManagement.IsAuthorized(
                            transferParam.Principal,
                            transferParam.DestinationDataspace.Id,
                            dsdItm.Agency,
                            dsdItm.Id,
                            dsdItm.Version,
                            PermissionType.CanDeleteStructuralMetadata))
                        {
                            // Not authorized to manage data of dataflow
                            Log.Notice(string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                    .CleanUpErrorUnauthorized), dsdItm));

                            continue;
                        }

                        var mappingStoreObjects = _mappingStoreDataAccess.GetDsd(
                            transferParam.DestinationDataspace.Id,
                            dsdItm.Agency,
                            dsdItm.Id,
                            dsdItm.Version);

                        if (mappingStoreObjects.DataStructures.Any())
                            continue; // Do nothing if dsd still exists in structure db

                        var dsdDbId = await unitOfWork.ArtefactRepository.GetArtefactDbId(dsdItm.Agency, dsdItm.Id,
                            new SdmxVersion(dsdItm.Version), SDMXArtefactType.Dsd, cancellationToken);

                        if (dsdDbId == -1) continue;

                        await dotStatDbService.CleanUpDsd(dsdDbId, deleteMsdObjectsOnly: false, _mappingStoreDataAccess, allDbComponents, allMsdDbComponents, cancellationToken);
                        
                        Log.Notice(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpSuccessful),
                            transferParam.DestinationDataspace.Id, 
                            dsdItm
                        ));

                        orphansFound = true;
                    }
                    catch (System.Exception)
                    {
                        Log.Notice(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpError), 
                            dsdItm
                        ));
                    }
                }

                var orphanCodelistsRemoved = await CleanUpOrphanedCodelists(dotStatDbService, unitOfWork, transferParam.Principal, cancellationToken);

                if (!orphansFound && !orphanCodelistsRemoved)
                    Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                        .NoOrphanDsdsToClean));
            }
            catch (System.Exception)
            {
                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);

                throw;
            }

            await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, true);
            return true;
        }

        private async Task<bool> CleanUpOrphanedCodelists(IDotStatDbService dotStatDbService, IUnitOfWork unitOfWork, DotStatPrincipal principal, CancellationToken cancellationToken)
        {
            // Obtain codelists from ARTEFACT table that have no relation to any components of any DSDs and MSDs in data database.
            var codelistArtefacts = await unitOfWork.ArtefactRepository.GetListOfCodelistsWithoutDsdAndMsd(cancellationToken);

            if (!codelistArtefacts.Any())
            {
                return false;
            }

            // Delete codelists from datat database that have no relation to any DSD nor MSD
            foreach (var cl in codelistArtefacts)
            {
                try
                {
                    // Check of permission
                    if (!_authorizationManagement.IsAuthorized(
                        principal,
                        unitOfWork.DataSpace,
                        cl.Agency,
                        cl.Id,
                        cl.Version,
                        PermissionType.CanDeleteStructuralMetadata))
                    {
                        // Not authorized to manage data of dataflow
                        Log.Notice(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                .CleanUpErrorUnauthorizedCodelist), cl));

                        continue;
                    }

                    if (await unitOfWork.ArtefactRepository.CleanUpCodelist(cl.DbId, cancellationToken))
                    {
                        Log.Notice(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpSuccessfulCodelist),
                            unitOfWork.DataSpace,
                            cl
                        ));
                    }
                }
                catch (System.Exception)
                {
                    Log.Notice(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpErrorCodelist),
                        cl
                    ));
                }
            }

            return true;
        }

        public bool IsAuthorized(ITransferParam transferParam, Dataflow dataflow)
        {
            return _authorizationManagement.IsAuthorized(
                transferParam.Principal,
                transferParam.DestinationDataspace.Id,
                dataflow.AgencyId,
                dataflow.Base.Id,
                dataflow.Version.ToString(),
                PermissionType.CanImportData
            );
        }

        public Dataflow GetDataflow(string sourceDataSpaceId, IDataflowMutableObject sourceDataflow, ResolveCrossReferences resolveCrossReferences = ResolveCrossReferences.ResolveExcludeAgencies)
        {
            var dataflow = _mappingStoreDataAccess.GetDataflow(
                sourceDataSpaceId,
                sourceDataflow.AgencyId,
                sourceDataflow.Id,
                sourceDataflow.Version,
                true,
                resolveCrossReferences);

            Log.Notice(
                string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowLoaded),
                    dataflow.FullId,
                    sourceDataSpaceId));

            return dataflow;
        }
    }
}
