﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Domain;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;

namespace DotStat.Transfer.Manager
{
    public abstract class TransferManager<T> : ITransferManager<T> where T : ITransferParam
    {
        public BaseConfiguration Configuration { get; set; }
        public IProducer<T> Producer { get; set; }
        public IConsumer<T> Consumer { get; set; }

        protected TransferManager(BaseConfiguration configuration, IProducer<T> producer, IConsumer<T> consumer)
        {
            Configuration = configuration;
            Producer = producer;
            Consumer = consumer;
        }

        public abstract Task Transfer(T transferParam, Transaction transaction, CancellationToken cancellationToken);

        public bool HasComponentsToProcess(Dataflow dataFlow, TransferContent transferContent)
        {
            if (transferContent.ReportedComponents.IsMetadataReported())
            {
                if (transferContent.ReportedComponents.MetadataAttributes != null && transferContent.ReportedComponents.MetadataAttributes.Any())
                {
                    return true;
                }

                Log.Warn(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.SourceFileHasNoComponentsToProcess));
                return false;
            }

            var allReportedAttributes = transferContent.ReportedComponents.DatasetAttributes
                //Attributes not attached to the time dimension
                .Concat(transferContent.ReportedComponents.SeriesAttributesWithNoTimeDim)
                //Attributes attached to the time dimension
                .Concat(transferContent.ReportedComponents.ObservationAttributes).ToList();

            //Nothing to do when the source file only contains the dataFlow and dimension columns
            if (allReportedAttributes.Count == 0  && !transferContent.ReportedComponents.IsPrimaryMeasureReported)
            {
                Log.Warn(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.SourceFileHasNoComponentsToProcess));
                return false;
            }

            var nonReportedComponents = dataFlow.Dsd.Attributes.Except(allReportedAttributes).Select(a => a.Code).ToList();

            if (!transferContent.ReportedComponents.IsPrimaryMeasureReported)
            {
                nonReportedComponents.Add(dataFlow.Dsd.PrimaryMeasure.Code);
            }

            if (nonReportedComponents.Count > 0)
            {
                Log.Warn(string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NotIncludedComponents), string.Join(", ", nonReportedComponents)));
            }

            return true;
        }
    }
}