﻿using DotStat.Domain;
using DotStat.Transfer.Param;
using System.Threading;
using System.Threading.Tasks;

namespace DotStat.Transfer.Consumer
{
    public interface IConsumer<in T> where T : ITransferParam
    {
        Task<bool> Save(T transferParam, Transaction transaction, Dataflow dataflow, TransferContent transferContent, CancellationToken cancellation);

        bool IsAuthorized(ITransferParam transferParam, Dataflow dataflow);
    }
}