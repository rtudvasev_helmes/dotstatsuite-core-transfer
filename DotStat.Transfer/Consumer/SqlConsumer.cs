﻿using System;
using System.Linq;
using DotStat.Common.Auth;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Exception;
using DotStat.Db.Util;
using DotStat.DB.Util;
using DotStat.Db.Validation;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.DataflowManager;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using Estat.Sdmxsource.Extension.Constant;
using System.Threading.Tasks;
using System.Threading;
using DotStat.Db.DB;
using DotStat.DB.Repository;
using DotStat.Db.Service;
using DotStat.Common.Exceptions;
using System.Transactions;
using System.Collections.Generic;

namespace DotStat.Transfer.Consumer
{
    public class SqlConsumer : IConsumer<ITransferParam>, IDataflowManager<ISqlTransferParam>
    {
        protected readonly IAuthorizationManagement AuthorizationManagement;
        protected readonly IGeneralConfiguration GeneralConfiguration;
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly DotStatDbResolver _dotStatDbResolver;
        private readonly UnitOfWorkResolver _unitOfWorkResolver;
        private readonly DotStatDbServiceResolver _dotStatDbServiceResolver;
        private readonly IDatasetAttributeDatabaseValidator _datasetAttributeDatabaseValidator;
        private readonly IDatasetAttributeValidator _datasetAttributeValidator;
        private readonly IKeyableDatabaseValidator _keyableDatabaseValidator;
        public SqlConsumer(
            IAuthorizationManagement authorizationManagement, 
            IMappingStoreDataAccess mappingStoreDataAccess,
            IGeneralConfiguration generalConfiguration,
            DotStatDbResolver dotStatDbResolver,
            UnitOfWorkResolver unitOfWorkResolver,
            DotStatDbServiceResolver dotStatDbServiceResolver,
            IDatasetAttributeDatabaseValidator datasetAttributeDatabaseValidator,
            IDatasetAttributeValidator datasetAttributeValidator,
            IKeyableDatabaseValidator keyableDatabaseValidator)
        {
            AuthorizationManagement = authorizationManagement;
            GeneralConfiguration = generalConfiguration;
            _mappingStoreDataAccess = mappingStoreDataAccess;
            _dotStatDbResolver = dotStatDbResolver;
            _unitOfWorkResolver = unitOfWorkResolver;
            _dotStatDbServiceResolver = dotStatDbServiceResolver;
            _datasetAttributeDatabaseValidator = datasetAttributeDatabaseValidator;
            _datasetAttributeValidator = datasetAttributeValidator;
            _keyableDatabaseValidator = keyableDatabaseValidator;

            if (_mappingStoreDataAccess == null)
            {
                throw new ArgumentNullException(nameof(_mappingStoreDataAccess));
            }
            if (_dotStatDbResolver == null)
            {
                throw new ArgumentNullException(nameof(_dotStatDbResolver));
            }
            if (_unitOfWorkResolver == null)
            {
                throw new ArgumentNullException(nameof(_unitOfWorkResolver));
            }
            if (_dotStatDbServiceResolver == null)
            {
                throw new ArgumentNullException(nameof(_dotStatDbServiceResolver));
            }
            if (_datasetAttributeDatabaseValidator == null)
            {
                throw new ArgumentNullException(nameof(_datasetAttributeDatabaseValidator));
            }

            if (_datasetAttributeValidator == null)
            {
                throw new ArgumentNullException(nameof(_datasetAttributeValidator));
            }

            if (_keyableDatabaseValidator == null)
            {
                throw new ArgumentNullException(nameof(_keyableDatabaseValidator));
            }
        }

        public async Task<bool> Save(
            ITransferParam transferParam,
            Domain.Transaction transaction,
            Dataflow dataFlow, 
            TransferContent transferContent,
            CancellationToken cancellationToken)
        {
            var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace?.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace?.Id);

            var codeTranslator = new CodeTranslator(unitOfWork.CodeListRepository);

            try
            {
                //check transaction and create one if none exists for the dsd of dataFlow.
                //also creates tables/rows for missing artefacts and new data version, populate dataFlow and children with database id-s 
                var tryNewTransactionResult = await dotStatDbService.TryNewTransaction(transaction, dataFlow, transferParam.Principal, _mappingStoreDataAccess, cancellationToken);
                if (!tryNewTransactionResult.Success)
                {
                    await dotStatDbService.CleanUpFailedTransaction(transaction);
                    return false;
                }

                if (transferParam.TransactionType == TransactionType.Transfer || transferParam.TransactionType == TransactionType.ValidateTransfer)
                {
                    // Replace items in list of reported components with components from destination space to have the proper db ids
                    transferContent.ReportedComponents = GetDestinationReportedComponents(transferContent.ReportedComponents, dataFlow);
                }

                transferParam.TargetVersion = (TargetVersion)transaction.FinalTargetVersion;

                //The dataFlow gets populated in TryNewTransaction, update codeTranslator codelists
                await codeTranslator.FillDict(dataFlow, cancellationToken);
                
                //Execute PIT Release if there is one pending 
                if (dataFlow.Dsd.PITVersion != null //Is there a PIT?
                    && (dataFlow.Dsd.PITReleaseDate != null && dataFlow.Dsd.PITReleaseDate <= DateTime.Now)) //PIT release date has passed
                {
                    var previousPITReleaseDate = dataFlow.Dsd.PITReleaseDate;

                    await dotStatDbService.ApplyPITRelease(dataFlow, transferParam.PITRestorationAllowed, _mappingStoreDataAccess);

                    if (!dataFlow.Dsd.LiveVersion.HasValue)
                    {
                        throw new DotStatException(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoValueSetForDsdLiveVersion));
                    }

                    var newDbTargetVersion = transferParam.TargetVersion == TargetVersion.Live
                        ? (DbTableVersion)dataFlow.Dsd.LiveVersion
                        : DbTableVersions.GetNewTableVersion((DbTableVersion)dataFlow.Dsd.LiveVersion);

                    await unitOfWork.TransactionRepository.UpdateTableVersionOfTransaction(transferParam.Id, newDbTargetVersion, cancellationToken);

                    Log.Debug(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TargetVersionInDBChanged),
                        transferParam.TargetVersion, newDbTargetVersion, previousPITReleaseDate.ToString())
                    );
                }

                if (transferParam.TargetVersion != TargetVersion.Live)
                {
                    dataFlow.Dsd.PITReleaseDate = transferParam.PITReleaseDate;
                }

                DbTableVersion targetTableVersion;

                //Copy data to PIT version, for the first pit load.
                if (transferParam.TargetVersion == TargetVersion.PointInTime && dataFlow.Dsd.PITVersion == null)
                {
                    //Assign new table version to PITVersion
                    dataFlow.Dsd.PITVersion = dataFlow.Dsd.LiveVersion == null ?
                        (char)DbTableVersion.A : (char)DbTableVersions.GetNewTableVersion((DbTableVersion)dataFlow.Dsd.LiveVersion);
                    targetTableVersion = (DbTableVersion)dataFlow.Dsd.PITVersion;
                    var sourceTableVersion = DbTableVersions.GetNewTableVersion(targetTableVersion);

                    if (transferParam.PITRestorationAllowed)
                        dataFlow.Dsd.PITRestorationDate = DateTime.Now;
                    //Cleanup target PIT tables
                    await unitOfWork.DataStoreRepository.DeleteData(dataFlow.Dsd, targetTableVersion, cancellationToken);
                    //Cleanup target PIT tables
                    await unitOfWork.MetadataStoreRepository.DeleteMetadata(dataFlow.Dsd, targetTableVersion, cancellationToken);
                    //Copy data to new data version
                    await unitOfWork.DataStoreRepository.CopyDataToNewVersion(dataFlow.Dsd, sourceTableVersion, targetTableVersion, cancellationToken);
                    //Copy data to new data version
                    await unitOfWork.MetadataStoreRepository.CopyMetadataToNewVersion(dataFlow, sourceTableVersion, targetTableVersion, cancellationToken);
                    //Copy attributes to new data version
                    await unitOfWork.DataStoreRepository.CopyAttributesToNewVersion(dataFlow.Dsd, sourceTableVersion, targetTableVersion, cancellationToken);
                }
                else if (transferParam.TargetVersion == TargetVersion.PointInTime && dataFlow.Dsd.PITVersion != null)
                {
                    targetTableVersion = (DbTableVersion)dataFlow.Dsd.PITVersion;
                }
                else
                {
                    //First data load to live version
                    if (dataFlow.Dsd.LiveVersion == null)
                        //Assign new table version to liveVersion
                        dataFlow.Dsd.LiveVersion = dataFlow.Dsd.PITVersion == null ?
                        (char)DbTableVersion.A : (char)DbTableVersions.GetNewTableVersion((DbTableVersion)dataFlow.Dsd.PITVersion);

                    targetTableVersion = (DbTableVersion)dataFlow.Dsd.LiveVersion;
                }

                var isTimeAtTimeDimensionSupported = await unitOfWork.ArtefactRepository.CheckSupportOfTimeAtTimeDimension(dataFlow.Dsd, targetTableVersion, cancellationToken);

                var importSummary = new ImportSummary();
                switch (transferParam.TransferType)
                {
                    //Stage data or metadata and merge to final tables
                    case TransferType.DataAndMetadata:
                    {
                        importSummary = await SaveData(transferParam, dataFlow, transferContent, unitOfWork, codeTranslator, targetTableVersion, isTimeAtTimeDimensionSupported, cancellationToken);

                        await SaveMetadata(transferParam, dataFlow, transferContent, unitOfWork, codeTranslator, targetTableVersion, isTimeAtTimeDimensionSupported, cancellationToken);
                        break;
                    }
                    case TransferType.DataOnly:
                    {
                        importSummary = await SaveData(transferParam, dataFlow, transferContent, unitOfWork, codeTranslator, targetTableVersion, isTimeAtTimeDimensionSupported, cancellationToken);
                        break;
                    }
                    case TransferType.MetadataOnly:
                    {
                        await SaveMetadata(transferParam, dataFlow, transferContent, unitOfWork, codeTranslator, targetTableVersion, isTimeAtTimeDimensionSupported, cancellationToken);
                        break;
                    }
                }

                //Close the transaction 
                if (transferParam.ValidationType == ValidationType.FullValidationOnly)
                {
                    await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, true);
                }
                else
                {
                    Log.Notice(LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.StartedCalculatingAvailability,
                            transferParam.CultureInfo.TwoLetterISOLanguageName));

                    //Close transaction, update ACC and mapping sets
                    await dotStatDbService.CloseTransaction(transaction, 
                        dataFlow, transferParam.PITRestorationAllowed, importSummary.ObservationsCount > 0, includeRelatedDataFlows: true,
                        tryNewTransactionResult.DsdMappingSetsSynchronizationRequired, tryNewTransactionResult.MsdMappingSetsSynchronizationRequired,
                        _mappingStoreDataAccess, cancellationToken);
                }
            }
            catch (System.Exception)
            {
                await dotStatDbService.CleanUpFailedTransaction(transaction);
                throw;
            }

            return true;
        }
        
        public Dataflow GetDataflow(ISqlTransferParam transferParam, bool throwErrorIfNotFound= true)
        {
            var dataFlow = _mappingStoreDataAccess.GetDataflow(
                transferParam.DestinationDataspace.Id,
                transferParam.DestinationDataflow.AgencyId,
                transferParam.DestinationDataflow.Id,
                transferParam.DestinationDataflow.Version,
                throwErrorIfNotFound
            );

            //Set log dataspaceId and TransactionId?
            Log.Notice( string.Format(
                    LocalizationRepository.GetLocalisedResource(
                        Localization.ResourceId.DataflowLoaded,
                        transferParam.CultureInfo.TwoLetterISOLanguageName),
                    dataFlow.FullId,
                    transferParam.DestinationDataspace.Id));

            return dataFlow;
        }

        public bool IsAuthorized(ITransferParam transferParam, Dataflow dataFlow)
        {
            return AuthorizationManagement.IsAuthorized(
                transferParam.Principal,
                transferParam.DestinationDataspace.Id,
                dataFlow.AgencyId,
                dataFlow.Base.Id,
                dataFlow.Version.ToString(),
                PermissionType.CanImportData
            ) || AuthorizationManagement.IsAuthorized(
                transferParam.Principal,
                transferParam.DestinationDataspace.Id,
                dataFlow.AgencyId,
                dataFlow.Base.Id,
                dataFlow.Version.ToString(),
                PermissionType.CanUpdateData
            ) || AuthorizationManagement.IsAuthorized(
                transferParam.Principal,
                transferParam.DestinationDataspace.Id,
                dataFlow.AgencyId,
                dataFlow.Base.Id,
                dataFlow.Version.ToString(),
                PermissionType.CanDeleteData
            );
        }

        //Check user rights depending on the content being written to the destination dataspace
        private bool IsAuthorized(ITransferParam transferParam, Dataflow dataFlow, bool hasMergeAction, bool hasDeleteAction)
        {
            if (hasDeleteAction &&
                !AuthorizationManagement.IsAuthorized(
                    transferParam.Principal,
                    transferParam.DestinationDataspace.Id,
                    dataFlow.AgencyId,
                    dataFlow.Base.Id,
                    dataFlow.Version.ToString(),
                    PermissionType.CanDeleteData)
             )
                return false;

            if (hasMergeAction &&
                !AuthorizationManagement.IsAuthorized(
                    transferParam.Principal,
                    transferParam.DestinationDataspace.Id,
                    dataFlow.AgencyId,
                    dataFlow.Base.Id,
                    dataFlow.Version.ToString(),
                    PermissionType.CanImportData) &&
                !AuthorizationManagement.IsAuthorized(
                    transferParam.Principal,
                    transferParam.DestinationDataspace.Id,
                    dataFlow.AgencyId,
                    dataFlow.Base.Id,
                    dataFlow.Version.ToString(),
                    PermissionType.CanUpdateData)
            )
                return false;

            return true;
        }
        
        private async Task<ImportSummary> SaveMetadata(
            ITransferParam transferParam,
            Dataflow dataflow,
            TransferContent transferContent,
            IUnitOfWork unitOfWork,
            ICodeTranslator codeTranslator,
            DbTableVersion targetTableVersion,
            bool isTimeAtTimeDimensionSupported,
            CancellationToken cancellationToken)
        {
            var importSummary = new ImportSummary();

            if (dataflow.Dsd.Msd == null)
            {
                return importSummary;
            }

            // Wipe and/or create staging table and target tables
            await unitOfWork.MetadataStoreRepository.RecreateMetadataStagingTables(dataflow, transferContent.ReportedComponents, isTimeAtTimeDimensionSupported, cancellationToken);

            Log.Notice(transferParam.ValidationType == ValidationType.FullValidationOnly
                ? LocalizationRepository.GetLocalisedResource(Localization.ResourceId.StartedValidatingMetadata, transferParam.CultureInfo.TwoLetterISOLanguageName)
                : LocalizationRepository.GetLocalisedResource(Localization.ResourceId.StartedReadingAndValidatingMetadata, transferParam.CultureInfo.TwoLetterISOLanguageName));

            // Load observation metadata staging table
            // Validate metadata attributes in the import file
            var bulkImportResult = await unitOfWork.MetadataStoreRepository.BulkInsertMetadata(transferContent.MetadataObservations, transferContent.ReportedComponents, codeTranslator as CodeTranslator,
                    dataflow, transferParam.ValidationType != ValidationType.ImportWithBasicValidation, isTimeAtTimeDimensionSupported, cancellationToken);

            var hasMerges = bulkImportResult.BatchActions.Any(a=> a.Value.Action == StagingRowActionEnum.Merge);
            var hasDeletions = bulkImportResult.BatchActions.Any(a => a.Value.Action == StagingRowActionEnum.Delete);
            //Validate user rights for dataset actions found in the existing data (only for transfer data across spaces).
            if (!IsAuthorized(transferParam, dataflow, hasMerges, hasDeletions))
            {
                throw new TransferUnauthorizedException();
            }

            //TODO fine tune, or make it configurable, the amount of observations at which 
            //At which is beneficial to add an index to the staging table
            if (bulkImportResult.RowsCopied >= 3000000 &&
                transferParam.ValidationType != ValidationType.FullValidationOnly)
            {
                await unitOfWork.MetadataStoreRepository.AddIndexMetadataStagingTable(dataflow, transferContent.ReportedComponents, cancellationToken);
            }

            if (bulkImportResult.Errors.Count > 0)
            {
                throw new ConsumerValidationException(bulkImportResult.Errors);
            }

            if (transferParam.ValidationType != ValidationType.FullValidationOnly)
            {
                //Merge data only for imports/transfer
                var includeSummary = transferParam.ValidationType == ValidationType.ImportWithFullValidation;

                importSummary = await unitOfWork.MetadataStoreRepository.MergeStagingTable(dataflow, transferContent.ReportedComponents,
                    bulkImportResult.BatchActions, bulkImportResult.DataSetLevelAttributeRows, codeTranslator as CodeTranslator, targetTableVersion, includeSummary,
                    cancellationToken);

                importSummary.ObservationsCount = bulkImportResult.RowsCopied;

                if (importSummary.Errors.Count > 0)
                {
                    bulkImportResult.Errors.AddRange(importSummary.Errors);
                    throw new ConsumerValidationException(bulkImportResult.Errors);
                }
            }

            //POINT OF NO RETURN
            cancellationToken = CancellationToken.None;

            //set transaction ready for validation
            await unitOfWork.TransactionRepository.MarkTransactionReadyForValidation(transferParam.Id, cancellationToken);

            //Clean restoration
            if (transferParam.TargetVersion == TargetVersion.Live)
            {
                await unitOfWork.MetadataStoreRepository.DeleteMetadata(dataflow.Dsd, DbTableVersions.GetNewTableVersion((DbTableVersion)dataflow.Dsd.LiveVersion), cancellationToken);
            }

            //Log result
            switch (transferParam.ValidationType)
            {
                case ValidationType.ImportWithFullValidation:
                    Log.Notice(string.Format(
                        LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.MetaDataObservationsProcessedDetails, 
                            transferParam.CultureInfo.TwoLetterISOLanguageName), 
                        importSummary.ObservationsCount, 
                        importSummary.ObservationLevelMergeResult.InsertCount,
                        importSummary.ObservationLevelMergeResult.UpdateCount,
                        importSummary.ObservationLevelMergeResult.DeleteCount));
                    break;
                case ValidationType.ImportWithBasicValidation:
                    Log.Notice(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MetaDataObservationsProcessedNoDetails, transferParam.CultureInfo.TwoLetterISOLanguageName),
                        importSummary.ObservationLevelMergeResult.TotalCount));
                    break;
                case ValidationType.FullValidationOnly:
                    Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ValidationSucceeded, transferParam.CultureInfo.TwoLetterISOLanguageName));
                    break;
            }

            //Drop staging tables
            if (transferParam.ValidationType == ValidationType.FullValidationOnly)
            {
                await unitOfWork.MetadataStoreRepository.DropMetadataStagingTables(dataflow.Dsd.DbId, cancellationToken);
            }

            return importSummary;
        }

        private async Task<ImportSummary> SaveData(
            ITransferParam transferParam,
            Dataflow dataflow,
            TransferContent transferContent,
            IUnitOfWork unitOfWork,
            ICodeTranslator codeTranslator,
            DbTableVersion targetTableVersion,
            bool isTimeAtTimeDimensionSupported,
            CancellationToken cancellationToken)
        {
            var fullValidation = transferParam.ValidationType == ValidationType.FullValidationOnly ||
                                 transferParam.ValidationType == ValidationType.ImportWithFullValidation;


            // Wipe and/or create staging table and target tables
            await unitOfWork.DataStoreRepository.RecreateStagingTables(dataflow.Dsd, transferContent.ReportedComponents, isTimeAtTimeDimensionSupported, cancellationToken);

            var maxErrorCount = GeneralConfiguration.MaxTransferErrorAmount;

            if (transferParam.ValidationType == ValidationType.FullValidationOnly)
                Log.Notice(LocalizationRepository.GetLocalisedResource(
                        Localization.ResourceId.StartedValidating,
                        transferParam.CultureInfo.TwoLetterISOLanguageName));
            else
                Log.Notice(LocalizationRepository.GetLocalisedResource(
                        Localization.ResourceId.StartedReadingAndValidating,
                        transferParam.CultureInfo.TwoLetterISOLanguageName));

            // Load observation data and attributes into staging table
            // Validate observation and attributes in the import file
            var bulkImportResult =
                await unitOfWork.DataStoreRepository.BulkInsertData(transferContent.DataObservations, transferContent.ReportedComponents, codeTranslator as CodeTranslator,
                    dataflow, transferParam.ValidationType != ValidationType.ImportWithBasicValidation, isTimeAtTimeDimensionSupported,
                    isTransfer: transferParam.TransactionType == TransactionType.Transfer, cancellationToken);

            var hasMerges = bulkImportResult.BatchActions.Any(a => a.Value.Action == StagingRowActionEnum.Merge);
            var hasReplaces = bulkImportResult.BatchActions.Any(a => a.Value.Action == StagingRowActionEnum.Replace);
            var hasDeletions = bulkImportResult.BatchActions.Any(a => a.Value.Action == StagingRowActionEnum.Delete);
            //Validate user rights for dataset actions found in the existing data (only for transfer data across spaces).
            if (!IsAuthorized(transferParam, dataflow, hasMerges, hasDeletions))
            {
                throw new TransferUnauthorizedException();
            }

            //TODO fine tune, or make it configurable, the amount of observations at which 
            //At which is beneficial to add an index to the staging table
            if ((bulkImportResult.RowsCopied >= 3000000 || hasReplaces || hasDeletions) && transferParam.ValidationType != ValidationType.FullValidationOnly)
                await unitOfWork.DataStoreRepository.AddIndexStagingTable(dataflow.Dsd, transferContent.ReportedComponents, cancellationToken);

            var datasetAttributeKeyValues = transferContent.DatasetAttributes;
            var reportedAttributes = transferContent.ReportedComponents.DatasetAttributes;

            // Validate dataset lvl attributes in the import file
            if (bulkImportResult.Errors.Count < maxErrorCount || maxErrorCount == 0)
            {
                _datasetAttributeValidator.Validate(reportedAttributes, datasetAttributeKeyValues,
                    bulkImportResult.DataSetLevelAttributeRows, dataflow,
                    maxErrorCount > 0 ? maxErrorCount - bulkImportResult.Errors.Count : 0, fullValidation);

                bulkImportResult.Errors.AddRange(_datasetAttributeValidator.GetErrors());
            }

            var dotStatDb = _dotStatDbResolver(transferParam.DestinationDataspace?.Id);
            // Validate non-dataset lvl attributes values in staging table and in database
            if (transferParam.ValidationType != ValidationType.ImportWithBasicValidation)
            {
                if (bulkImportResult.Errors.Count < maxErrorCount || maxErrorCount == 0)
                {
                    await _keyableDatabaseValidator.Validate(dotStatDb, codeTranslator, dataflow, reportedAttributes, targetTableVersion,
                        maxErrorCount > 0 ? maxErrorCount - bulkImportResult.Errors.Count : 0, cancellationToken);
                    bulkImportResult.Errors.AddRange(_keyableDatabaseValidator.GetErrors());
                }
            }

            // Merge all attributes found
            var allDataSetAttributeRows = bulkImportResult.DataSetLevelAttributeRows.MergeToDict(datasetAttributeKeyValues);

            // Validate dataset lvl attributes database
            if (bulkImportResult.Errors.Count < maxErrorCount || maxErrorCount == 0)
            {
                await _datasetAttributeDatabaseValidator.Validate(dotStatDb, dataflow, targetTableVersion,
                    reportedAttributes, allDataSetAttributeRows.Select(a => a.Value).ToList(),
                    maxErrorCount > 0 ? maxErrorCount - bulkImportResult.Errors.Count : 0, fullValidation,
                    cancellationToken);

                bulkImportResult.Errors.AddRange(_datasetAttributeDatabaseValidator.GetErrors());
            }

            if (bulkImportResult.Errors.Count > 0)
            {
                throw new ConsumerValidationException(bulkImportResult.Errors);
            }

            //Add transaction scope
            var importSummary = new ImportSummary();
            if (transferParam.ValidationType != ValidationType.FullValidationOnly)
            {
                Log.Notice(LocalizationRepository.GetLocalisedResource(
                        Localization.ResourceId.StartedWritingData,
                        transferParam.CultureInfo.TwoLetterISOLanguageName));

                //Merge data only for imports/transfer
                var includeSummary = transferParam.ValidationType == ValidationType.ImportWithFullValidation;

                importSummary = await unitOfWork.DataStoreRepository.MergeStagingTable(dataflow, transferContent.ReportedComponents,
                    bulkImportResult.BatchActions, allDataSetAttributeRows.Select(a => a.Value).ToList(), codeTranslator as CodeTranslator, targetTableVersion, includeSummary,
                    cancellationToken);

                importSummary.ObservationsCount = bulkImportResult.RowsCopied;

                if (importSummary.Errors.Count > 0)
                {
                    bulkImportResult.Errors.AddRange(importSummary.Errors);
                    throw new ConsumerValidationException(bulkImportResult.Errors);
                }
            }

            //POINT OF NO RETURN
            cancellationToken = CancellationToken.None;

            //set transaction ready for validation
            await unitOfWork.TransactionRepository.MarkTransactionReadyForValidation(transferParam.Id, cancellationToken);

            //Clean restoration
            if (transferParam.TargetVersion == TargetVersion.Live)
            {
                await unitOfWork.DataStoreRepository.DeleteData(dataflow.Dsd,
                    DbTableVersions.GetNewTableVersion((DbTableVersion)dataflow.Dsd.LiveVersion), cancellationToken);
            }

            //Log result
            switch (transferParam.ValidationType)
            {
                case ValidationType.ImportWithFullValidation:
                    var obsLevelDetails = string.Format(
                        LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.DataObservationsProcessedDetailsObsLevel,
                            transferParam.CultureInfo.TwoLetterISOLanguageName), 
                        importSummary.ObservationLevelMergeResult.InsertCount,
                        importSummary.ObservationLevelMergeResult.UpdateCount,
                        importSummary.ObservationLevelMergeResult.DeleteCount);

                    var seriesLevelDetails = string.Format(
                        LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.DataObservationsProcessedDetailsSeriesLevel,
                            transferParam.CultureInfo.TwoLetterISOLanguageName),
                        importSummary.SeriesLevelMergeResult.InsertCount,
                        importSummary.SeriesLevelMergeResult.UpdateCount,
                        importSummary.SeriesLevelMergeResult.DeleteCount);

                    var dataSetLevelDetails = string.Format(
                        LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.DataObservationsProcessedDetailsDataFlowLevel,
                            transferParam.CultureInfo.TwoLetterISOLanguageName),
                        importSummary.DataFlowLevelMergeResult.InsertCount,
                        importSummary.DataFlowLevelMergeResult.UpdateCount,
                        importSummary.DataFlowLevelMergeResult.DeleteCount);

                    Log.Notice(string.Format(
                        LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.DataObservationsProcessedDetails,
                            transferParam.CultureInfo.TwoLetterISOLanguageName), importSummary.ObservationsCount,
                        obsLevelDetails, seriesLevelDetails, dataSetLevelDetails));
                    break;
                case ValidationType.ImportWithBasicValidation:
                    Log.Notice(string.Format(
                        LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.DataObservationsProcessedNoDetails,
                            transferParam.CultureInfo.TwoLetterISOLanguageName), importSummary.ObservationsCount));
                    break;
                case ValidationType.FullValidationOnly:
                    Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ValidationSucceeded,
                            transferParam.CultureInfo.TwoLetterISOLanguageName));
                    break;
            }

            if (transferParam.ValidationType == ValidationType.FullValidationOnly)
            {
                //Close transaction and drop staging tables
                await unitOfWork.DataStoreRepository.DropStagingTables(dataflow.Dsd.DbId, cancellationToken);
            }

            return importSummary;
        }

        /// <summary>
        /// Creates a new ReportedComponents instance using the component objects of the destination dataflow's DSD, 
        /// in the order they appear in the DSD.
        /// </summary>
        /// <param name="sourceReportedComponents">The reported source components</param>
        /// <param name="destinationDataflow">The destination dataflow objext</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        private static ReportedComponents GetDestinationReportedComponents(ReportedComponents sourceReportedComponents, Dataflow destinationDataflow)
        {
            if (sourceReportedComponents == null)
                return null;

            if (destinationDataflow == null)
                throw new ArgumentNullException(nameof(destinationDataflow));

            if (destinationDataflow.Dsd == null)
                throw new ArgumentNullException(nameof(destinationDataflow.Dsd));

            var dsd = destinationDataflow.Dsd;

            var reportedComponents = new ReportedComponents();

            foreach (var destDimension in dsd.Dimensions)
            {
                var sourceDimension = sourceReportedComponents.Dimensions.FirstOrDefault(d => d.Code.Equals(destDimension.Code));
                // Add to dimension list in case a matching source component is found
                if (sourceDimension is not null)
                    reportedComponents.Dimensions.Add(destDimension);
            }

            if (sourceReportedComponents.TimeDimension is not null)
                reportedComponents.TimeDimension = dsd.TimeDimension;

            var dsdAttributes = dsd.Attributes.ToList();
            foreach (var destAttribute in dsdAttributes)
            {
                var sourceAttribute = sourceReportedComponents.DatasetAttributes.FirstOrDefault(a => a.Code.Equals(destAttribute.Code));
                // Add to attribute list in case a matching source component is found
                if (sourceAttribute is not null)
                {
                    reportedComponents.DatasetAttributes.Add(destAttribute);
                    continue;
                }

                //Attributes not attached to the time dimension
                sourceAttribute = sourceReportedComponents.SeriesAttributesWithNoTimeDim.FirstOrDefault(a => a.Code.Equals(destAttribute.Code));
                // Add to attribute list in case a matching source component is found
                if (sourceAttribute is not null)
                {
                    reportedComponents.SeriesAttributesWithNoTimeDim.Add(destAttribute);
                    continue;
                }

                //Attributes attached to the time dimension
                sourceAttribute = sourceReportedComponents.ObservationAttributes.FirstOrDefault(a => a.Code.Equals(destAttribute.Code));
                // Add to attribute list in case a matching source component is found
                if (sourceAttribute is not null)
                {
                    reportedComponents.ObservationAttributes.Add(destAttribute);
                }
            }

            var msd = destinationDataflow.Dsd.Msd;
            if (msd is not null)
            {
                foreach (var destAttribute in msd.MetadataAttributes)
                {
                    var sourceAttribute = sourceReportedComponents.MetadataAttributes.FirstOrDefault(a => a.Code.Equals(destAttribute.Code));

                    // Add to attribute list in case a matching source component is found
                    if (sourceAttribute is not null)
                        reportedComponents.MetadataAttributes.Add(destAttribute);
                }
            }

            reportedComponents.IsPrimaryMeasureReported = sourceReportedComponents.IsPrimaryMeasureReported;

            return reportedComponents;
        }
    }
}