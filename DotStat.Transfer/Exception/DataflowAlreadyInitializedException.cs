﻿
using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Transfer.Exception
{
    public class DataflowAlreadyInitializedException : DotStatException
    {
        [ExcludeFromCodeCoverage]
        public DataflowAlreadyInitializedException()
        {
        }

        [ExcludeFromCodeCoverage]
        public DataflowAlreadyInitializedException(string message) : base(message)
        {
        }

        [ExcludeFromCodeCoverage]
        public DataflowAlreadyInitializedException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
