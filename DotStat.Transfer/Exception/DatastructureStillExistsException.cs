﻿
using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Transfer.Exception
{
    public class DatastructureStillExistsException : DotStatException
    {
        [ExcludeFromCodeCoverage]
        public DatastructureStillExistsException()
        {
        }

        [ExcludeFromCodeCoverage]
        public DatastructureStillExistsException(string message) : base(message)
        {
        }

        [ExcludeFromCodeCoverage]
        public DatastructureStillExistsException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
