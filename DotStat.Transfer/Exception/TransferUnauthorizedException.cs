﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Transfer.Exception
{
    public class TransferUnauthorizedException : DotStatException
    {
        [ExcludeFromCodeCoverage]
        public TransferUnauthorizedException()
        {
        }

        [ExcludeFromCodeCoverage]
        public TransferUnauthorizedException(string message) : base(message)
        {
        }

        [ExcludeFromCodeCoverage]
        public TransferUnauthorizedException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
