﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Transfer.Exception
{
    public class TransferValidationException : DotStatException
    {
        [ExcludeFromCodeCoverage]
        public TransferValidationException()
        {
        }

        [ExcludeFromCodeCoverage]
        public TransferValidationException(string message) : base(message)
        {
        }

        [ExcludeFromCodeCoverage]
        public TransferValidationException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
