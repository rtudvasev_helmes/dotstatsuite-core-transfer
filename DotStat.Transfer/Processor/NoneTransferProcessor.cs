﻿using DotStat.Domain;

namespace DotStat.Transfer.Processor
{
    public class NoneTransferProcessor : ITransferProcessor
    {
        public TransferContent Process(Dataflow sourceDataflow, Dataflow destinationDataflow, TransferContent transferContent)
        {
            return transferContent;
        }
    }
}
