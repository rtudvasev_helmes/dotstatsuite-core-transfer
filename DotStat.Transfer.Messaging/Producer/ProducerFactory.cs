﻿using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using System.Diagnostics.CodeAnalysis;

namespace DotStat.Transfer.Messaging.Producer
{

    [ExcludeFromCodeCoverage]
    public static class ProducerFactory
    {
        public static IProducer Create(IConfiguration configuration)
        {
            return new RabbitMqProducer(configuration, null);
        }

        ///Todo: abstract IConnectionFactory, currently its part of the RabbitMq namespace but should be generic for other messagebroker vendors 
        public static IProducer Create(IConfiguration configuration, IConnectionFactory connectionFactory)
        {
            return new RabbitMqProducer(configuration, connectionFactory);
        }
    }
}
