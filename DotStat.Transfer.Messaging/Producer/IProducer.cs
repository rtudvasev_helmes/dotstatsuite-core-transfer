﻿using Estat.Sdmxsource.Extension.Model.Error;
using System.Collections.Generic;
using System.Security.Principal;

namespace DotStat.Transfer.Messaging.Producer
{
    public interface IProducer
    {
        void Notify(IList<IResponseWithStatusObject> summary, IPrincipal principal);

        void Dispose();
    }
}
