﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Dto;
using DotStat.Db.DB;
using DotStat.DB.Repository;
using DotStat.Db.Service;
using DotStat.Domain;
using DotStat.Test;
using DotStat.Test.Moq;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.DataflowManager;
using DotStat.Transfer.Interface;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Model;
using DotStat.Transfer.Param;
using DotStat.Transfer.Processor;
using DotStat.Transfer.Producer;
using DotStat.Transfer.Test.Helper;
using DotStatServices.Transfer;
using DotStatServices.Transfer.BackgroundJob;
using DotStatServices.Transfer.Controllers;
using Estat.Sdmxsource.Extension.Constant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using DotStat.Transfer.Utils;

namespace DotStat.Transfer.Test.Unit.Controllers
{
    [TestFixture]
    public class TransferControllerTests : UnitTestBase
    {
        private readonly Mock<IAuthorizationManagement> _authorisation = new Mock<IAuthorizationManagement>();
        private readonly Mock<IHttpContextAccessor> _contextAccessorMock = new Mock<IHttpContextAccessor>();
        private readonly Mock<IProducer<TransferParam>> _producerMock = new Mock<IProducer<TransferParam>>();
        private readonly Mock<IConsumer<TransferParam>> _consumerMock = new Mock<IConsumer<TransferParam>>();
        private readonly Mock<IMailService> _mailServiceMock = new Mock<IMailService>();
        private readonly Mock<BackgroundQueue> _backgroundQueueMock = new Mock<BackgroundQueue>();
        private readonly ITempFileManagerBase _tempFileManager = new TestTempFileManager();
        private readonly TransferController _controller;
        private readonly AuthConfiguration _authConfig;
        private readonly Mock<IAuthorizationManagement> _authManagement = new Mock<IAuthorizationManagement>();

        private readonly Mock<IDotStatDb> _dotStatDbMock = new Mock<IDotStatDb>();
        private readonly Mock<IUnitOfWork> _unitOfWorkMock = new Mock<IUnitOfWork>();
        private readonly Mock<IDotStatDbService> _dotStatDbServiceMock = new Mock<IDotStatDbService>();

        private readonly Dataflow _dataflow;
        private IAsyncEnumerable<ObservationRow> _observationsInDb = AsyncEnumerable.Empty<ObservationRow>();

        public TransferControllerTests()
        {
            this.Configuration.SpacesInternal = new List<DataspaceInternal>()
            {
                new DataspaceInternal() {Id = "sourceInternalSpace"},
                new DataspaceInternal() {Id = "targetSpace"}
            };

           _authConfig = new AuthConfiguration()
            {
                ClaimsMapping = new Dictionary<string, string>()
            };

            _authorisation.Setup(x => x.IsAuthorized(
                It.IsAny<DotStatPrincipal>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<PermissionType>())
            ).Returns(true);

            // ----------------------------------------------------------------------

            var mappingStore = new TestMappingStoreDataAccess("sdmx/264D_264_SALDI+2.1.xml");
            _dataflow = mappingStore.GetDataflow();

            // Setup required moq objects -------------------------------------------

            _contextAccessorMock
                .Setup(x => x.HttpContext.User)
                .Returns(new ClaimsPrincipal(new ClaimsIdentity()));

            var smdxFileProducer = new SdmxFileProducer(_authorisation.Object, mappingStore, _tempFileManager);

            _producerMock
                .Setup(x => x.Process(
                    It.IsAny<TransferParam>(),
                    It.IsAny<Dataflow>(),
                    It.IsAny<CancellationToken>()
                ))
                .Returns(smdxFileProducer.Process(new SdmxFileToSqlTransferParam(new InternalSdmxParameters() {filepath = "data/264D_264_SALDI.csv"}, this.Configuration.SpacesInternal.FirstOrDefault(), "en-US"),
                    _dataflow,
                    CancellationToken.None));

            _consumerMock.As<IDataflowManager<SqlToSqlTransferParam>>();


            _backgroundQueueMock
                .Setup(x => x.Enqueue(It.IsAny<TransactionQueueItem>()))
                .Callback((TransactionQueueItem transactionQueueItem) =>
                {
                    transactionQueueItem.MainTask.Invoke(new CancellationToken());
                });

            // ----------------------------------------------------------------------
            var sqlToSqlManager = new SqlToSqlTransferManager(Configuration, _producerMock.Object, _consumerMock.Object, new NoneTransferProcessor());
            // ----------------------------------------------------------------------

            _unitOfWorkMock
                .Setup(m =>
                    m.TransactionRepository.GetNextTransactionId(It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(0));

            Func<string, IDotStatDb> dotStatDbResolver = dataSpaceId => _dotStatDbMock.Object;
            UnitOfWorkResolver unitOfWorkResolver = dataSpaceId => _unitOfWorkMock.Object;
            DotStatDbServiceResolver dotStatDbServiceResolver = dataSpaceId => _dotStatDbServiceMock.Object;

            _authManagement
                .Setup(x => x.IsAuthorized(
                    It.IsAny<DotStatPrincipal>(),
                    "targetSpace",
                    _dataflow.AgencyId,
                    _dataflow.Code,
                    _dataflow.Version.ToString(),
                    PermissionType.CanImportData
                ))
                .Returns(true);

            _controller = new TransferController(
                _contextAccessorMock.Object,
                sqlToSqlManager,
                Configuration,
                _authConfig,
                _mailServiceMock.Object,
                _backgroundQueueMock.Object,
                _authManagement.Object,
                unitOfWorkResolver,
                dotStatDbServiceResolver,
                mappingStore
            );

            _producerMock
                .Setup(x => x.IsAuthorized(
                    It.IsAny<TransferParam>(),
                    It.IsAny<Dataflow>()
                ))
                .Returns(true);
        }

        [TearDown]
        public void TearDown()
        {
            _observationsInDb = AsyncEnumerable.Empty<ObservationRow>();
            _consumerMock.Invocations.Clear();
            _backgroundQueueMock.Invocations.Clear();
        }

        [Test]
        public async Task ImportFromInternal()
        {
            SetupConsumer(_dataflow, true);

            var result = await Transfer();

            await ImportAssert<SqlToSqlTransferParam>(
                result,
                Times.Once(),
                StatusCodes.Status200OK,
                true,
                101
            );
        }

        [Test]
        public async Task ValidateTransferFromInternal()
        {
            SetupConsumer(_dataflow, true);

            var result = await _controller.ValidateTransferDataflow(
                "sourceInternalSpace",
                _dataflow.FullId,
                null,
                "targetSpace",
                _dataflow.FullId,
                "en",
                null,
                null,
                null
            );

            await ImportAssert<SqlToSqlTransferParam>(
                result,
                Times.Once(),
                StatusCodes.Status200OK,
                true,
                101
            );
        }
        [TestCase(TargetVersion.Live)]
        [TestCase(TargetVersion.PointInTime)]
        public async Task ImportFromInternalNoSourceVersion(TargetVersion targetVersion)
        {
            var mappingStore = new TestMappingStoreDataAccess("sdmx/264D_264_SALDI+2.1.xml");
            //_authorisation.Setup(x => x.IsAuthorized(
            //    It.IsAny<DotStatPrincipal>(),
            //    It.IsAny<string>(),
            //    It.IsAny<string>(),
            //    It.IsAny<string>(),
            //    It.IsAny<string>(),
            //    It.IsAny<PermissionType>())
            //).Returns(true);
            var smdxFileProducer = new SdmxFileProducer(_authorisation.Object, mappingStore, _tempFileManager);

            SqlToSqlTransferParam actualTransferParam = null;
            _producerMock
                .Setup(x => x.Process(
                    It.IsAny<TransferParam>(),
                    It.IsAny<Dataflow>(),
                    It.IsAny<CancellationToken>()
                ))
                .Returns(smdxFileProducer.Process(new SdmxFileToSqlTransferParam(new InternalSdmxParameters() {filepath = "data/264D_264_SALDI.csv" }, this.Configuration.SpacesInternal.FirstOrDefault(), "en-US"),
                    _dataflow,
                    CancellationToken.None
                ))
                .Callback<TransferParam, Dataflow, CancellationToken>((param, df, ct)=>
                {
                    actualTransferParam = param as SqlToSqlTransferParam;
                });


            SetupConsumer(_dataflow, true);

            await Transfer(targetVersion);

            Assert.AreEqual(TargetVersion.Live, actualTransferParam.SourceVersion); // Regardless of what is provided for targetVersion, the sourceVersion should default to Live.
        }


        [Test]
        public async Task DestinationDataflowDoesntExists()
        {
            SetupConsumer(null, true);

            var result = await Transfer();

            await ImportAssert<SqlToSqlTransferParam>(
                result,
                Times.Never(),
                StatusCodes.Status400BadRequest,
                false,
                0
            );
        }

        [Test]
        public async Task NotAuthorized()
        {
            SetupConsumer(_dataflow, false);

            var result = await Transfer();

            await ImportAssert<SqlToSqlTransferParam>(
                result,
                Times.Never(),
                StatusCodes.Status403Forbidden,
                false,
                0
            );
        }


        private async Task ImportAssert<T>(
            ActionResult<OperationResult> result,
            Times bgJobExecution,
            int expectedStatus,
            bool expectedSuccess,
            int expectedReadObsCount
        ) where T : TransferParam
        {
            // Background task is executed
            _backgroundQueueMock.Verify(mock => mock.Enqueue(It.IsAny<TransactionQueueItem>()), bgJobExecution);

            // Consumer.Save method is called
            _consumerMock.Verify(mock => mock.Save(
                    It.IsAny<T>(),
                    It.IsAny<Transaction>(),
                    It.IsAny<Dataflow>(),
                    It.IsAny<TransferContent>(),
                    It.IsAny<CancellationToken>()
                ),
                bgJobExecution
            );

            Assert.IsNotNull(result);

            var resultSummary = result.Value ?? ((ObjectResult)result.Result).Value as OperationResult;

            if (result.Result != null)
            {
                Assert.AreEqual(expectedStatus, ((ObjectResult)result.Result).StatusCode);
            }

            Assert.IsNotNull(resultSummary);
            Assert.AreEqual(expectedSuccess, resultSummary.Success);
            Assert.AreEqual(expectedReadObsCount, await _observationsInDb.CountAsync());
        }


        private async Task<ActionResult<OperationResult>> Transfer(TargetVersion? targetVersion=null)
        {
            return await _controller.TransferDataflow(
                "sourceInternalSpace",
                _dataflow.FullId,
                null,
                "targetSpace",
                _dataflow.FullId,
                "en",
                null,
                targetVersion,
                null,
                null,
                null,
                null
            );
        }
        
        private void SetupConsumer(Dataflow dataflow, bool isAuthorized)
        {
            _consumerMock.As<IDataflowManager<SqlToSqlTransferParam>>()
                .Setup(x => x.GetDataflow(It.IsAny<SqlToSqlTransferParam>(), It.IsAny<bool>()))
                .Returns(dataflow);

            _consumerMock
                .Setup(x => x.IsAuthorized(
                    It.IsAny<TransferParam>(),
                    It.IsAny<Dataflow>()
                ))
                .Returns(isAuthorized);

            _consumerMock
                .Setup(x => x.Save(
                    It.IsAny<TransferParam>(),
                    It.IsAny<Transaction>(),
                    It.IsAny<Dataflow>(),
                    It.IsAny<TransferContent>(),
                    It.IsAny<CancellationToken>()
                ))
                .Returns(Task.FromResult(true))
                .Callback((TransferParam p, Transaction t, Dataflow d, TransferContent c, CancellationToken ct) =>
                {
                    _observationsInDb = c.DataObservations;
                });
        }

    }
}
