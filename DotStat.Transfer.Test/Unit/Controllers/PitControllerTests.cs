﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Dto;
using DotStat.Db.DB;
using DotStat.Db.Repository;
using DotStat.DB.Repository;
using DotStat.Db.Service;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Test;
using DotStat.Transfer.Interface;
using DotStat.Transfer.Manager;
using DotStatServices.Transfer.BackgroundJob;
using DotStatServices.Transfer.Controllers;
using DotStatServices.Transfer.Services;
using Estat.Sdmxsource.Extension.Constant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model;
using DotStat.Transfer.Utils;

namespace DotStat.Transfer.Test.Unit.Controllers
{
    [TestFixture]
    public class PitControllerTests : SdmxUnitTestBase
    {
        private readonly PointInTimeController _controller;
        private readonly Mock<IHttpContextAccessor> _contextAccessorMock = new Mock<IHttpContextAccessor>();
        private readonly Mock<IMappingStoreDataAccess> _mappingStore = new Mock<IMappingStoreDataAccess>();
        private readonly Mock<IAuthorizationManagement> _authManagement = new Mock<IAuthorizationManagement>();
        private readonly Mock<IMailService> _mailService = new Mock<IMailService>();
        private readonly Mock<BackgroundQueue> _backgroundQueueMock = new Mock<BackgroundQueue>();
        
        private readonly Mock<IDotStatDb> _dotStatDbMock = new Mock<IDotStatDb>();
        private readonly Mock<IUnitOfWork> _unitOfWorkMock = new Mock<IUnitOfWork>();
        private readonly Mock<IDotStatDbService> _dotStatDbServiceMock = new Mock<IDotStatDbService>();

        private readonly string _dataSpace = "design";
        private readonly Dataflow _dataflow;
        private readonly AuthConfiguration _authConfig;

        public PitControllerTests()
        {
            this.Configuration.SpacesInternal = new List<DataspaceInternal>()
            {
                new DataspaceInternal() {Id = _dataSpace}
            };
            this.Configuration.DefaultLanguageCode = "en";

            _dataflow = GetDataflow();

            _unitOfWorkMock.Setup(mock => 
                mock.ArtefactRepository.GetDSDPITInfo(
                    It.IsAny<Dataflow>(),
                    It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(new JObject()));

            _dotStatDbServiceMock.Setup(mock => 
                mock.TryNewTransaction(
                    It.IsAny<Transaction>(), It.IsAny<Dataflow>(), It.IsAny<DotStatPrincipal>(),
                    It.IsAny<IMappingStoreDataAccess>(), It.IsAny<CancellationToken>(), It.IsAny<bool>()))
                .Returns(Task.FromResult((true, true, true)));

            _dotStatDbServiceMock.Setup(mock => 
                mock.Rollback(It.IsAny<Dataflow>(), It.IsAny<IMappingStoreDataAccess>(), It.IsAny<CancellationToken>()));

            _authManagement
                .Setup(x => x.IsAuthorized(
                    It.IsAny<DotStatPrincipal>(),
                    _dataSpace,
                    _dataflow.AgencyId,
                    _dataflow.Code,
                    _dataflow.Version.ToString(),
                    PermissionType.CanImportData
                ))
                .Returns(true);
            
            _mappingStore.Setup(mock =>
                    mock.GetDataflow(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<ResolveCrossReferences>(), It.IsAny<bool>()))
                .Returns(new Dataflow(_dataflow.Base, _dataflow.Dsd));

            _unitOfWorkMock.Setup(mock => 
                mock.TransactionRepository.MarkTransactionAsCompleted(
                    It.IsAny<int>(), It.IsAny<bool>()))
                .Returns(Task.FromResult(true));
            
            _contextAccessorMock
                .Setup(x => x.HttpContext.User)
                .Returns(new ClaimsPrincipal(new ClaimsIdentity()));

            _backgroundQueueMock
                .Setup(x => x.Enqueue(It.IsAny<TransactionQueueItem>()))
                .Callback((TransactionQueueItem transactionQueueItem) =>
                {
                    transactionQueueItem.MainTask.Invoke(new CancellationToken());
                });

            _authConfig = new AuthConfiguration()
            {
                ClaimsMapping = new Dictionary<string, string>()
            };

            Func<string, IDotStatDb> dotStatDbResolver = dataSpaceId => _dotStatDbMock.Object;
            UnitOfWorkResolver unitOfWorkResolver = dataSpaceId => _unitOfWorkMock.Object;
            DotStatDbServiceResolver dotStatDbServiceResolver = dataSpaceId => _dotStatDbServiceMock.Object;

            var commonManager = new CommonManager(
                _authManagement.Object, _mappingStore.Object, unitOfWorkResolver, dotStatDbServiceResolver);

            _controller = new PointInTimeController(
                _contextAccessorMock.Object,
                commonManager, 
                Configuration,
                _authConfig,
                _mailService.Object,
                _backgroundQueueMock.Object,
                _authManagement.Object,
                unitOfWorkResolver,
                dotStatDbServiceResolver,
                _mappingStore.Object
             );
        }

        [Test]
        public async Task PitInfoTest()
        {
            var result = await _controller.PitInfo("design", _dataflow.FullId);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<OkObjectResult>(result.Result);
        }

        [Test]
        public async Task RollbackTest()
        {
            var result = await _controller.Rollback("design", _dataflow.FullId);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<OkObjectResult>(result.Result);
        }

        [Test]
        public async Task RestoreTest()
        {
            var result = await _controller.Restore("design", _dataflow.FullId);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<OkObjectResult>(result.Result);
        }
    }
}
