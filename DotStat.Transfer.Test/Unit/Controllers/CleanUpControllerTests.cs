﻿using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Dto;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Test;
using DotStatServices.Transfer;
using DotStatServices.Transfer.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.Util.Extension;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Exceptions;
using DotStat.Db.DB;
using DotStat.Db.Dto;
using DotStat.DB.Repository;
using DotStat.Db.Service;
using DotStat.Transfer.Manager;
using DotStatServices.Transfer.BackgroundJob;
using Estat.Sdmxsource.Extension.Constant;
using DotStat.Transfer.Interface;
using DotStat.Common.Enums;
using DotStat.Transfer.Utils;

namespace DotStat.Transfer.Test.Unit.Controllers
{
    [TestFixture]
    public class CleanUpControllerTests : SdmxUnitTestBase
    {
        private CleanUpController _controller;
        private readonly AuthConfiguration _authConfig;
        private readonly Mock<IHttpContextAccessor> _contextAccessorMock = new Mock<IHttpContextAccessor>();
        private readonly Mock<IAuthorizationManagement> _authManagementMock = new Mock<IAuthorizationManagement>();
        private readonly string _dataSpace = "design";
        private readonly Mock<IMappingStoreDataAccess> _mappingStoreDataAccessMock = new Mock<IMappingStoreDataAccess>();

        private readonly Mock<IDotStatDb> _dotStatDbMock = new Mock<IDotStatDb>();
        private readonly Mock<IUnitOfWork> _unitOfWorkMock = new Mock<IUnitOfWork>();
        private readonly Mock<IDotStatDbService> _dotStatDbServiceMock = new Mock<IDotStatDbService>();

        private readonly CommonManager _commonManager;
        private readonly SdmxObjectsImpl _sdmxObj;
        private readonly Mock<IMailService> _mailServiceMock = new Mock<IMailService>();
        private readonly Mock<BackgroundQueue> _backgroundQueueMock = new Mock<BackgroundQueue>();

        public CleanUpControllerTests()
        {
            _contextAccessorMock
                .Setup(x => x.HttpContext.User)
                .Returns(new ClaimsPrincipal(new ClaimsIdentity()));

            _authConfig = new AuthConfiguration()
            {
                ClaimsMapping = new Dictionary<string, string>()
            };

            _sdmxObj = new SdmxObjectsImpl(DatasetActionEnumType.Append);
            _sdmxObj.Merge(new FileInfo("sdmx/264D_264_SALDI+2.1.xml").GetSdmxObjects(new StructureParsingManager()));
            _sdmxObj.Merge(new FileInfo("sdmx/ESTAT+NA.xml").GetSdmxObjects(new StructureParsingManager()));
            _sdmxObj.Merge(new FileInfo("sdmx/CsvV2.xml").GetSdmxObjects(new StructureParsingManager()));

            foreach (var dsd in _sdmxObj.DataStructures)
            {
                _authManagementMock
                    .Setup(x => x.IsAuthorized(
                        It.IsAny<DotStatPrincipal>(),
                        _dataSpace,
                        dsd.AgencyId,
                        dsd.Id,
                        dsd.Version,
                        PermissionType.CanDeleteStructuralMetadata
                    ))
                    .Returns(true);

            }
            
            _unitOfWorkMock
                .Setup(x => x.TransactionRepository.CreateTransactionItem(
                    It.IsAny<int>(), It.IsAny<ArtefactItem>(), It.IsAny<DotStatPrincipal>(),
                    It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<TransactionType>(), It.IsAny<TargetVersion>(),
                    It.IsAny<CancellationToken>(), It.IsAny<bool>()));

            _unitOfWorkMock
                .Setup(x => x.TransactionRepository.LockTransaction(
                    It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int?>(), It.IsAny<DbTableVersion>(),
                    It.IsAny<TargetVersion>(), It.IsAny<CancellationToken>()));

            _unitOfWorkMock
                .Setup(x => x.TransactionRepository.MarkTransactionAsCompleted(
                    It.IsAny<int>(), It.IsAny<bool>()));

            _unitOfWorkMock
                .Setup(x => x.TransactionRepository.MarkTransactionAsCompleted(
                    It.IsAny<int>(), It.IsAny<bool>()));

            Configuration.SpacesInternal = new List<DataspaceInternal>()
            {
                new DataspaceInternal() {Id = _dataSpace}
            };

            Configuration.DefaultLanguageCode = "en";

            Func<string, IDotStatDb> dotStatDbResolver = dataSpaceId => _dotStatDbMock.Object;
            UnitOfWorkResolver unitOfWorkResolver = dataSpaceId => _unitOfWorkMock.Object;
            DotStatDbServiceResolver dotStatDbServiceResolver = dataSpaceId => _dotStatDbServiceMock.Object;
            var _mailServiceMock = new Mock<IMailService>();
            var _backgroundQueueMock = new Mock<BackgroundQueue>();

            _backgroundQueueMock
                .Setup(x => x.Enqueue(It.IsAny<TransactionQueueItem>()))
                    .Callback((TransactionQueueItem transactionQueueItem) => { transactionQueueItem.MainTask.Invoke(new CancellationToken()); });

            _commonManager = new CommonManager(
                _authManagementMock.Object, _mappingStoreDataAccessMock.Object,
                unitOfWorkResolver, dotStatDbServiceResolver);

            _controller = new CleanUpController(
                _contextAccessorMock.Object, _authConfig, Configuration,
                _commonManager, _backgroundQueueMock.Object, _mailServiceMock.Object, 
                _authManagementMock.Object, unitOfWorkResolver, dotStatDbServiceResolver);

        }

        [Test]
        public void  CleanUpDsdWithWrongDsd()
        {
            var exception = Assert.ThrowsAsync<DotStatException>(() => _controller.CleanUpDsd("design", "wrong ID"));
            Assert.That(exception.Message.Contains("AGENCYID:ID(VERSION)", StringComparison.OrdinalIgnoreCase));
        }

        [Test]
        public async Task CleanUpDsdUnauthorized()
        {
            var result = await _controller.CleanUpDsd("design", "NOACCESS:264D_Consumer_Confiance(1.0)");
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<ForbidResult>(result.Result);
        }

        [Test]
        public async Task CleanUpDsdMappingStoreHasDsd()
        {
            SetupMappingStoreDataAccess(new SdmxObjectsImpl(null, _sdmxObj.DataStructures));
            SetupCleanupSteps(true, 1);
            SetupTransactionForCleanup(true);

            var result = await _controller.CleanUpDsd("design", "IT1:264D_Consumer_Confiance(1.0)");
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsTrue(result.Value.Success);
        }

        [Test]
        public async Task CleanUpDsdNothingToDelete()
        {
            SetupMappingStoreDataAccess(new SdmxObjectsImpl());
            SetupCleanupSteps(false, -1);
            SetupTransactionForCleanup(true);

            var result = await _controller.CleanUpDsd("design", "IT1:264D_Consumer_Confiance(1.0)");
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsFalse(result.Value.Success);
        }

        [Test]
        public async Task CleanUpDsdSuccessful()
        {
            SetupMappingStoreDataAccess(new SdmxObjectsImpl());
            SetupCleanupSteps(true, 1);
            SetupTransactionForCleanup(true);

            var result = await _controller.CleanUpDsd("design", "IT1:264D_Consumer_Confiance(1.0)", true);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsTrue(result.Value.Success);
        }

        [Test]
        public async Task CleanUpDsdConflictingTransaction()
        {
            SetupMappingStoreDataAccess(new SdmxObjectsImpl());
            SetupCleanupSteps(true, 1);
            SetupTransactionForCleanup(false);

            var result = await _controller.CleanUpDsd("design", "IT1:264D_Consumer_Confiance(1.0)", true);
            Assert.IsNotNull(result);
            Assert.IsInstanceOf<ConflictObjectResult>(result.Result);
            var conflicResult = (ConflictObjectResult) result.Result;

            Assert.IsNotNull(conflicResult.Value);
            Assert.IsInstanceOf<OperationResult>(conflicResult.Value);
            Assert.IsFalse(((OperationResult)conflicResult.Value).Success);
        }
        
        [Test]
        public async Task CleanUpOrphanDsd()
        {
            SetupMappingStoreDataAccess(new SdmxObjectsImpl());
            SetupCleanupSteps(true, 1);
            SetupTransactionForCleanup(true);

            var result = await _controller.CleanUpOrphanDsds("design");

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsTrue(result.Value.Success);
        }

        [Test]
        public async Task CleanUpOrphanDsd2()
        {
            SetupMappingStoreDataAccess(new SdmxObjectsImpl());
            SetupCleanupSteps(true, 1);
            SetupTransactionForCleanup(true);

            var result = await _controller.CleanUpOrphanDsdsV2("design");

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsTrue(result.Value.Success);
        }

        [Test]
        public async Task CleanUpNonOrphanDsds()
        {
            SetupMappingStoreDataAccess(new SdmxObjectsImpl());
            SetupCleanupSteps(true, 1);
            SetupTransactionForCleanup(true);
            
            var result = await _controller.CleanUpOrphanDsds("design");

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsTrue(result.Value.Success);
        }

        [Test]
        public async Task CleanUpOrphanDsdsWithConcurrentTransaction()
        {
            SetupMappingStoreDataAccess(new SdmxObjectsImpl());
            SetupCleanupSteps(true, 1);
            SetupTransactionForCleanup(false);
            
            var result = await _controller.CleanUpOrphanDsds("design");
            
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<ConflictObjectResult>(result.Result);

            var objResult = (ConflictObjectResult)result.Result;
            Assert.IsInstanceOf<OperationResult>(objResult.Value);
            Assert.IsFalse(((OperationResult)objResult.Value).Success);
        }

        [Test]
        public async Task CleanUpOrphanDsdsWithStatus200And409()
        {
            SetupMappingStoreDataAccess(new SdmxObjectsImpl());
            SetupCleanupSteps(true, 1);
            SetupTransactionForCleanup(_sdmxObj);

            var result = await _controller.CleanUpOrphanDsds("design");

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<ObjectResult>(result.Result);

            ObjectResult objResult = (ObjectResult)result.Result;
            Assert.IsInstanceOf<ObjectResult[]>(objResult.Value);
            ObjectResult[] objResults = (ObjectResult[]) objResult.Value;
            Assert.IsInstanceOf<OkObjectResult>(objResults[0]);
            Assert.IsInstanceOf<ConflictObjectResult>(objResults[1]);
        }

        [TearDown]
        public void TearDown()
        {
            _mappingStoreDataAccessMock.Invocations.Clear();
            _dotStatDbServiceMock.Invocations.Clear();
            _unitOfWorkMock.Invocations.Clear();
            _dotStatDbMock.Invocations.Clear();
        }

        private void SetupMappingStoreDataAccess(ISdmxObjects sdmxObjs)
        {
            _mappingStoreDataAccessMock.Setup(x =>
                    x.GetDsd(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(sdmxObjs);
        }

        private void SetupCleanupSteps(bool cleanUpDsdResult, int resultOfGetArtefactDbId)
        {
            _dotStatDbServiceMock
                .Setup( x =>  
                    x.CleanUpDsd(It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<IMappingStoreDataAccess>(), It.IsAny<List<ComponentItem>>(), It.IsAny<List<MetadataAttributeItem>>(), It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(cleanUpDsdResult));
            
            _unitOfWorkMock
                .Setup(x => 
                    x.ArtefactRepository.GetArtefactDbId(
                        It.IsAny<string>(),
                        It.IsAny<string>(),
                        It.IsAny<SdmxVersion>(),
                        SDMXArtefactType.Dsd,
                        It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(resultOfGetArtefactDbId));


            _unitOfWorkMock
            .Setup(x =>
                x.ComponentRepository.GetAllComponents(It.IsAny<CancellationToken>()))
            .Returns(Task.FromResult((IList<ComponentItem>)new List<ComponentItem>())); 

            _unitOfWorkMock
            .Setup(x =>
                x.MetaMetadataComponentRepository.GetAllMetadataAttributeComponents(It.IsAny<CancellationToken>()))
            .Returns(Task.FromResult((IList<MetadataAttributeItem>)new List<MetadataAttributeItem>()));

            _unitOfWorkMock
            .Setup(x => 
                x.ArtefactRepository.GetListOfArtefacts(
                    It.IsAny<CancellationToken>(),"DSD"))
            .Returns(
                Task.FromResult(
                (IList<ArtefactItem>) _sdmxObj.DataStructures.Select(dsd => new ArtefactItem() {Agency = dsd.AgencyId, Id = dsd.Id, Version = dsd.Version})
                    .ToList())
            );
        }

        private void SetupTransactionForCleanup(bool resultOfTryNewTransaction)
        {
            _dotStatDbServiceMock.Setup(x =>
                x.TryNewTransactionForCleanup(
                    It.IsAny<Transaction>(),
                    It.IsAny<int>(),
                    It.IsAny<int?>(),
                    It.IsAny<string>(), 
                    It.IsAny<string>(), 
                    It.IsAny<SdmxVersion>(), It.IsAny<CancellationToken>(), It.IsAny<bool>()))
                .Returns(Task.FromResult(resultOfTryNewTransaction));
        }

        private void SetupTransactionForCleanup(ISdmxObjects sdmxObjs)
        {
            bool returnValue = true;

            foreach (var dsd in sdmxObjs.DataStructures)
            {
                var value = returnValue;
                _dotStatDbServiceMock.Setup( x =>
                     x.TryNewTransactionForCleanup(
                        It.IsAny<Transaction>(),
                        It.IsAny<int>(),
                        It.IsAny<int?>(),
                        It.Is<string>(i => i == dsd.AgencyId),
                        It.Is<string>(i => i == dsd.Id),
                        It.IsAny<SdmxVersion>(), 
                        It.IsAny<CancellationToken>(), 
                        It.IsAny<bool>()
                    )).Returns(Task.FromResult(value) );

                returnValue = !returnValue;
            }
            
        }


    }
}
