﻿using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Domain;
using DotStat.Test;
using DotStat.Test.Moq;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Utils;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Org.Sdmxsource.Util.Io;
using System;
using System.Collections.Generic;
using System.IO;
using DotStat.MappingStore;
using Moq;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.DataParser.Engine.Csv;
using Dataflow = DotStat.Domain.Dataflow;

namespace DotStat.Transfer.Test.Unit.Utilities
{
    [TestFixture]
    public class ExtensionTests : UnitTestBase
    {
        private Dataflow _dataflow;
        public ExtensionTests()
        {
            var mappingStore = new TestMappingStoreDataAccess("sdmx/OECD-DF_TEST_DELETE-1.0-all_structures.xml");
            _dataflow = mappingStore.GetDataflow();
        }

        [TestCase("", true)]
        [TestCase(null, true)]
        [TestCase("2022-06-04T10:16:01", true)]
        [TestCase("2022-06-04T08:16:01Z", true)]
        [TestCase("2022-06-04T09:46:01+01:30", true)]
        [TestCase("2022-06-04T10:16:01.123", true)]
        [TestCase("2022-06-04T08:16:01.123Z", true)]
        [TestCase("2022-06-04T09:46:01.123+01:30", true)]
        [TestCase("2022-06-03T20:16:01-14:00", true)]
        [TestCase("2022-06-03T20:16:01-1400", false)]
        [TestCase("2022-06-04T24:00:00", false)] // DateTime does not support 24th hour in accordance with ISO ISO 8601-1:2019: it is explicitly disallowed by the 2019 revision.
        [TestCase("2022-06-04T10:16:0", false)]
        [TestCase("2022-06-04T09:46:01.1230+01:30", false)]
        [TestCase("2022-06-04T09:46:01.+01:30", false)]
        [TestCase("06-24-2021 23:22:21", false)]
        [TestCase("24-06-2021 23:22:21", false)]
        [TestCase("06/24/2021 23:22:21", false)]
        [TestCase("24/06/2021 23:22:21", false)]
        [TestCase("2021-06-24T25:22:21", false)]
        [TestCase("0900-06-24T23:22:21", false)]
        [TestCase("900-06-24T23:22:21", false)]
        [TestCase("2021-16-24T23:22:21", false)]
        [TestCase("2021-02-31T23:22:21", false)]
        [TestCase("2021-06-24T25:22:21", false)]
        [TestCase("2021-06-24T23:60:21", false)]
        [TestCase("2021-06-24T23:22:60", false)]
        public void GetPitReleaseDateTest(string dateTimeStr, bool isValidExpected)
        {
            DateTime? dateTimeValue = null;
            if (isValidExpected)
            {
                Assert.DoesNotThrow(() => dateTimeValue = dateTimeStr.GetPITReleaseDate("en"));

                if (string.IsNullOrEmpty(dateTimeStr))
                {
                    Assert.IsNull(dateTimeValue);
                }
                else
                {
                    Assert.IsNotNull(dateTimeValue);
                }
            }
            else
            {
                Assert.Throws<DotStatException>(() => dateTimeValue = dateTimeStr.GetPITReleaseDate("en"));
            }
        }

        [TestCase("I", StagingRowActionEnum.Merge)]
        [TestCase("A", StagingRowActionEnum.Merge)]
        [TestCase("M", StagingRowActionEnum.Merge)]
        [TestCase("D", StagingRowActionEnum.Delete)]
        [TestCase("R", StagingRowActionEnum.Replace)]
        public void GetStagingRowActionEnum(string action, StagingRowActionEnum? expectedAction)
        {
            var dataReaderEngine = new Mock<IDataReaderEngine>();
            dataReaderEngine.Setup(x => x.CurrentAction).Returns(DatasetAction.GetAction(action));

            Assert.AreEqual(expectedAction, dataReaderEngine.Object.GetStagingRowActionEnum());
        }

        [TestCase("value", StagingRowActionEnum.Delete, "value")]
        [TestCase("value", StagingRowActionEnum.Merge, "value")]
        [TestCase("", StagingRowActionEnum.Delete, null)]
        [TestCase("", StagingRowActionEnum.Merge, "")]
        public void GetPresentComponents(string compValue, StagingRowActionEnum action, string expectedValue)
        {
            var allComponents = new List<IKeyValue>{ new KeyValueImpl(compValue, "concept")};
            var presentComponents = allComponents.GetPresentComponents(action);

            if (expectedValue is not null)
            {
                Assert.AreEqual(1, presentComponents.Count);
                Assert.AreEqual(expectedValue, presentComponents[0].Code);
            }
            else
                Assert.AreEqual(0, presentComponents.Count);
        }
        
        [TestCase(null, StagingRowActionEnum.Delete, null)]
        [TestCase(null, StagingRowActionEnum.Merge, null)]
        [TestCase("value", StagingRowActionEnum.Delete, "value")]
        [TestCase("value", StagingRowActionEnum.Merge, "value")]
        [TestCase("", StagingRowActionEnum.Delete, null)]
        [TestCase("", StagingRowActionEnum.Merge, "")]
        public void GetObservationValue(string obsValue, StagingRowActionEnum action, string expectedValue)
        {
            var observation = new ObservationImpl(new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, Array.Empty<IKeyValue>(), null), null, obsValue, null, crossSectionValue: null);
            
            Assert.AreEqual(expectedValue, observation.GetObservationValue(action));
        }

        //CSV V1
        [TestCase("data/264D_264_SALDI.csv", false)]
        //CSV V2
        [TestCase("data/delete/case_1__delete_whole_content_of_the_dataflow.csv", false)]
        //XML
        [TestCase("data/264D_264_SALDI_generic.xml", true)]
        public void CheckIsXml(string filePath, bool expectedValue)
        {
            using var sourceData = new ReadableDataLocationFactory().GetReadableDataLocation(new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
            using var dataReaderEngine = new DataReaderManager().GetDataReaderEngine(sourceData, _dataflow.Dsd.Base, null, _dataflow.Dsd.Msd?.Base);

            Assert.AreEqual(expectedValue, dataReaderEngine.IsXml());
        }

        //xml
        [TestCase(StagingRowActionEnum.Merge, false, false, true, false)]
        [TestCase(StagingRowActionEnum.Merge, true, true, true, true)]
        [TestCase(StagingRowActionEnum.Delete, false, false, true, false)]
        [TestCase(StagingRowActionEnum.Delete, false, true, true, true)]
        //csv
        [TestCase(StagingRowActionEnum.Merge, false, false, false, false)]
        [TestCase( StagingRowActionEnum.Merge, true, true, false, true)]
        [TestCase(StagingRowActionEnum.Delete, false, false, false, false)]
        [TestCase(StagingRowActionEnum.Delete, false, true, false, true)]
        //non supported actions
        [TestCase(StagingRowActionEnum.Skip, true, true, true, false, false)]
        [TestCase(StagingRowActionEnum.DeleteAll, true, true, true, false, false)]
        public void CheckAuthorized(StagingRowActionEnum action, bool canMergeData, bool canDeleteData, bool isXml, bool isAuthorized, bool isSupported = true)
        {
            var unauthorizedToMergeText = isXml
                ? string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnauthorizedToMergeXml), 1)
                : string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnauthorizedToMergeCsv), 1, 1 + 1);

            var unauthorizedToDeleteText = isXml
                ? string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnauthorizedToDeleteXml), 1)
                : string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnauthorizedToDeleteCsv), 1, 1 + 1);

            if (isAuthorized)
                Assert.DoesNotThrow(() =>
                    action.CheckAuthorized(canMergeData, canDeleteData, canMergeData, 1, isXml));
            else if (!isSupported)
            {
                Assert.Throws<NotSupportedException>(() =>
                    action.CheckAuthorized(canMergeData, canDeleteData, canMergeData, 1, isXml));
            }
            else
            {
                var ex = Assert.Throws<TransferUnauthorizedException>(() =>
                    action.CheckAuthorized(canMergeData, canDeleteData, canMergeData, 1, isXml));

                Assert.AreEqual(
                    action is StagingRowActionEnum.Delete ? unauthorizedToDeleteText : unauthorizedToMergeText,
                    ex.Message);
            }
        }
    }
}
