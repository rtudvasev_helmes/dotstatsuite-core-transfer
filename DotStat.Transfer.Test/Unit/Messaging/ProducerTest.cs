﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;
using DotStat.Common.Messaging;
using DotStat.Transfer.Messaging.Producer;
using Estat.Sdmxsource.Extension.Model.Error;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using RabbitMQ.Client;

namespace DotStat.Transfer.Test.Unit.Messaging
{
    public class ProducerTests
    {
        /// <summary>
        /// Tests ProducerFactory and RabbitMqProducer
        /// </summary>
        [Test()]
        public void ProducerTest()
        {
            // ARRANGE
            var configBuilder = new ConfigurationBuilder().AddJsonFile("config/messaging.json");
            var conf = configBuilder.Build();

            var factory = new Mock<IConnectionFactory>();
            var connection = new Mock<IConnection>();
            var model = new Mock<IModel>();
            var basicProperties = new Mock<IBasicProperties>();

            model.Setup(x => x.QueueDeclarePassive(It.IsAny<string>())).Returns(new QueueDeclareOk("", 0, 0));
            model.Setup(x => x.BasicPublish(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(),
                It.IsAny<IBasicProperties>(), It.IsAny<ReadOnlyMemory<byte>>()));
            model.Setup(x => x.CreateBasicProperties()).Returns(basicProperties.Object);
            model.Setup(x => x.IsOpen).Returns(true);
            connection.Setup(x => x.CreateModel()).Returns(model.Object);
            factory.Setup(e => e.CreateConnection()).Returns(connection.Object);

            var responseWithStatusObjects = new List<IResponseWithStatusObject> {
                new ResponseWithStatusObject(
                    new List<IMessageObject>(),
                    Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success,
                    new StructureReferenceImpl("dummyAgency", "dummyId", "1.0", SdmxStructureEnumType.Dataflow),
                    DatasetActionEnumType.Information)
            };

            GenericIdentity userIdentity = new GenericIdentity("dummyIdentity");
            GenericPrincipal userPrincipal = new GenericPrincipal(userIdentity, null);

            // ACT
            var sut = ProducerFactory.Create(conf, factory.Object);

            sut.Notify(responseWithStatusObjects, userPrincipal);
            sut.Dispose();

            // ASSERT
            var messageConf = conf.GetSection(MessagingServiceConfiguration.MessageBrokerSettings).Get<MessagingServiceConfiguration>();
            factory.Verify(x => x.CreateConnection(), Times.Once);
            connection.Verify(x => x.CreateModel(), Times.Once);
            model.Verify(x => x.BasicPublish(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(),
                It.IsAny<IBasicProperties>(), It.IsAny<ReadOnlyMemory<byte>>()), Times.Once);
            model.Verify(x => x.QueueDeclare(
                It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<IDictionary<string,object>>()), 
                Times.Once);

            Assert.Pass();
        }





    }
}
