﻿using System.Text;
using System.Threading;
using DotStat.Test.Moq;
using DotStat.Transfer.Interface;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Messaging.Consumer;
using DotStat.Transfer.Param;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using DotStat.Db.Service;
using DotStat.Domain;
using System.Threading.Tasks;
using DotStat.Common.Auth;
using DotStat.MappingStore;

namespace DotStat.Transfer.Test.Unit.Messaging
{
    [TestFixture()]
    public class ConsumerTests
    {
        [Test()]
        public void StartTest()
        {
            // ARRANGE
            var configuration = new ConfigurationBuilder().AddJsonFile("config/messaging.json").Build();

            var commonMgr = new Mock<ICommonManager>();
            var mailService = new Mock<IMailService>();
            var connection = new Mock<IConnection>();
            var model = new Mock<IModel>();
            var connectionFactory = new Mock<IConnectionFactory>();

            connectionFactory.Setup(x => x.CreateConnection()).Returns(connection.Object);
            connection.Setup(x => x.CreateModel()).Returns(model.Object);
            
            var mappingStore = new TestMappingStoreDataAccess("sdmx/264D_264_SALDI+2.1.xml");
            
            var basicEvent = new EventingBasicConsumer(null);
            var basicDeliverEventArgs = new BasicDeliverEventArgs();
            basicDeliverEventArgs.Body = Encoding.UTF8.GetBytes(
                @"[{ ""Action"":""Append"",""Dataspace"":""demo-design"",""PrincipalName"":""Test Admin"",""PrincipaEmail"":""admin@dotstat-suite.com"",""ArtefactId"":""CL_FREQ"", ""ArtefactAgency"":""OECD"",""ArtefactVersion"":""1.0""}]");

            var dotStatDbServiceMock = new Mock<IDotStatDbService>();

            DotStatDbServiceResolver dotStatDbServiceResolver = dataSpaceId => dotStatDbServiceMock.Object;

            dotStatDbServiceMock
                .Setup(m =>
                    m.TryNewTransaction(It.IsAny<Transaction>(), It.IsAny<Dataflow>(),
                        It.IsAny<DotStatPrincipal>(), It.IsAny<IMappingStoreDataAccess>(), It.IsAny<CancellationToken>(), It.IsAny<bool>()))
                .Returns(Task.FromResult((true, true, true)))
                .Callback(() =>
                {

                });

            //ACT
            var sut = new RabbitMqConsumer(configuration, commonMgr.Object, mappingStore, mailService.Object, dotStatDbServiceResolver, connectionFactory.Object);
                sut.Start();
                sut.OnReceived(basicEvent, basicDeliverEventArgs);
                sut.Dispose();

            // ASSERT
            commonMgr.Verify(x => x.InitDataDbObjectsOfDataflow(
                It.IsAny<TransferParam>(),
                It.IsAny<IDataflowMutableObject>(),
                It.IsAny<CancellationToken>(),
                It.IsAny<bool>()
                ), Times.Once);           


            Assert.Pass();
        }



    }
}