﻿using DotStat.Common.Auth;
using DotStat.Common.Configuration.Dto;
using DotStat.Common.Exceptions;
using DotStat.Domain;
using DotStat.Test;
using DotStat.Test.Moq;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Model;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;
using DotStatServices.Transfer.BackgroundJob;
using Microsoft.AspNetCore.Http.Features;
using Moq;
using Moq.Protected;
using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Estat.Sdmxsource.Extension.Constant;

namespace DotStat.Transfer.Test.Unit.Producer
{
    [TestFixture]
    public class UrlProducerTests : UnitTestBase
    {
        private readonly Mock<IAuthorizationManagement> _authorisation = new Mock<IAuthorizationManagement>();
        private readonly Mock<TempFileManager> _tempFileManager;
        private readonly TestMappingStoreDataAccess _mappingStore;
        private readonly Dataflow _dataflow;

        public UrlProducerTests()
        {
            _tempFileManager = new Mock<TempFileManager>(Configuration, new FormOptions());
            _mappingStore = new TestMappingStoreDataAccess("sdmx/264D_264_SALDI+2.1.xml");
            _dataflow = _mappingStore.GetDataflow();

            _authorisation.Setup(x => x.IsAuthorized(
                It.IsAny<DotStatPrincipal>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<PermissionType>())
            ).Returns(true);
        }

        [Test]
        public void SuccessfulDataProcess()
        {
            var fileProducer = new SdmxFileProducer(_authorisation.Object, _mappingStore, _tempFileManager.Object);

            var transferParam = new SdmxFileToSqlTransferParam(
                new InternalSdmxParameters() {filepath = "data/264D_264_SALDI_compact.xml", PITReleaseDate = string.Empty, ImportValidationType = ValidationType.ImportWithBasicValidation}, 
                new DataspaceInternal() { Id = "reset" }, 
                "en-US")
            {
                Id = 0,
                Principal = new DotStatPrincipal(new ClaimsPrincipal(new ClaimsIdentity()), new Dictionary<string, string>()),
            };

            Assert.IsFalse(fileProducer.IsMetadataOnly(transferParam).Result);
            var dataflow = fileProducer.GetDataflow(transferParam);
            Assert.IsNotNull(dataflow);
            Assert.IsTrue(fileProducer.IsAuthorized(transferParam, _dataflow));

            TransferContent result = null;
            Assert.DoesNotThrowAsync(async () => result = await fileProducer.Process(transferParam, dataflow, CancellationToken));
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.ReportedComponents);
            Assert.AreEqual(12, result.DataObservations.CountAsync().Result);
        }

        [Test]
        public void SuccessfulMetaDataProcess()
        {
            var mappingStore = new TestMappingStoreDataAccess("sdmx/CsvV2.xml");
            var mockFactory = new Mock<IHttpClientFactory>();
            var mockHttpMessageHandler = new Mock<HttpMessageHandler>();
            mockHttpMessageHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(File.ReadAllText("data/CsvV2_without_obs_value.csv"))
                });

            var client = new HttpClient(mockHttpMessageHandler.Object);
            mockFactory.Setup(_ => _.CreateClient(It.IsAny<string>())).Returns(client);

            var urlProducer = new SdmxUrlProducer(_authorisation.Object, mappingStore, mockFactory.Object, Configuration, _tempFileManager.Object);

            var transferParam = new SdmxUrlToSqlTransferParam(
                new InternalSdmxParameters() { filepath = "http://testurl", PITReleaseDate = string.Empty, ImportValidationType = ValidationType.ImportWithBasicValidation },
                new DataspaceInternal() { Id = "reset" },
                "en-US",
                new HeaderDictionary())
            {
                Id = 0,
                Principal = new DotStatPrincipal(new ClaimsPrincipal(new ClaimsIdentity()), new Dictionary<string, string>())
            };

            Assert.IsTrue(urlProducer.IsMetadataOnly(transferParam).Result);
            var dataflow = urlProducer.GetDataflow(transferParam);
            Assert.IsNotNull(dataflow);
            Assert.IsTrue(urlProducer.IsAuthorized(transferParam, dataflow));
            Assert.AreEqual(1, transferParam.FilesToDelete.Count);

            TransferContent result = null;
            Assert.DoesNotThrowAsync(async () => result = await urlProducer.Process(transferParam, dataflow, CancellationToken));
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.ReportedComponents);
            Assert.AreEqual(4, result.MetadataObservations.CountAsync().Result);
        }

        [Test]
        public void ShouldThrowDotStatException()
        {
            var mockFactory = new Mock<IHttpClientFactory>();
            var mockHttpMessageHandler = new Mock<HttpMessageHandler>();
            mockHttpMessageHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .Callback(() => throw new HttpRequestException());

            var client = new HttpClient(mockHttpMessageHandler.Object);
            mockFactory.Setup(_ => _.CreateClient(It.IsAny<string>())).Returns(client);

            var urlProducer = new SdmxUrlProducer(_authorisation.Object, _mappingStore, mockFactory.Object, Configuration, _tempFileManager.Object);

            Assert.ThrowsAsync<DotStatException>(() => urlProducer.IsMetadataOnly(new SdmxUrlToSqlTransferParam(
                new InternalSdmxParameters()
                {
                    filepath = "http://testurl",
                    PITReleaseDate = string.Empty,
                    ImportValidationType = ValidationType.ImportWithBasicValidation
                },
                new DataspaceInternal() { Id = "reset" },
                "en-US",
                new HeaderDictionary())
            {
                Id = 0,
                Principal = new DotStatPrincipal(new ClaimsPrincipal(new ClaimsIdentity()), new Dictionary<string, string>())
            }));

            mockFactory = new Mock<IHttpClientFactory>();
            mockHttpMessageHandler = new Mock<HttpMessageHandler>();
            mockHttpMessageHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .Callback(() => throw new TaskCanceledException());

            client = new HttpClient(mockHttpMessageHandler.Object);
            mockFactory.Setup(_ => _.CreateClient(It.IsAny<string>())).Returns(client);

            urlProducer = new SdmxUrlProducer(_authorisation.Object, _mappingStore, mockFactory.Object, Configuration, _tempFileManager.Object);

            Assert.ThrowsAsync<TransferFailedException>(() => urlProducer.IsMetadataOnly(new SdmxUrlToSqlTransferParam(
                new InternalSdmxParameters() { filepath = "http://testurl", PITReleaseDate = string.Empty, ImportValidationType = ValidationType.ImportWithBasicValidation },
                new DataspaceInternal() { Id = "reset" },
                "en-US",
                new HeaderDictionary())
            {
                Id = 0,
                Principal = new DotStatPrincipal(new ClaimsPrincipal(new ClaimsIdentity()), new Dictionary<string, string>()),
            }));
        }

        [Test()]
        public void ShouldThrowTransferFailedException()
        {
            var mockFactory = new Mock<IHttpClientFactory>();
            var mockHttpMessageHandler = new Mock<HttpMessageHandler>();
            mockHttpMessageHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .Callback(() => throw new TaskCanceledException());

            var client = new HttpClient(mockHttpMessageHandler.Object);
            mockFactory.Setup(_ => _.CreateClient(It.IsAny<string>())).Returns(client);

            var urlProducer = new SdmxUrlProducer(_authorisation.Object, _mappingStore, mockFactory.Object, Configuration, _tempFileManager.Object);

            Assert.ThrowsAsync<TransferFailedException>(() => urlProducer.IsMetadataOnly(new SdmxUrlToSqlTransferParam(
                new InternalSdmxParameters() { filepath = "http://testurl", PITReleaseDate = string.Empty, ImportValidationType = ValidationType.ImportWithBasicValidation },
                new DataspaceInternal() { Id = "reset" },
                "en-US",
                new HeaderDictionary())
            {
                Id = 0,
                Principal = new DotStatPrincipal(new ClaimsPrincipal(new ClaimsIdentity()), new Dictionary<string, string>()),
            }));
        }
    }
}