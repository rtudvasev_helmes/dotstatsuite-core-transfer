﻿using DotStat.Common.Configuration.Dto;
using DotStat.Test.Moq;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;
using DotStat.Transfer.Test.Helper;
using NUnit.Framework;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Transfer.Model;
using DotStat.Common.Auth;
using Moq;
using Estat.Sdmxsource.Extension.Constant;

namespace DotStat.Transfer.Test.Unit.Producer
{
    [TestFixture]
    public class SdmxFilesProducerTests
    {
        private readonly Mock<IAuthorizationManagement> _authorisation = new Mock<IAuthorizationManagement>();

        public SdmxFilesProducerTests() {
            _authorisation.Setup(x => x.IsAuthorized(
                It.IsAny<DotStatPrincipal>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<PermissionType>())
            ).Returns(true);
        }

        [TestCase("sdmx/264D_264_SALDI+2.1.xml", "data/264D_264_SALDI_compact.xml", 12)]
        [TestCase("sdmx/264D_264_SALDI+2.1.xml", "data/264D_264_SALDI_generic.xml", 12)]
        [TestCase("sdmx/264D_264_SALDI+2.1.xml", "data/264D_264_SALDI.csv", 101)]
        [TestCase("sdmx/DF_DROPOUT_RT.xml", "data/DF_DROPOUT_RT_generic_series.xml", 810)]
        [TestCase("sdmx/DF_DROPOUT_RT.xml", "data/DF_DROPOUT_RT_generic_flat.xml", 810)]
        [TestCase("sdmx/DF_DROPOUT_RT.xml", "data/DF_DROPOUT_RT_generic_flat.zip", 810)]
        [TestCase("sdmx/KH_NIS,DF_AGRI_NO_TIME,2.0.xml", "data/KH_NIS,DF_AGRI_NO_TIME,2.0-data-structurespecific.xml", 183)]
        [TestCase("sdmx/KH_NIS,DF_AGRI_NO_TIME,2.0.xml", "data/KH_NIS,DF_AGRI_NO_TIME,2.0-data-generic.xml", 183)]
        [TestCase("sdmx/KH_NIS,DF_AGRI_NO_TIME,2.0.xml", "data/KH_NIS,DF_AGRI_NO_TIME,2.0-data.csv", 183)]
        public async Task Test_SdmxFile_Read_Dataflow_Reference(string structureFile, string dataFile, int expectedObservations)
        {
            var producer = new SdmxFileProducer(_authorisation.Object, new TestMappingStoreDataAccess(structureFile), new TestTempFileManager());

            var param = new SdmxFileToSqlTransferParam(new InternalSdmxParameters() {filepath = dataFile}, new DataspaceInternal() { Id = "Test" }, "en-US");

            Assert.IsFalse(producer.IsMetadataOnly(param).Result);
            var dataflow = producer.GetDataflow(param);
            var transferContent = await producer.Process(param, dataflow, CancellationToken.None);

            Assert.IsNotNull(transferContent.DataObservations);

            var observations        = 0;

            await using (var enumerator = transferContent.DataObservations.GetAsyncEnumerator(CancellationToken.None))
                while (await enumerator.MoveNextAsync())
                    observations++;

            Assert.AreEqual(expectedObservations, observations);
        }
    }
}