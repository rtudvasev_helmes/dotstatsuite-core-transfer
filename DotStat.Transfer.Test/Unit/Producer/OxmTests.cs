﻿using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Xml.Linq;
using DotStat.Test.Moq;
using DotStat.Transfer.Excel.Excel;
using NUnit.Framework;

namespace DotStat.Transfer.Test.Unit.Producer
{
    [TestFixture]
    public class OxmTests
    {
        private sealed class Test
        {
            public int MyProp = 12;
            public string MyString = "test";

            public string SomeMethod()
            {
                return "func";
            }
        }

        [Test]
        public void TestExpressionParser()
        {
            var expr = DynamicExpression.ParseLambda<Test, string>("MyProp.ToString() ");
            Assert.AreEqual(expr.Compile()(new Test()), "12");

            expr = DynamicExpression.ParseLambda<Test, string>("MyString");
            Assert.AreEqual(expr.Compile()(new Test()), "test");

            expr = DynamicExpression.ParseLambda<Test, string>("SomeMethod()");
            Assert.AreEqual(expr.Compile()(new Test()), "func");
        }


        [TestCase("edd/264D_264_SALDI-no-attributes.xml", 1, false, 0)]
        [TestCase("edd/264D_264_SALDI-pivot-table.xml", 1, false, 0)]
        [TestCase("edd/264D_264_SALDI-en-labels.xml", 1, false, 0)]
        [TestCase("edd/264D_264_SALDI-it-labels.xml", 1, false, 0)]
        [TestCase("edd/264D_264_SALDI-all-attributes.xml", 1, true, 1)]
        public void TestEddXMap(string eddPath, int observationMappingCount, bool datasetMappingfalse, int dimMappingCount)
        {
            var xml = XDocument.Load(new StreamReader(eddPath));
            var edd = ExcelDataDescription.Build(1, "test", "test", xml, new TestMappingStoreDataAccess(), "");

            Assert.IsNotNull(edd);
            Assert.AreEqual(observationMappingCount, edd.CoordMappingDescriptors.Count());
            Assert.AreEqual(datasetMappingfalse, edd.DatasetAttributesDescriptor != null);
            Assert.AreEqual(dimMappingCount, edd.DimAttributesDescriptors.Count());
        }
    }
}