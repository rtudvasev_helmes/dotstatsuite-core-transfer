﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Transfer.Interface;
using Microsoft.Extensions.Configuration;
using DotStat.Common.Logger;
using System;
using DotStat.Db.Service;

namespace DotStat.Transfer.Messaging.Consumer
{
    [ExcludeFromCodeCoverage]
    public class ConsumerService :  BackgroundService 
    {

        private readonly IServiceProvider _serviceProvider;
        private IConsumer _consumer;
        private readonly IConsumerFactory _consumerFactory;
        private readonly IConfiguration _configuration;
        private readonly IMailService _mailService;
        private readonly DotStatDbServiceResolver _dotStatDbServiceResolver;

        [ExcludeFromCodeCoverage]
        public ConsumerService(IServiceProvider serviceProvider, IConfiguration configuration, IConsumerFactory consumerFactory, IMailService mailService, DotStatDbServiceResolver dotStatDbServiceResolver)
        {
            try
            {
                _serviceProvider = serviceProvider;
                _mailService = mailService;
                _configuration = configuration;
                _consumerFactory = consumerFactory;
                _dotStatDbServiceResolver = dotStatDbServiceResolver;
            }
            catch (System.Exception e)
            {
                Log.Fatal(e);
            }
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                _consumer = _consumerFactory.Create(_serviceProvider, _configuration, _mailService, _dotStatDbServiceResolver);

                _consumer?.Start();
                
            }
            catch (System.Exception e)
            {
                Log.Fatal(e);
            }

            return Task.CompletedTask;
        }

        public override void Dispose()
        {
            _consumer?.Dispose();

            base.Dispose();
        }
    }

}
