﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading;
using DotStat.Common.Configuration;
using DotStat.Common.Exceptions;
using DotStat.Common.Logger;
using DotStat.Common.Messaging;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Interface;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Param;
using DotStat.Transfer.Utils;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using DotStat.Db.Service;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Dto;

namespace DotStat.Transfer.Messaging.Consumer
{
    public class RabbitMqConsumer : IConsumer 
    {
        private readonly ICommonManager _commonMngr;
        private readonly MessagingServiceConfiguration _messsageBrokerConfiguration;
        private readonly BaseConfiguration _baseConfiguration; 
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly IMailService _mailService;
        private IConnectionFactory _connectionFactory;
        private IConnection _connection;
        private IModel _channel;
        private readonly DotStatDbServiceResolver _dotStatDbServiceResolver;

        public RabbitMqConsumer(IConfiguration configuration, ICommonManager commonMngr, IMappingStoreDataAccess mappingStoreDataAccess, IMailService mailService, DotStatDbServiceResolver dotStatDbServiceResolver, IConnectionFactory connectionFactory = null)
        {
            _commonMngr = commonMngr;
            _mappingStoreDataAccess = mappingStoreDataAccess;
            _mailService = mailService;
            _messsageBrokerConfiguration =  configuration.GetSection(MessagingServiceConfiguration.MessageBrokerSettings).Get<MessagingServiceConfiguration>();
            _baseConfiguration = configuration.Get<BaseConfiguration>();

            _dotStatDbServiceResolver = dotStatDbServiceResolver;

            _connectionFactory = connectionFactory;
            _connectionFactory??= new ConnectionFactory
            {
                HostName = _messsageBrokerConfiguration.HostName,
                VirtualHost = _messsageBrokerConfiguration.VirtualHost,
                Port = _messsageBrokerConfiguration.Port,
                UserName = _messsageBrokerConfiguration.UserName,
                Password = _messsageBrokerConfiguration.Password
            };
        }
        
        public void Start()
        {
            if (!TryCreateConnection()) 
                return;

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += OnReceived;
            _channel.BasicConsume(_messsageBrokerConfiguration.QueueName, false, consumer);
        }

        /// <summary>
        /// Creates Connection and channel, these are meant to be long-lived, so create them early in the flow.
        /// </summary>
        /// <param name="connectionFactory"></param>
        /// <returns>true if successful connection is created</returns>
        private bool TryCreateConnection()
        {
            try
            {
                _connection = _connectionFactory.CreateConnection();
                _channel = _connection.CreateModel();

                _channel.QueueDeclare(
                    queue: _messsageBrokerConfiguration.QueueName,
                    durable: _messsageBrokerConfiguration.QueueDurable,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                _connection.ConnectionShutdown += (sender, ea) =>
                {
                    //if messagebroker is unreachable, try to restart 
                    TryCreateConnection();
                };
            }
            catch (System.Exception e) when (e is BrokerUnreachableException || e is OperationInterruptedException)
            {
                //Do nothing, try again at a later stage
                return false;
            }

            return true;
        }


        public void OnReceived(object sender, BasicDeliverEventArgs e)
        { 
            try
            {
                if (_connection == null || !_connection.IsOpen || _channel == null || _channel.IsClosed)
                    TryCreateConnection();

                var queueItem = JsonSerializer.Deserialize<List<QueueItem>>(Encoding.UTF8.GetString(e.Body.ToArray()));
                var dataSpace = queueItem.First().Dataspace.GetSpaceInternal(_baseConfiguration, _baseConfiguration.DefaultLanguageCode);

                Dataflow dataflowInDb = null;

                foreach (var item in queueItem)
                {
                    //payload in queue item contains all types of artefacts such as codelist ... not only dataflows
                    //method throws exception if artefact isnt a dataflow and will continue next artefact in the queue item list
                    dataflowInDb = _mappingStoreDataAccess.GetDataflow(
                        item.Dataspace,
                        item.ArtefactAgency,
                        item.ArtefactId,
                        item.ArtefactVersion,
                        false);

                    if (dataflowInDb is null)
                        break;

                    var result = Task.Run(async ()=> await InitDataFlow(dataflowInDb, dataSpace, queueItem.First().PrincipaEmail)).Result;
                    if(!result)
                        _channel?.BasicNack(e.DeliveryTag, false, true);

                    //end loop since currently only one single dataflow per message can be transaction
                    break;
                }

                _channel.BasicAck(e.DeliveryTag, false);
            }
            catch 
            {
                _channel?.BasicNack(e.DeliveryTag, false, true);
            }

        }
        public void Dispose()
        {
            if(_connection?.IsOpen == true)
                _connection.Close(); //Closes connection and all its channelstion.Close(); //Closes connection and all its channels
        }

        private async Task<bool> InitDataFlow(Dataflow dataFlow, DataspaceInternal dataSpace, string userEmail)
        {
            var initSuccessful = false;
            var cancellationToken = CancellationToken.None;
            var transferParam = new TransferParam()
            {
                TransactionType = TransactionType.InitDataFlow,
                CultureInfo = CultureInfo.GetCultureInfo(_baseConfiguration.DefaultLanguageCode),
                DestinationDataspace = dataSpace,
                TargetVersion = TargetVersion.Live,
                PITReleaseDate = null
            };

            var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace.Id);

            var transactionResult = new TransactionResult()
            {
                TransactionType = transferParam.TransactionType,
                TransactionId = transferParam.Id,
                DestinationDataSpaceId = dataSpace.Id,
                User = userEmail,
                Aftefact = dataFlow.FullId
            };

            try
            {
                initSuccessful = await _commonMngr.InitDataDbObjectsOfDataflow(
                    transferParam, dataFlow.FullId.GetDataflow(_baseConfiguration.DefaultLanguageCode), cancellationToken, true);

            }
            catch (System.Exception exception)
            {
                if (exception is DotStatException)
                    Log.Error(exception);
                else
                    Log.Fatal(exception);

                initSuccessful = false;
            }
            finally
            {
                if (dotStatDbService != null)
                    transactionResult.TransactionStatus = Task.Run(async () => await dotStatDbService.GetFinalTransactionStatus(transferParam.Id, CancellationToken.None)).Result;

                await _mailService.SendMail(
                    transactionResult,
                    _baseConfiguration.DefaultLanguageCode,
                    false
                );
            }

            return initSuccessful;
        }
    }
}
