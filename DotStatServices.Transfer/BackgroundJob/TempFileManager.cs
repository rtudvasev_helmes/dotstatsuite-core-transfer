﻿using DotStat.Common.Configuration;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Transfer.Model;
using DotStat.Transfer.Utils;
using DotStatServices.Transfer.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Net.Http.Headers;
using System;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace DotStatServices.Transfer.BackgroundJob
{
    public class TempFileManager: ITempFileManager
    {
        private const int TaskWaitInMinutes = 5;
        private readonly DirectoryInfo _tempFileDirectory;
        private readonly int _tempFileMaxAgeInHours;
        private readonly int _minPercentageDiskSpace; 
        private readonly FormOptions _defaultFormOptions;
        private readonly BaseConfiguration _configuration;

        public TempFileManager(BaseConfiguration configuration, FormOptions formOptions)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));

            _tempFileDirectory = GetTempDir(_configuration.TempFileDirectory);
            //Enforce temp files deletion after 1 hour, even if the value given to the configuration TempFileMaxAgeInHours is set to 0 or less. 
            _tempFileMaxAgeInHours = _configuration.TempFileMaxAgeInHours <= 0 ? 1 : _configuration.TempFileMaxAgeInHours;
            //Enforce min percentage of available disk space = 25, when the value provided is under or above a valid % value
            _minPercentageDiskSpace =
                _configuration.MinPercentageDiskSpace <= 0 || _configuration.MinPercentageDiskSpace >= 100
                    ? 25
                    : _configuration.MinPercentageDiskSpace;
            _defaultFormOptions = formOptions;
        }

        public async Task<FormValueProvider> StreamBodyToDisk(HttpRequest request, IFileParameter parameters)
        {
            if (!MultipartRequestHelper.IsMultipartContentType(request.ContentType))
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.WrongContentType, _configuration.DefaultLanguageCode),
                    request.ContentType));
            }

            var formAccumulator = new KeyValueAccumulator();
            var boundary = MultipartRequestHelper.GetBoundary(
                MediaTypeHeaderValue.Parse(request.ContentType),
                _defaultFormOptions.MultipartBoundaryLengthLimit,
                _configuration.DefaultLanguageCode);
            var reader = new MultipartReader(boundary, request.Body);

            var section =  await reader.ReadNextSectionAsync();
            while (section != null)
            {
                var hasContentDispositionHeader = ContentDispositionHeaderValue.TryParse(section.ContentDisposition, out var contentDisposition);

                if (hasContentDispositionHeader)
                {
                    var key = HeaderUtilities.RemoveQuotes(contentDisposition.Name);
                    //Threat segment as file
                    if (MultipartRequestHelper.HasFileContentDisposition(contentDisposition))
                    {
                        var nameField = parameters.GetFileFieldName(key.Value);
                        //validate extension
                        contentDisposition.FileName.Value.IsValidExtension(nameField, _configuration.DefaultLanguageCode, parameters.GetFieldAllowedExtensions(_configuration, key.Value));

                        //validate content-type
                        section.ContentType.IsValidContentType(nameField, _configuration.DefaultLanguageCode, parameters.GetFieldAllowedContentTypes(_configuration, key.Value));

                        string tempFilePath;
                        await using (var stream = TryCreateTempFileStream())
                        {
                            await section.Body.CopyToAsync(stream);
                            tempFilePath = stream.Name;
                        }
                        //todo: validate signature

                        if (!string.IsNullOrEmpty(nameField))
                            formAccumulator.Append(nameField, contentDisposition.FileName.Value);

                        var pathField = parameters.GetPathFieldName(key.Value);
                        if (!string.IsNullOrEmpty(pathField))
                            formAccumulator.Append(pathField, tempFilePath);
                    }
                    //Treat section as key value parameter
                    else if (MultipartRequestHelper.HasFormDataContentDisposition(contentDisposition))
                    {
                        var encoding = section.GetEncoding();
                        using var streamReader = new StreamReader(
                            section.Body,
                            encoding,
                            detectEncodingFromByteOrderMarks: true,
                            bufferSize: 1024,
                            leaveOpen: true);

                        // The value length limit is enforced by MultipartBodyLengthLimit
                        var value = await streamReader.ReadToEndAsync();
                        if (string.Equals(value, "undefined", StringComparison.OrdinalIgnoreCase))
                        {
                            value = string.Empty;
                        }
                        formAccumulator.Append(key.Value, value);

                        if (formAccumulator.ValueCount > _defaultFormOptions.ValueCountLimit)
                        {
                            throw new DotStatException(string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.RequestBodyKeyLimit, _configuration.DefaultLanguageCode),
                                _defaultFormOptions.ValueCountLimit));
                        }
                    }
                }

                section = await reader.ReadNextSectionAsync();
            }

            // Bind form data to a model
            var formValueProvider = new FormValueProvider(
                BindingSource.Form,
                new FormCollection(formAccumulator.GetResults()),
                CultureInfo.CurrentCulture);

            return formValueProvider;
        }
        
        public FileStream TryCreateTempFileStream(string prefix=null)
        {
            if(!HasAvailableDiskSpace())
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TemporaryFileDirectoryFull));
            try
            {
                return File.Create(GetTempFileName(prefix));
            }
            catch (Exception)
            {
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TemporaryFileDirectoryNotAccessible));
            }
        }

        public bool DeleteTempFile(string file)
        {
            if (!File.Exists(file)) return false;
            var fileName = Path.GetFileName(file);
            try
            {
                File.Delete(file);
            }
            catch (Exception e)
            {
                var error= new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DeleteTempFileFailImport, _configuration.DefaultLanguageCode),
                    fileName),
                    e);
                Log.Error(error);
                return false;
            }
            return true;
        }
        
        private bool HasAvailableDiskSpace()
        {
            var drive = new DriveInfo(_tempFileDirectory.FullName);

            var totalBytes = drive.TotalSize;
            var freeBytes = drive.AvailableFreeSpace;

            var freePercent = (int)((100 * freeBytes) / totalBytes);

            return freePercent >= _minPercentageDiskSpace;
        }

        public void ClearTempFiles()
        {
            var now = DateTime.Now;
            var fileName = "";
            try
            {
                foreach (var f in _tempFileDirectory.GetFiles("*.*", SearchOption.TopDirectoryOnly))
                {
                    if (!(now.Subtract(f.CreationTime).TotalHours >= _tempFileMaxAgeInHours)) continue;
                    fileName = f.Name;
                    f.Delete();
                }
            }
            catch(Exception e)
            {
                var error = new Exception(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DeleteTempFileFailBackgroundTask),
                        fileName),
                    e);
                Log.Error(error);
            }
        }
        
        private static DirectoryInfo GetTempDir(string dir)
        {
            try
            {
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
                return new DirectoryInfo(dir);
            }
            catch(UnauthorizedAccessException)
            {
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TemporaryFileDirectoryNotAccessible));
            }
            catch (System.Security.SecurityException)
            {
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TemporaryFileDirectoryNotAccessible));
            }
            catch (Exception)
            {
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TemporaryFileDirectoryNotValid));
            }
        }

        internal class TempFilePurgeService : BackgroundService
        {
            private readonly ITempFileManager _tempFileManager;

            public TempFilePurgeService(ITempFileManager tempFileManager)
            {
                _tempFileManager = tempFileManager;
            }

            protected override async Task ExecuteAsync(CancellationToken cancellationToken)
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    _tempFileManager.ClearTempFiles();

                    await Task.Delay(TaskWaitInMinutes * 1000 * 60, cancellationToken);
                }
            }
        }

        public static void RegisterService(IServiceCollection services)
        {
            services.AddSingleton<IHostedService, TempFilePurgeService>();
        }

        public string GetTempFileName(string filename=null) => Path.Combine(_tempFileDirectory.FullName, filename ?? $"{DateTime.Now:yyyy_MM_dd_HH_mm_ss_ffffff}.tmp");

    }
}
