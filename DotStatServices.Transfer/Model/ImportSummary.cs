﻿using System;
using System.Collections.Generic;
using DotStat.Domain;

namespace DotStatServices.Transfer.Model
{
    public sealed class ImportSummary
    {
        public int RequestId { get; set; }
        public string Action { get; set; }
        public string UserEmail { get; set; }
        public string Artefact { get; set; }
        public string SourceDataspace { get; set; }
        public string DestinationDataspace { get; set; }
        public string SourceData { get; set; }
        public DateTime? SubmissionTime { get; set; }
        public DateTime? ExecutionStart { get; set; }
        public DateTime? ExecutionEnd { get; set; }
        public string ExecutionStatus { get; set; }
        public string Outcome { get; set; }
        public IEnumerable<TransactionLog> Logs { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}
