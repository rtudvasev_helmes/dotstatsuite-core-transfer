using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Service;
using DotStat.DB.Repository;
using DotStat.Domain;
using DotStat.Transfer.Interface;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Model;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;
using DotStat.Transfer.Utils;
using DotStatServices.Transfer.BackgroundJob;
using DotStatServices.Transfer.Utilities;
using Estat.Sdmxsource.Extension.Constant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using static DotStat.Common.Localization.LocalizationRepository;
using Extensions = DotStat.Transfer.Utils.Extensions;
using DotStat.MappingStore;

namespace DotStatServices.Transfer.Controllers
{
    /// <summary>
    /// Import controller
    /// </summary>
    [ApiVersion("1.2")]
    [ApiVersion("2")]
    [ApiController]
    [Route("{apiVersion:apiVersion}/")]
    public class ImportController : ControllerBase
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly ITransferManager<ExcelToSqlTransferParam> _excelTransferManager;
        private readonly ITransferManager<SdmxFileToSqlTransferParam> _sdmxFileTransferManager;
        private readonly ITransferManager<SdmxMetadataFileToSqlTransferParam> _sdmxMetadataFileTransferManager; 
        private readonly ITransferManager<SdmxUrlToSqlTransferParam> _sdmxUrlTransferManager;
        private readonly ITransferManager<SdmxMetadataUrlToSqlTransferParam> _sdmxMetadataUrlTransferManager;
        private readonly IMailService _mailService;
        private readonly BackgroundQueue _backgroundQueue;
        private readonly BaseConfiguration _configuration;
        private readonly IAuthConfiguration _authConfiguration;
        private readonly ITempFileManager _tempFileManager;
        private readonly UnitOfWorkResolver _unitOfWorkResolver;
        private readonly DotStatDbServiceResolver _dotStatDbServiceResolver;
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;

        /// <summary>
        /// Constructor for this Controlled filled by DryIoc
        /// </summary>
        public ImportController(
                IHttpContextAccessor contextAccessor,
                IAuthorizationManagement authorizationManagement,
                ITransferManager<ExcelToSqlTransferParam> excelTransferManager, 
                ITransferManager<SdmxFileToSqlTransferParam> sdmxFileTransferManager,
                ITransferManager<SdmxMetadataFileToSqlTransferParam> sdmxMetadataFileTransferManager,
                ITransferManager<SdmxUrlToSqlTransferParam> sdmxUrlTransferManager,
                ITransferManager<SdmxMetadataUrlToSqlTransferParam> sdmxMetadataUrlTransferManager,
                BaseConfiguration configuration,
                IAuthConfiguration authConfiguration,
                IMailService mailService,
                BackgroundQueue backgroundQueue,
                ITempFileManager tempFileManager,
                UnitOfWorkResolver unitOfWorkResolver,
                DotStatDbServiceResolver dotStatDbServiceResolver,
                IMappingStoreDataAccess mappingStoreDataAccess
            )
        {
            _contextAccessor = contextAccessor;
            _authorizationManagement = authorizationManagement;
            _excelTransferManager = excelTransferManager;
            _sdmxFileTransferManager = sdmxFileTransferManager;
            _sdmxMetadataFileTransferManager = sdmxMetadataFileTransferManager;
            _sdmxUrlTransferManager = sdmxUrlTransferManager;
            _sdmxMetadataUrlTransferManager = sdmxMetadataUrlTransferManager;
            _configuration = configuration;
            _authConfiguration = authConfiguration;
            _mailService = mailService;
            _backgroundQueue = backgroundQueue;
            _tempFileManager = tempFileManager;
            _unitOfWorkResolver = unitOfWorkResolver;
            _dotStatDbServiceResolver = dotStatDbServiceResolver;
            _mappingStoreDataAccess = mappingStoreDataAccess;
        }

        /// <summary>
        /// Submit an import request of data or referential metadata from EDD + Excel files into provided dataspace
        /// </summary>
        /// <remarks>
        /// Example: Advance validation import to the 'design' dataspace, using the eddFile 'importexample_EDD.xml' and the Excel file 'dataexample.xlsx'
        /// 
        ///     POST /import/excel
        ///     {
        ///        "dataspace": "design",
        ///        "eddFile": "importexample_EDD.xml", 
        ///        "excelFile": "dataexample.xlsx",
        ///        "validationType": 1
        ///     }
        ///     
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">Import task successfully submitted</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>        
        [HttpPost]
        [MapToApiVersion("1.2")]
        [MapToApiVersion("2")]
        [Route("import/excel")]
        [DisableFormValueModelBinding]
        public async Task<ActionResult<OperationResult>> ImportExcel([FromForm] ExcelImportParameters inputImportParameters)
        {
            var cancellationToken = CancellationToken.None;
            var internalImportParameters = new InternalExcelParameters();
           
            var formModel = await _tempFileManager.StreamBodyToDisk(Request, inputImportParameters);
            var bindingSuccessful = await TryUpdateModelAsync(internalImportParameters, "", formModel);
            if (!bindingSuccessful)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
            }

            internalImportParameters.ImportValidationType = internalImportParameters.validationType == ImportValidationType.ImportWithBasicValidation || internalImportParameters.validationType == null
                ? ValidationType.ImportWithBasicValidation
                : ValidationType.ImportWithFullValidation;

            return await ImportExcel(internalImportParameters, cancellationToken);
        }

        /// <summary>
        /// Submit a validation of an import request of data or referential metadata from EDD + Excel files into provided dataspace
        /// </summary>
        /// <remarks>
        /// Example: Validation of an import to the 'design' dataspace, using the eddFile 'importexample_EDD.xml' and the Excel file 'dataexample.xlsx'
        /// 
        ///     POST /validate/excel
        ///     {
        ///        "dataspace": "design",
        ///        "eddFile": "importexample_EDD.xml", 
        ///        "excelFile": "dataexample.xlsx"
        ///     }
        ///     
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">Validation task successfully submitted</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>        
        [HttpPost]
        [MapToApiVersion("1.2")]
        [MapToApiVersion("2")]
        [Route("validate/excel")]
        [DisableFormValueModelBinding]
        public async Task<ActionResult<OperationResult>> ValidateImportExcel([FromForm] BaseExcelParameters inputImportParameters)
        {
            var cancellationToken = CancellationToken.None;
            var formModel = await _tempFileManager.StreamBodyToDisk(Request, inputImportParameters);
            var internalImportParameters = new InternalExcelParameters();
            var bindingSuccessful = await TryUpdateModelAsync(internalImportParameters, "", formModel);
            if (!bindingSuccessful)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
            }
            internalImportParameters.ImportValidationType = ValidationType.FullValidationOnly;
            return await ImportExcel(internalImportParameters, cancellationToken);
        }

        private async Task<ActionResult<OperationResult>> ImportExcel(InternalExcelParameters internalExcelParameters, CancellationToken cancellationToken)
        {
            //Set default values
            internalExcelParameters.targetVersion ??= TargetVersion.Live;
            internalExcelParameters.restorationOptionRequired ??= false;
            if (internalExcelParameters.targetVersion != TargetVersion.PointInTime)
            {
                internalExcelParameters.PITReleaseDate = null;
                internalExcelParameters.restorationOptionRequired = false;
            }

            var language = internalExcelParameters.lang ?? _configuration.DefaultLanguageCode;

            //Validate files
            internalExcelParameters.EddFileLocalPath.TempLocalFileExists("eddFile", language);
            internalExcelParameters.ExcelFileLocalPath.TempLocalFileExists("excelFile", language);

            var transferParam = new ExcelToSqlTransferParam
            {
                FilesToDelete = new[] { internalExcelParameters.EddFileLocalPath, internalExcelParameters.ExcelFileLocalPath }
            };

            try
            {
                var destinationDataspace = internalExcelParameters.dataspace.GetSpaceInternal(_configuration, language);
                
                if (string.IsNullOrEmpty(internalExcelParameters.EddFileLocalPath))
                {
                    throw new DotStatException(string.Format(GetLocalisedResource(Localization.ResourceId.FileAttachmentNotProvided, language), "excelFile"));
                }

                transferParam.DestinationDataspace = destinationDataspace;
                transferParam.EddFilePath = internalExcelParameters.EddFileLocalPath;
                transferParam.ExcelFilePath = internalExcelParameters.ExcelFileLocalPath;
                transferParam.CultureInfo = CultureInfo.GetCultureInfo(language);
                transferParam.TargetVersion = (TargetVersion)internalExcelParameters.targetVersion;
                transferParam.PITReleaseDate = internalExcelParameters.PITReleaseDate.GetPITReleaseDate(language);
                transferParam.PITRestorationAllowed = (bool)internalExcelParameters.restorationOptionRequired;
                transferParam.ValidationType = internalExcelParameters.ImportValidationType;
                transferParam.DataSource = string.Format(GetLocalisedResource(Localization.ResourceId.EmailSummaryDataSourceEddExcel, language),
                    internalExcelParameters.OriginalEddFileName, internalExcelParameters.OriginalExcelFileName);

                return await Import(_excelTransferManager, transferParam, cancellationToken);
            }
            catch
            {
                //cleanup temp local files if there are any errors
                foreach (var fileName in transferParam.FilesToDelete)
                {
                    _tempFileManager.DeleteTempFile(fileName);
                }

                throw;
            }
        }

        /// <summary>
        /// Submit an import request of data or referential metadata from SDMX file (xml|csv) into provided dataspace
        /// </summary>
        /// <param name="dataspace">The ID of the dataspace to where the values will be stored. <br></br> Example: <code>design</code></param>
        /// <param name="lang">Optional - Two letter (ISO Alpha-2) language code to produce the response message/email</param>
        /// <param name="dataflow">Obsolete - The artefact value is now directly read from the input file</param>
        /// <param name="filepath">Optional - The remote location (path/url) of the input file, containing the data or referential metadata to be imported. <br></br> Make sure that the remote file is accessible by the transfer-service</param>
        /// <param name="file">Optional - The input SDMX data or referential metadata file to be imported/validated (if no filepath is given). <br></br> A maximum size file limit might apply</param>
        /// <param name="targetVersion">Optional - The target version of the data and referential metadata to be used during the import. <br></br> Live (0), Point in Time PIT (1), Default (Live)</param>
        /// <param name="PITReleaseDate">Optional - Point in Time release date (YYYY-MM-DDThh:mm:ss.sTZD) <br></br>Example: <code>2022-06-04T10:16:01</code>, <code>2022-06-04T08:16:01Z</code>, <code>2022-06-04T10:16:01+02:00</code>, <code>2022-06-04T10:16:01.021+02:00</code></param>
        /// <param name="restorationOptionRequired">Optional - Indicate if the current LIVE version should be kept for restoration purposes when PIT release becomes active</param>
        /// <param name="validationType">Optional - The type of validation to use during import:  <br></br> Import With <b>Basic Validation (0)</b>, Import With <b>Advanced Validation (1)</b>, Default (Import With Basic Validation)</param>
        /// <remarks>
        /// Example: Advanced validation import to the 'design' dataspace, using the CSV file 'dataexample.csv'
        /// 
        ///     POST /import/sdmxFile
        ///     {
        ///        "dataspace": "design",
        ///        "file": "dataexample.csv",
        ///        "validationType": 1
        ///     }
        ///     
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">Import task successfully submitted</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        [HttpPost]
        [MapToApiVersion("1.2")]
        [MapToApiVersion("2")]
        [Route("import/sdmxFile")]
        [DisableFormValueModelBinding]
        public async Task<ActionResult<OperationResult>> ImportSdmxFile([FromForm] SdmxImportParameters inputImportParameters)
        {
            var cancellationToken = CancellationToken.None;
            var formModel = await _tempFileManager.StreamBodyToDisk(Request, inputImportParameters);
            var internalImportParameters = new InternalSdmxParameters();
            var bindingSuccessful = await TryUpdateModelAsync(internalImportParameters, "", formModel);
            if (!bindingSuccessful)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
            }

            internalImportParameters.ImportValidationType = internalImportParameters.validationType == ImportValidationType.ImportWithBasicValidation || internalImportParameters.validationType == null
                ? ValidationType.ImportWithBasicValidation
                : ValidationType.ImportWithFullValidation;

            return await ImportSdmxFile(internalImportParameters, cancellationToken);
        }

        /// <summary>
        /// Submit a validation of an import request of data or referential metadata from SDMX file (xml|csv) to a given dataspace
        /// </summary>
        /// <remarks>
        /// Example: Make a validation request to the 'design' dataspace, using the CSV file 'dataexample.csv'
        /// 
        ///     POST /validate/sdmxFile
        ///     {
        ///        "dataspace": "design",
        ///        "file": "dataexample.csv"
        ///     }
        ///     
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">Import task successfully submitted</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        [HttpPost]
        [MapToApiVersion("1.2")]
        [MapToApiVersion("2")]
        [Route("validate/sdmxFile")]
        [DisableFormValueModelBinding]
        public async Task<ActionResult<OperationResult>> ValidateImportSdmxFile([FromForm] BaseSdmxParameters inputImportParameters)
        {
            var cancellationToken = CancellationToken.None;
            var formModel = await _tempFileManager.StreamBodyToDisk(Request, inputImportParameters);
            var internalImportParameters = new InternalSdmxParameters();
            var bindingSuccessful = await TryUpdateModelAsync(internalImportParameters, "", formModel);
            if (!bindingSuccessful)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
            }
            internalImportParameters.ImportValidationType = ValidationType.FullValidationOnly;
            return await ImportSdmxFile(internalImportParameters, cancellationToken);
        }

        private async Task<ActionResult<OperationResult>> ImportSdmxFile(InternalSdmxParameters internalSdmxParameters, CancellationToken cancellationToken)
        {
            //Set default values
            internalSdmxParameters.targetVersion ??= TargetVersion.Live;
            internalSdmxParameters.restorationOptionRequired ??= false;
            if (internalSdmxParameters.targetVersion != TargetVersion.PointInTime)
            {
                internalSdmxParameters.PITReleaseDate = null;
                internalSdmxParameters.restorationOptionRequired = false;
            }
            
            var language = internalSdmxParameters.lang ?? _configuration.DefaultLanguageCode;
            SdmxMetadataFileToSqlTransferParam sdmxMetadataFileToSqlTransferParam = null;
            SdmxFileToSqlTransferParam sdmxFileTransferParam = null;
            SdmxUrlToSqlTransferParam sdmxUrlToSqlTransferParam = null;
            SdmxMetadataUrlToSqlTransferParam sdmxMetadataUrlToSqlTransferParam = null;

            try
            {
                var destinationDataspace = internalSdmxParameters.dataspace.GetSpaceInternal(_configuration, language);

                ActionResult<OperationResult> requestMsg;

                //Import from sdmx url source
                if (Extensions.IsValidUrl(internalSdmxParameters.filepath))
                {
                    sdmxMetadataUrlToSqlTransferParam = new SdmxMetadataUrlToSqlTransferParam(internalSdmxParameters, destinationDataspace, language, Request.Headers);

                    if (await ((IMetadataProducer<SdmxMetadataUrlToSqlTransferParam>)_sdmxMetadataUrlTransferManager.Producer).IsMetadataOnly(sdmxMetadataUrlToSqlTransferParam))
                    {
                        requestMsg = await Import(_sdmxMetadataUrlTransferManager, sdmxMetadataUrlToSqlTransferParam, cancellationToken);
                    }
                    else
                    {
                        sdmxUrlToSqlTransferParam = new SdmxUrlToSqlTransferParam(internalSdmxParameters, destinationDataspace, language, Request.Headers)
                        {
                            FilePath = sdmxMetadataUrlToSqlTransferParam.FilePath,
                            FilesToDelete = new List<string>(sdmxMetadataUrlToSqlTransferParam.FilesToDelete)
                        };

                        requestMsg = await Import(_sdmxUrlTransferManager, sdmxUrlToSqlTransferParam, cancellationToken);
                    } 
                }
                //import from sdmx file
                else
                {
                    sdmxMetadataFileToSqlTransferParam = new SdmxMetadataFileToSqlTransferParam(internalSdmxParameters, destinationDataspace, language);

                    if (await ((IMetadataProducer<SdmxMetadataFileToSqlTransferParam>)_sdmxMetadataFileTransferManager.Producer).IsMetadataOnly(sdmxMetadataFileToSqlTransferParam))
                    {
                        requestMsg = await Import(_sdmxMetadataFileTransferManager, sdmxMetadataFileToSqlTransferParam, cancellationToken);
                    }
                    else
                    {
                        sdmxFileTransferParam = new SdmxFileToSqlTransferParam(internalSdmxParameters, destinationDataspace, language)
                        {
                            FilesToDelete = new List<string>(sdmxMetadataFileToSqlTransferParam.FilesToDelete)
                        };

                        requestMsg = await Import(_sdmxFileTransferManager, sdmxFileTransferParam, cancellationToken);
                    }
                }

                return requestMsg;
            }
            catch
            {
                //cleanup temp local files if there are any errors
                if (sdmxMetadataFileToSqlTransferParam != null)
                {
                    foreach (var fileName in sdmxMetadataFileToSqlTransferParam.FilesToDelete)
                    {
                        _tempFileManager.DeleteTempFile(fileName);
                    }
                }

                if (sdmxFileTransferParam != null)
                {
                    foreach (var fileName in sdmxFileTransferParam.FilesToDelete)
                    {
                        _tempFileManager.DeleteTempFile(fileName);
                    }
                }

                if (sdmxUrlToSqlTransferParam != null)
                {
                    foreach (var fileName in sdmxUrlToSqlTransferParam.FilesToDelete)
                    {
                        _tempFileManager.DeleteTempFile(fileName);
                    }
                }

                if (sdmxMetadataUrlToSqlTransferParam != null)
                {
                    foreach (var fileName in sdmxMetadataUrlToSqlTransferParam.FilesToDelete)
                    {
                        _tempFileManager.DeleteTempFile(fileName);
                    }
                }

                if (!string.IsNullOrEmpty(internalSdmxParameters.FileLocalPath))
                {
                    _tempFileManager.DeleteTempFile(internalSdmxParameters.FileLocalPath);
                }

                throw;
            }
        }

        private async Task<ActionResult<OperationResult>> Import<T>(ITransferManager<T> transferManager, T transferParam, CancellationToken cancellationToken) where T : TransferParam
        {
            try
            {
                var dataflow = transferManager.Producer.GetDataflow(transferParam);
                transferParam.Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping);

                //Check if the user can insert, update or delete to the destination dataspace
                if (!transferManager.Consumer.IsAuthorized(transferParam, dataflow))
                {
                    var template = GetLocalisedResource(Localization.ResourceId.UnauthorizedImport, transferParam.CultureInfo.TwoLetterISOLanguageName);
                    var errorMessage = string.Format(
                        template,
                        dataflow.FullId,
                        transferParam.DestinationDataspace.Id);

                    return StatusCode(StatusCodes.Status403Forbidden, new OperationResult(false, errorMessage));
                }

                var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace.Id);
                var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace.Id);

                transferParam.Id = await unitOfWork.TransactionRepository.GetNextTransactionId(cancellationToken);
                transferParam.TransactionType =
                    transferParam.ValidationType == ValidationType.FullValidationOnly
                        ? TransactionType.ValidateImport
                        : TransactionType.Import;

                var transactionResult = new TransactionResult()
                {
                    TransactionType = transferParam.TransactionType,
                    TransactionId = transferParam.Id,
                    DataSource = transferParam.DataSource,
                    SourceDataSpaceId = transferParam.SourceDataspace?.Id,
                    DestinationDataSpaceId = transferParam.DestinationDataspace?.Id,
                    Aftefact = dataflow != null ? $"{dataflow.AgencyId}:{dataflow.Base.Id}({dataflow.Version})" : string.Empty,
                    User = transferParam.Principal.Email,
                    TransactionStatus = TransactionStatus.Queued
                };

                LogHelper.RecordNewTransaction(transferParam.Id, transferParam.DestinationDataspace);

                var message = string.Format(GetLocalisedResource(Localization.ResourceId.SubmissionResult, transferParam.CultureInfo.TwoLetterISOLanguageName), transferParam.Id);

                Log.Notice(message);

                var transaction = await unitOfWork.TransactionRepository.CreateTransactionItem(
                    transferParam.Id, dataflow?.FullId, transferParam.Principal, 
                    transferParam?.SourceDataspace?.Id, transferParam?.DataSource,
                    transferParam.TransactionType, transferParam.TargetVersion,
                    cancellationToken);

                //Function with the main steps to execute the request
                async Task mainTask(CancellationToken backgroundCancellationToken)
                {
                    using var internalCancellationToken = backgroundCancellationToken.SetBackgroundTaskCompletionSteps(dotStatDbService, transferParam, cancellationToken);

                    try
                    {
                        await transferManager.Transfer(transferParam, transaction, internalCancellationToken.Token);
                    }
                    catch (DotStatException exception)
                    {
                        Log.Warn(GetLocalisedResource(Localization.ResourceId.NoObservationsProcessed, transferParam.CultureInfo.TwoLetterISOLanguageName));
                        Log.Error(exception);
                    }
                    catch (Exception exception)
                    {
                        if (exception is not TaskCanceledException && !internalCancellationToken.Token.IsCancellationRequested)
                        {
                            Log.Error(GetLocalisedResource(Localization.ResourceId.NoObservationsProcessed, transferParam.CultureInfo.TwoLetterISOLanguageName));
                            Log.Fatal(exception);
                        }
                    }
                    finally
                    {
                        transactionResult.TransactionStatus = await dotStatDbService.GetFinalTransactionStatus(transferParam.Id, CancellationToken.None);

                        await _mailService.SendMail(
                            transactionResult,
                            transferParam.CultureInfo.TwoLetterISOLanguageName,
                            _authorizationManagement.IsAuthorized(transferParam.Principal, PermissionType.AdminRole)
                        );

                        //cleanup temp local files if when the import has completed
                        foreach (var fileName in transferParam.FilesToDelete)
                        {
                            _tempFileManager.DeleteTempFile(fileName);
                        }
                    }

                    await Task.CompletedTask;
                }

                //Delegate to check if this request can begin to process, or if it should be placed back to the queue.
                async Task<bool> canBeProcessed(CancellationToken backgroundCancellationToken)
                {
                    Log.SetTransactionId(transferParam.Id);
                    Log.SetDataspaceId(transferParam.DestinationDataspace.Id);
                    return (await dotStatDbService.TryNewTransaction(transaction, dataflow, transferParam.Principal, _mappingStoreDataAccess, backgroundCancellationToken, onlyLockTransaction: true)).Success;
                }
        

                //Function to execute to notify/log that the request is still in progress.
                async Task notifyInProgressTask(CancellationToken backgroundCancellationToken) => await backgroundCancellationToken.SetStillProcessingLogging(transferParam);

                //Action to execute when the application is shutting down
                async void callBackAction() => await unitOfWork.TransactionRepository.MarkTransactionAsCanceled(transferParam.Id);

                _backgroundQueue.Enqueue(new TransactionQueueItem(canBeProcessed, mainTask, notifyInProgressTask, callBackAction));

                return new OperationResult(true, message);
            }
            catch (Exception exception)
            {
                Log.Fatal(exception);
                throw;
            }
        }

    }
}
