using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Service;
using DotStat.DB.Repository;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Interface;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Param;
using DotStat.Transfer.Utils;
using DotStatServices.Transfer.BackgroundJob;
using Estat.Sdmxsource.Extension.Constant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DotStatServices.Transfer.Controllers
{
    /// <summary>
    /// Point In Time controller
    /// </summary>
    [ApiVersion("1.2")]
    [ApiVersion("2")]
    [ApiController]
    [Route("{apiVersion:apiVersion}/")]
    public class PointInTimeController : ControllerBase
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly CommonManager _commonManager;
        private readonly BaseConfiguration _configuration;
        private readonly IAuthConfiguration _authConfiguration;
        private readonly IMailService _mailService;
        private readonly BackgroundQueue _backgroundQueue;
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly UnitOfWorkResolver _unitOfWorkResolver;
        private readonly DotStatDbServiceResolver _dotStatDbServiceResolver;
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;

        /// <summary>
        /// Constructor for this Controlled filled by DryIoc
        /// </summary>
        public PointInTimeController(
            IHttpContextAccessor contextAccessor,
            CommonManager commonManager, 
            BaseConfiguration configuration,
            IAuthConfiguration authConfiguration,
            IMailService mailService,
            BackgroundQueue backgroundQueue,
            IAuthorizationManagement authorizationManagement,
            UnitOfWorkResolver unitOfWorkResolver,
            DotStatDbServiceResolver dotStatDbServiceResolver,
            IMappingStoreDataAccess mappingStoreDataAccess
        )
        {
            _contextAccessor = contextAccessor;
            _commonManager = commonManager;
            _configuration = configuration;
            _authConfiguration = authConfiguration;
            _mailService = mailService;
            _backgroundQueue = backgroundQueue;
            _authorizationManagement = authorizationManagement;
            _unitOfWorkResolver = unitOfWorkResolver;
            _dotStatDbServiceResolver = dotStatDbServiceResolver;
            _mappingStoreDataAccess = mappingStoreDataAccess;
        }

        /// <summary>
        /// Get the Point-In-Time information of a dataflow for a given dataspace 
        /// </summary>
        /// <param name="dataspace">The ID of the dataspace where to consult the PIT information. <br></br> Example: <code>design</code></param>
        /// <param name="dataflow">The SDMX ID of the dataflow. <br></br> Format: 'AGENCYID:DATAFLOWID(VERSION)'. <br></br> Example: <code>OECD:DF_MEI(1.0)</code></param>
        /// <remarks>
        /// Example: Get the PIT information of the dataflow 'OECD:MEI(1.0)' in the 'design' dataspace
        /// 
        ///     POST /pointintime/PITInfo
        ///     {
        ///        "dataspace": "design",
        ///        "dataflow": "OECD:DF_MEI(1.0)"
        ///     }
        ///     
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">Information successfully retrieved</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        /// <response code="401">Unauthorized</response>
        [HttpPost]
        [MapToApiVersion("1.2")]
        [MapToApiVersion("2")]
        [Route("pointintime/PITInfo")]
        public async Task<ActionResult<OperationResult>> PitInfo(
            [FromForm, Required] string dataspace,
            [FromForm, Required] string dataflow
        )
        {
            var cancellationToken = CancellationToken.None;
            var transferParam = new TransferParam()
            {
                DestinationDataspace = dataspace.GetSpaceInternal(_configuration, _configuration.DefaultLanguageCode),
                Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping)
            };

            Log.SetDataspaceId(transferParam.DestinationDataspace.Id);

            var sourceDataFlow = dataflow.GetDataflow(_configuration.DefaultLanguageCode);

            var PITInfo = await _commonManager.GetPITInfo(transferParam, sourceDataFlow, cancellationToken);

            return new OkObjectResult(PITInfo);
        }

        /// <summary>
        /// Perform a Rollback of the Point-In-Time of a dataflow for a given dataspace
        /// </summary>
        /// <param name="dataspace">The ID of the dataspace where to perform the rollback. <br></br> Example: <code>design</code></param>
        /// <param name="dataflow">The SDMX ID of the dataflow. <br></br> Format: 'AGENCYID:DATAFLOWID(VERSION)'. <br></br> Example: <code>OECD:DF_MEI(1.0)</code></param>
        /// <remarks>
        /// Example: Perform a rollback of the PIT for the dataflow 'OECD:MEI(1.0)' in the 'design' dataspace
        /// 
        ///     POST /pointintime/rollback
        ///     {
        ///        "dataspace": "design",
        ///        "dataflow": "OECD:DF_MEI(1.0)"
        ///     }
        ///     
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">Rollback successfully submitted</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        /// <response code="401">Unauthorized</response>
        [HttpPost]
        [MapToApiVersion("1.2")]
        [MapToApiVersion("2")]
        [Route("pointintime/rollback")]
        public async Task<ActionResult<OperationResult>> Rollback(
            [FromForm, Required] string dataspace,
            [FromForm, Required] string dataflow
        )
        {
            var cancellationToken = CancellationToken.None;
            var transferParam = new TransferParam()
            {
                CultureInfo = CultureInfo.GetCultureInfo(_configuration.DefaultLanguageCode),
                DestinationDataspace = dataspace.GetSpaceInternal(_configuration, _configuration.DefaultLanguageCode),
                Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping),
                TransactionType = TransactionType.Rollback,
                TargetVersion = TargetVersion.Live
            };

            return await Rollback(transferParam, dataflow, cancellationToken);
        }

        /// <summary>
        /// Perform a Restoration of a released PIT version of a dataflow for a given dataspace 
        /// </summary>
        /// <param name="dataspace">The ID of the dataspace where to perform the restoration. <br></br> Example: <code>design</code></param>
        /// <param name="dataflow">The SDMX ID of the dataflow. <br></br> Format: 'AGENCYID:DATAFLOWID(VERSION)'. <br></br> Example: <code>OECD:DF_MEI(1.0)</code></param>
        /// <remarks>
        /// Example: Restore the previous version of the dataflow 'OECD:MEI(1.0)' in the 'design' dataspace
        /// 
        ///     POST /pointintime/restoration
        ///     {
        ///        "dataspace": "design",
        ///        "dataflow": "OECD:DF_MEI(1.0)"
        ///     }
        ///     
        /// </remarks>
        /// 
        /// <returns></returns>
        /// <response code="200">Restoration successfully submitted</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        /// <response code="401">Unauthorized</response>
        [HttpPost]
        [MapToApiVersion("1.2")]
        [MapToApiVersion("2")]
        [Route("pointintime/restoration")]
        public async Task<ActionResult<OperationResult>> Restore(
             [FromForm, Required] string dataspace,
             [FromForm, Required] string dataflow
         )
        {
            var cancellationToken = CancellationToken.None;
            var transferParam = new TransferParam()
            {
                CultureInfo = CultureInfo.GetCultureInfo(_configuration.DefaultLanguageCode),
                DestinationDataspace = dataspace.GetSpaceInternal(_configuration, _configuration.DefaultLanguageCode),
                Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping),
                TransactionType = TransactionType.Restore,
                TargetVersion = TargetVersion.PointInTime
            };

            return await Restore(transferParam, dataflow, cancellationToken);
        }

        #region private methods
        private async Task<ActionResult<OperationResult>> Rollback(TransferParam transferParam, string dataFlowParam, CancellationToken cancellationToken)
        {

            var destinationDataFlow = dataFlowParam.GetDataflow(transferParam.CultureInfo.TwoLetterISOLanguageName);
            var dataFlow = _commonManager.GetDataflow(
                transferParam.DestinationDataspace.Id, 
                destinationDataFlow);

            if (!_commonManager.IsAuthorized(transferParam, dataFlow))
                throw new TransferUnauthorizedException();

            var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace.Id);

            transferParam.Id = await unitOfWork.TransactionRepository.GetNextTransactionId(cancellationToken);

            // -----------------------------------------------------

            LogHelper.RecordNewTransaction(transferParam.Id, transferParam.DestinationDataspace);
            Log.SetTransactionId(transferParam.Id);
            Log.SetDataspaceId(transferParam.DestinationDataspace.Id);

            var message = string.Format(
                LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.SubmissionResult,
                    transferParam.CultureInfo.TwoLetterISOLanguageName
                ),
                transferParam.Id
            );

            Log.Notice(message);

            var transactionResult = new TransactionResult()
            {
                TransactionType = transferParam.TransactionType,
                TransactionId = transferParam.Id,
                DestinationDataSpaceId = transferParam.DestinationDataspace.Id,
                Aftefact = dataFlowParam,
                User = transferParam.Principal.Email,
                TransactionStatus = TransactionStatus.Queued
            };

            try
            {
                await _commonManager.Rollback(transferParam, dataFlow, cancellationToken);
            }
            catch (DotStatException exception)
            {
                Log.Error(exception);
            }
            catch (Exception exception)
            {
                Log.Fatal(exception);
            }
            finally
            {
                transactionResult.TransactionStatus = await dotStatDbService.GetFinalTransactionStatus(transferParam.Id, CancellationToken.None);
                await _mailService.SendMail(
                    transactionResult,
                    transferParam.CultureInfo.TwoLetterISOLanguageName,
                    _authorizationManagement.IsAuthorized(transferParam.Principal, PermissionType.AdminRole)
                );
            }

            return new OkObjectResult(new OperationResult(true, message));
        }

        private async Task<ActionResult<OperationResult>> Restore(TransferParam transferParam, string dataFlowParam, CancellationToken cancellationToken)
        {
            var destinationDataFlow = dataFlowParam.GetDataflow(transferParam.CultureInfo.TwoLetterISOLanguageName);
            var dataFlow = _commonManager.GetDataflow(
                transferParam.DestinationDataspace.Id, 
                destinationDataFlow);

            if (!_commonManager.IsAuthorized(transferParam, dataFlow))
                throw new TransferUnauthorizedException();

            var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace.Id);

            transferParam.Id = await unitOfWork.TransactionRepository.GetNextTransactionId(cancellationToken);

            // ---------------------------------------------

            LogHelper.RecordNewTransaction(transferParam.Id, transferParam.DestinationDataspace);

            var message = string.Format(
                LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.SubmissionResult,
                    transferParam.CultureInfo.TwoLetterISOLanguageName
                ),
                transferParam.Id
            );

            Log.Notice(message);

            try
            {
                var transaction = await unitOfWork.TransactionRepository.CreateTransactionItem(
                    transferParam.Id, dataFlow.FullId,
                    transferParam.Principal, null, null,
                    TransactionType.Restore, TargetVersion.Live, cancellationToken);

                //Function with the main steps to execute the request
                async Task mainTask(CancellationToken backgroundCancellationToken)
                {
                    using var internalCancellationToken = backgroundCancellationToken.SetBackgroundTaskCompletionSteps(dotStatDbService, transferParam, cancellationToken);
                    var transactionResult = new TransactionResult()
                    {
                        TransactionType = transferParam.TransactionType,
                        TransactionId = transferParam.Id,
                        DestinationDataSpaceId = transferParam.DestinationDataspace.Id, // NB not a typo
                        Aftefact = dataFlowParam,
                        User = transferParam.Principal.Email,
                        TransactionStatus = TransactionStatus.Unknown
                    };

                    try
                    {
                        await _commonManager.Restore(transferParam, transaction, dataFlow, internalCancellationToken.Token);
                    }
                    catch (DotStatException exception)
                    {
                        Log.Error(exception);
                    }
                    catch (Exception e)
                    {
                        if (e is not TaskCanceledException && !internalCancellationToken.Token.IsCancellationRequested)
                        {
                            Log.Fatal(e);
                        }
                    }
                    finally
                    {
                        transactionResult.TransactionStatus = await dotStatDbService.GetFinalTransactionStatus(transferParam.Id, CancellationToken.None);

                        await _mailService.SendMail(
                            transactionResult,
                            transferParam.CultureInfo.TwoLetterISOLanguageName,
                            _authorizationManagement.IsAuthorized(transferParam.Principal, PermissionType.AdminRole)
                        );
                    }

                    await Task.CompletedTask;
                }

                //Delegate to check if this request can begin to process, or if it should be placed back to the queue.
                async Task<bool> canBeProcessed(CancellationToken backgroundCancellationToken)
                {
                    Log.SetTransactionId(transferParam.Id);
                    Log.SetDataspaceId(transferParam.DestinationDataspace.Id);
                    return (await dotStatDbService.TryNewTransaction(transaction, dataFlow, transferParam.Principal, _mappingStoreDataAccess, backgroundCancellationToken, onlyLockTransaction: true)).Success;
                }

                //Function to execute to notify/log that the request is still in progress.
                async Task notifyInProgressTask(CancellationToken backgroundCancellationToken) => await backgroundCancellationToken.SetStillProcessingLogging(transferParam);

                //Action to execute when the application is shutting down
                async void callBackAction() => await unitOfWork.TransactionRepository.MarkTransactionAsCanceled(transferParam.Id);

                _backgroundQueue.Enqueue(new TransactionQueueItem(canBeProcessed, mainTask, notifyInProgressTask, callBackAction));

                return new OkObjectResult(new OperationResult(true, message));
            }
            catch (Exception exception)
            {
                Log.Fatal(exception);

                throw;
            }
        }
        #endregion
    }
}
