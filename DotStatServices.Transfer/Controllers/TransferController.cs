using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.DB.Repository;
using DotStat.Db.Service;
using DotStat.Domain;
using DotStat.Transfer.DataflowManager;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Param;
using DotStatServices.Transfer.BackgroundJob;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using DotStat.MappingStore;
using DotStat.Transfer.Interface;
using DotStat.Transfer.Utils;
using Estat.Sdmxsource.Extension.Constant;
using static DotStat.Common.Localization.LocalizationRepository;

namespace DotStatServices.Transfer.Controllers
{
    /// <summary>
    /// Transfer controller
    /// </summary>
    [ApiVersion("1.2")]
    [ApiVersion("2")]
    [ApiController]
    [Route("{apiVersion:apiVersion}/")]
    public class TransferController : ControllerBase
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly ITransferManager<SqlToSqlTransferParam> _transferMngrSql;
        private readonly BaseConfiguration _configuration;
        private IAuthConfiguration _authConfiguration;
        private readonly IMailService _mailService;
        private readonly BackgroundQueue _backgroundQueue;
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly UnitOfWorkResolver _unitOfWorkResolver;
        private readonly DotStatDbServiceResolver _dotStatDbServiceResolver;
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;

        /// <summary>
        /// Constructor for this Controlled filled by DryIoc
        /// </summary>
        public TransferController(
            IHttpContextAccessor contextAccessor,
            ITransferManager<SqlToSqlTransferParam> transferMngrSql,
            BaseConfiguration configuration,
            IAuthConfiguration authConfiguration,
            IMailService mailService,
            BackgroundQueue backgroundQueue, 
            IAuthorizationManagement authorizationManagement,
            UnitOfWorkResolver unitOfWorkResolver,
            DotStatDbServiceResolver dotStatDbServiceResolver,
            IMappingStoreDataAccess mappingStoreDataAccess
        )
        {
            _contextAccessor = contextAccessor;
            _transferMngrSql = transferMngrSql;
            _configuration = configuration;
            _authConfiguration = authConfiguration;
            _mailService = mailService;
            _backgroundQueue = backgroundQueue;
            _authorizationManagement = authorizationManagement;
            _unitOfWorkResolver = unitOfWorkResolver;
            _dotStatDbServiceResolver = dotStatDbServiceResolver;
            _mappingStoreDataAccess = mappingStoreDataAccess;
        }

        /// <summary>
        /// Transfer data and/or referential metadata from one source dataspace to a destination dataspace. 
        /// </summary>
        /// <param name="sourceDataspace">The ID of the source dataspace. <br></br> Example: <code>design</code></param>
        /// <param name="sourceDataflow">The SDMX ID of the source dataflow. <br></br> Format: 'AGENCYID:DATAFLOWID(VERSION)'. <br></br> Example: <code>OECD:DF_MEI(1.0)</code></param>
        /// <param name="sourceQuery">Optional - SDMX query to filter the data/referential metadata to be transferred (as provided to the NSIWS to get data) <br></br> Example: <code><![CDATA["D.NOK.EUR.SP00.A?startPeriod=2012-01-01&endPeriod=2010-02-01"]]></code></param>
        /// <param name="destinationDataspace">The ID of the destination dataspace. <br></br> Example: <code>staging</code></param>
        /// <param name="destinationDataflow">The SDMX ID of the destination dataflow. <br></br> Format: 'AGENCYID:DATAFLOWID(VERSION)'. <br></br> Example: <code>OECD:DF_MEI(1.0)</code></param>
        /// <param name="lang">Optional - Two letter (ISO Alpha-2) language code to produce the response message/email<br></br>Example: <code>en</code></param>
        /// <param name="sourceVersion">Optional - The version of the data and referential metadata to be used from the <b>source</b> dataspace. <br></br>Possible values: Live (0), Point in Time PIT (1), Default (Live)</param>
        /// <param name="targetVersion">Optional - The version of the data and referential metadata to be used to the <b>destination</b> dataspace. <br></br>Possible values: Live (0), Point in Time PIT (1), Default (Live)</param>
        /// <param name="PITReleaseDate">Optional - Point in time release date (YYYY-MM-DDThh:mm:ss.sTZD) Example: <code>2022-06-04T10:16:01</code>, <code>2022-06-04T08:16:01Z</code>, <code>2022-06-04T10:16:01+02:00</code>, <code>2022-06-04T10:16:01.021+02:00</code></param>
        /// <param name="restorationOptionRequired">Optional - Indicate if the current LIVE version should be kept for restoration purposes when PIT release becomes active</param>
        /// <param name="validationType">Optional - The type of validation to use during import:  <br></br>Possible values: Import With <b>Basic Validation (0)</b>, Import With <b>Advanced Validation (1)</b>, Default (Import With Basic Validation)</param>
        /// <param name="updatedAfter">Optional - Only the data that has been changed after this  date (YYYY-MM-DD hh:mm:ss) will be transfered Example: <code>2022-06-04 10:16:01</code>, Default (all data regardless of when it was changed)</param>
        /// <param name="transferContent">Optional - The content to be transferred across dataspaces: <br></br>Possible values: Transfer <b>both data and referential metadata (0)</b>, Transfer <b>data only (1)</b>, Transfer <b>referential metadata only (2)</b>, Default (Transfer both data and referential metadata)</param>
        /// <remarks>
        /// Example: transfer request
        /// 
        ///     POST /transfer/dataflow
        ///     {
        ///        "sourceDataspace": "design",
        ///        "sourceDataflow": "OECD:DF_MEI(1.0)",
        ///        "destinationDataspace": "staging",
        ///        "destinationDataflow": "OECD:DF_MEI(1.0)"
        ///     }
        ///     
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">Transfer task successfully submitted</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>    
        [HttpPost]
        [MapToApiVersion("1.2")]
        [MapToApiVersion("2")]
        [Route("transfer/dataflow")]
        public async Task<ActionResult<OperationResult>> TransferDataflow(
            [FromForm, Required] string sourceDataspace,
            [FromForm, Required] string sourceDataflow,
            [FromForm] string sourceQuery,
            [FromForm, Required] string destinationDataspace,
            [FromForm, Required] string destinationDataflow,
            [FromForm] string lang,
            [FromForm] TargetVersion? sourceVersion,
            [FromForm] TargetVersion? targetVersion,
            [FromForm] string PITReleaseDate,
            [FromForm] ImportValidationType? validationType,
            [FromForm] string updatedAfter,
            [FromForm] TransferType? transferContent = TransferType.DataAndMetadata,
            [FromForm] bool? restorationOptionRequired = false
        )
        {
            var cancellationToken = CancellationToken.None;
            if (validationType == null)
                validationType = ImportValidationType.ImportWithBasicValidation;

            var importValidationType = validationType == ImportValidationType.ImportWithBasicValidation
                ? ValidationType.ImportWithBasicValidation
                : ValidationType.ImportWithFullValidation;

            return await TransferDataflow(sourceDataspace, sourceDataflow, sourceQuery, destinationDataspace, destinationDataflow,
                lang, targetVersion, sourceVersion, PITReleaseDate, restorationOptionRequired?? false, 
                importValidationType, transferContent?? TransferType.DataAndMetadata, updatedAfter, cancellationToken);

        }

        /// <summary>
        /// Validates a transfer of data and/or referential metadata from one source dataspace to a destination dataspace. 
        /// </summary>
        /// <param name="sourceDataspace">The ID of the source dataspace. <br></br> Example: <code>design</code></param>
        /// <param name="sourceDataflow">The SDMX ID of the source dataflow. <br></br> Format: 'AGENCYID:DATAFLOWID(VERSION)'. <br></br> Example: <code>OECD:DF_MEI(1.0)</code></param>
        /// <param name="sourceQuery">Optional - SDMX query to filter the data/referential metadata to be validated (as provided to the NSIWS to get data) <br></br> Example: <code><![CDATA["D.NOK.EUR.SP00.A?startPeriod=2012-01-01&endPeriod=2010-02-01"]]></code></param>
        /// <param name="destinationDataspace">The ID of the destination dataspace. <br></br> Example: <code>staging</code></param>
        /// <param name="destinationDataflow">The SDMX ID of the destination dataflow. <br></br> Format: 'AGENCYID:DATAFLOWID(VERSION)'. <br></br> Example: <code>OECD:DF_MEI(1.0)</code></param>
        /// <param name="lang">Optional - Two letter (ISO Alpha-2) language code to produce the response message/email<br></br>Example: <code>en</code></param>
        /// <param name="sourceVersion">Optional - The version of the data and referential metadata to be used from the <b>source</b> dataspace. <br></br>Possible values: Live (0), PIT (1), Default(Live)</param>
        /// <param name="targetVersion">Optional - The version of the data and referential metadata to be used to the <b>destination</b> dataspace. <br></br>Possible values: Live (0), Point in Time PIT (1), Default (Live)</param>
        /// <param name="updatedAfter">Optional - Only the data that has been changed after this  date (YYYY-MM-DD hh:mm:ss) will be transfered Example: <code>2022-06-04 10:16:01</code>, Default (all data regardless of when it was changed)</param>
        /// <param name="transferContent">Optional - The content to be validated across dataspaces: <br></br>Possible values: Transfer <b>both data and referential metadata (0)</b>, Transfer <b>data only (1)</b>, Transfer <b>referential metadata only (2)</b>, Default (Transfer both data and referential metadata)</param>
        /// <remarks>
        /// Example: Validate a transfer request
        /// 
        ///     POST /validate/transferDataflow
        ///     {
        ///        "sourceDataspace": "design",
        ///        "sourceDataflow": "OECD:DF_MEI(1.0)",
        ///        "destinationDataspace": "staging",
        ///        "destinationDataflow": "OECD:DF_MEI(1.0)"
        ///     }
        ///     
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">The transfer validation task successfully submitted</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>    
        [HttpPost]
        [MapToApiVersion("1.2")]
        [MapToApiVersion("2")]
        [Route("validate/transferDataflow")]
        public async Task<ActionResult<OperationResult>> ValidateTransferDataflow(
            [FromForm, Required] string sourceDataspace,
            [FromForm, Required] string sourceDataflow,
            [FromForm] string sourceQuery,
            [FromForm, Required] string destinationDataspace,
            [FromForm, Required] string destinationDataflow,
            [FromForm] string lang,
            [FromForm] TargetVersion? sourceVersion,
            [FromForm] TargetVersion? targetVersion,
            [FromForm] string updatedAfter,
            [FromForm] TransferType? transferContent = TransferType.DataAndMetadata
        )
        {
            var cancellationToken = CancellationToken.None;
            return await TransferDataflow(sourceDataspace, sourceDataflow, sourceQuery, destinationDataspace, destinationDataflow,
                lang, targetVersion, sourceVersion, null, false, ValidationType.FullValidationOnly, 
                transferContent?? TransferType.DataAndMetadata, updatedAfter, cancellationToken);

        }
        
        private async Task<ActionResult<OperationResult>> TransferDataflow(string sourceDataspace, string sourceDataflow,
            string sourceQuery, string destinationDataspace, string destinationDataflow, string lang,
            TargetVersion? targetVersion, TargetVersion? sourceVersion, string PITReleaseDate,
            bool restorationOptionRequired, ValidationType validationType, TransferType transferType, string updateAfter, CancellationToken cancellationToken)
        {
            //set LIVE version as default
            if (targetVersion == null)
                targetVersion = TargetVersion.Live;

            if (targetVersion != TargetVersion.PointInTime)
            {
                PITReleaseDate = null;
                restorationOptionRequired = false;
            }

            // Set sourceVersion to Live by default
            if (sourceVersion == null)
                sourceVersion = TargetVersion.Live;

            var language = lang ?? _configuration.DefaultLanguageCode;
            var sourceSpace = sourceDataspace.GetSpaceInternal(_configuration, language);
            var destinationSpace = destinationDataspace.GetSpaceInternal(_configuration, language);
            
            var transferParam = new SqlToSqlTransferParam()
            {
                TransactionType = validationType == ValidationType.FullValidationOnly
                    ? TransactionType.ValidateTransfer
                    : TransactionType.Transfer,
                SourceDataspace = sourceSpace,
                SourceDataflow = sourceDataflow.GetDataflow(language),
                DestinationDataspace = destinationSpace,
                DestinationDataflow = destinationDataflow.GetDataflow(language),
                SourceQuery = sourceQuery,
                CultureInfo = CultureInfo.GetCultureInfo(lang ?? _configuration.DefaultLanguageCode),
                TargetVersion = (TargetVersion)targetVersion,
                SourceVersion = (TargetVersion)sourceVersion,
                PITReleaseDate = PITReleaseDate.GetPITReleaseDate(language),
                PITRestorationAllowed = restorationOptionRequired,
                ValidationType = validationType,
                TransferType = transferType,
                UpdatedAfter = updateAfter.GetSdmxDateCore(nameof(updateAfter), language)
        };

            return await DoTransfer(transferParam, cancellationToken);

        }
        private async Task<ActionResult<OperationResult>> DoTransfer(SqlToSqlTransferParam transferParam, CancellationToken cancellationToken)
        {
            try {

                transferParam.Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping);

                var dataflow = ((IDataflowManager<SqlToSqlTransferParam>)_transferMngrSql.Consumer).GetDataflow(transferParam);

                if (dataflow == null)
                {
                    var template = GetLocalisedResource(Localization.ResourceId.DataflowNotFoundInDataspace, transferParam.CultureInfo.TwoLetterISOLanguageName);

                    var errorMessage = string.Format(
                        template,
                        transferParam.DestinationDataflow.AgencyId,
                        transferParam.DestinationDataflow.Id,
                        transferParam.DestinationDataflow.Version,
                        transferParam.DestinationDataspace.Id
                    );

                    return new BadRequestObjectResult(new OperationResult(false, errorMessage));
                }

                //Check if the user can read from the source dataspace
                if (!_transferMngrSql.Producer.IsAuthorized(transferParam, dataflow))
                {
                    var template = GetLocalisedResource(Localization.ResourceId.UnauthorizedImport, transferParam.CultureInfo.TwoLetterISOLanguageName);
                    var errorMessage = string.Format(
                        template,
                        transferParam.DestinationDataflow.FullId(),
                        transferParam.SourceDataspace.Id);

                    return StatusCode(StatusCodes.Status403Forbidden, new OperationResult(false, errorMessage));
                }

                //Check if the user can insert, update or delete to the destination dataspace
                if (!_transferMngrSql.Consumer.IsAuthorized(transferParam, dataflow))
                {
                    var template = GetLocalisedResource(Localization.ResourceId.UnauthorizedImport, transferParam.CultureInfo.TwoLetterISOLanguageName);
                    var errorMessage = string.Format(
                        template,
                        transferParam.DestinationDataflow.FullId(),
                        transferParam.DestinationDataspace.Id);

                    return StatusCode(StatusCodes.Status403Forbidden, new OperationResult(false, errorMessage));
                }

                var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace.Id);
                var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace.Id);
                transferParam.Id = await unitOfWork.TransactionRepository.GetNextTransactionId(cancellationToken);

                LogHelper.RecordNewTransaction(transferParam.Id, transferParam.DestinationDataspace);

                //Set data source information for email message
                var dataSource = string.IsNullOrEmpty(transferParam.SourceQuery) ?
                    GetLocalisedResource(
                        Localization.ResourceId.EmailSummaryDataSourceTransactionNoQuery,
                        transferParam.CultureInfo.TwoLetterISOLanguageName)
                    : string.Format(
                        format: GetLocalisedResource(
                            Localization.ResourceId.EmailSummaryDataSourceTransactionQuery,
                            transferParam.CultureInfo.TwoLetterISOLanguageName), arg0: transferParam.SourceQuery);
                transferParam.DataSource = dataSource;

                var transactionResult = new TransactionResult()
                {
                    TransactionType = transferParam.TransactionType,
                    TransactionId = transferParam.Id,
                    DataSource = dataSource,
                    SourceDataSpaceId = transferParam.SourceDataspace?.Id,
                    DestinationDataSpaceId = transferParam.DestinationDataspace?.Id,
                    Aftefact = transferParam.DestinationDataflow.FullId(),
                    User = transferParam.Principal.Email,
                    TransactionStatus = TransactionStatus.Queued
                };

                var message = string.Format(
                    GetLocalisedResource(
                        Localization.ResourceId.SubmissionResult,
                        transferParam.CultureInfo.TwoLetterISOLanguageName),
                    transferParam.Id);

                Log.Notice(message);

                var transaction = await unitOfWork.TransactionRepository.CreateTransactionItem(
                    transferParam.Id, dataflow.FullId, transferParam.Principal,
                    transferParam?.SourceDataspace?.Id, transferParam?.DataSource,
                    transferParam.TransactionType, transferParam.TargetVersion, cancellationToken);

                // ------------------------------------------------

                //Function with the main steps to execute the request
                async Task mainTask(CancellationToken backgroundCancellationToken)
                {
                    using var internalCancellationToken = backgroundCancellationToken.SetBackgroundTaskCompletionSteps(dotStatDbService, transferParam, cancellationToken);

                    try
                    {
                        await _transferMngrSql.Transfer(transferParam, transaction, internalCancellationToken.Token);
                    }
                    catch (DotStatException exception)
                    {
                        Log.Warn(GetLocalisedResource(Localization.ResourceId.NoObservationsProcessed, transferParam.CultureInfo.TwoLetterISOLanguageName));
                        Log.Error(exception);
                    }
                    catch (Exception e)
                    {
                        if (e is not TaskCanceledException && !internalCancellationToken.Token.IsCancellationRequested)
                        {
                            Log.Error(GetLocalisedResource(Localization.ResourceId.NoObservationsProcessed, transferParam.CultureInfo.TwoLetterISOLanguageName));
                            Log.Fatal(e);
                        }
                    }
                    finally
                    {
                        transactionResult.TransactionStatus = await dotStatDbService.GetFinalTransactionStatus(transferParam.Id, CancellationToken.None);

                        await _mailService.SendMail(
                            transactionResult,
                            transferParam.CultureInfo.TwoLetterISOLanguageName,
                            _authorizationManagement.IsAuthorized(transferParam.Principal, PermissionType.AdminRole)
                        );
                    }

                    await Task.CompletedTask;
                }

                //Delegate to check if this request can begin to process, or if it should be placed back to the queue.
                async Task<bool> canBeProcessed(CancellationToken backgroundCancellationToken)
                {
                    Log.SetTransactionId(transferParam.Id);
                    Log.SetDataspaceId(transferParam.DestinationDataspace.Id);
                    return (await dotStatDbService.TryNewTransaction(transaction, dataflow, transferParam.Principal, _mappingStoreDataAccess, backgroundCancellationToken, onlyLockTransaction: true)).Success;
                }

                //Function to execute to notify/log that the request is still in progress.
                async Task notifyInProgressTask(CancellationToken backgroundCancellationToken) => await backgroundCancellationToken.SetStillProcessingLogging(transferParam);

                //Action to execute when the application is shutting down
                async void callBackAction() => await unitOfWork.TransactionRepository.MarkTransactionAsCanceled(transferParam.Id);

                _backgroundQueue.Enqueue(new TransactionQueueItem(canBeProcessed, mainTask, notifyInProgressTask, callBackAction));

                return new OperationResult(true, message);
            }
            catch (Exception exception)
            {
                Log.Fatal(exception);

                throw;
            }
        }
    }
}
