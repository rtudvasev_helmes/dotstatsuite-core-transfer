using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Domain;
using DotStat.Transfer.Interface;
using DotStat.Transfer.Utils;
using log4net.Core;
using static DotStat.Common.Localization.LocalizationRepository;

namespace DotStatServices.Transfer.Services
{
    /// <summary>
    /// MailService class.
    /// </summary>
    public class MailService : IMailService
    {
        private readonly IMailConfiguration _configuration;

        /// <summary>
        /// MailService constructor.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public MailService(IMailConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Sends the mail.
        /// </summary>
        /// <param name="transferResult">The transfer result.</param>
        /// <param name="languageCode">The language code.</param>
        /// <param name="isAdmin">The is admin flag.</param>
        public async Task SendMail(TransactionResult transactionResult, string languageCode, bool isAdmin)
        {
            if (string.IsNullOrEmpty(transactionResult.User))
            {
                return;
            }

            try
            {
                var transactionMessages = new StringBuilder();
                var hasWarnings = false;
                var hasErrors = false;

                var transactionLogs = isAdmin
                    ? transactionResult.TransactionLogs
                    : transactionResult.TransactionLogs.Where(x => x.Level != Level.Fatal);

                foreach (var transactionLog in transactionLogs)
                {
                    string row;
                    if (transactionLog.Level == Level.Error || transactionLog.Level == Level.Fatal)
                    {
                        hasErrors = true;
                        row = $"<tr style=\"color:red;font-weight:bold;\"><td>{transactionLog.TimeStamp}</td><td>{transactionLog.Level}</td><td><pre>{transactionLog.RenderedMessage}</pre></td></tr>";
                    }
                    else if (transactionLog.Level == Level.Warn)
                    {
                        hasWarnings = true;
                        row = $"<tr style=\"color:orange;font-weight:bold;\"><td>{transactionLog.TimeStamp}</td><td>{transactionLog.Level}</td><td><pre>{transactionLog.RenderedMessage}</pre></td></tr>";
                    }
                    else { 
                        row = $"<tr><td>{transactionLog.TimeStamp}</td><td>{transactionLog.Level}</td><td><pre>{transactionLog.RenderedMessage}</pre></td></tr>";
                    }

                    transactionMessages.Append(row);
                }

                var emailSummarySourceDataSpace = transactionResult.SourceDataSpaceId != null
                    ? string.Format(GetLocalisedResource(
                        Localization.ResourceId.EmailSummarySourceDataSpace, languageCode), transactionResult.SourceDataSpaceId)
                    : string.Empty;

                var status = string.Empty;
                var subjectStatus = transactionResult.TransactionStatus.ToString();
                switch (transactionResult.TransactionStatus)
                {
                    case TransactionStatus.InProgress:
                        status = $"<span style=\"color:orange;font-weight:bold;\">{TransactionStatus.InProgress}</span>";
                        break;
                    case TransactionStatus.Completed when !hasErrors && !hasWarnings:
                        status = $"<span style=\"color:green;font-weight:bold;\">{TransactionStatus.Completed}</span>";
                        subjectStatus = GetLocalisedResource(Localization.ResourceId.MailSuccessfullyCompleted, languageCode);
                        break;
                    case TransactionStatus.Completed when hasErrors:
                        status = $"<span style=\"color:red;font-weight:bold;\">{TransactionStatus.Completed}</span>";
                        subjectStatus = GetLocalisedResource(Localization.ResourceId.MailFailed, languageCode);
                        break;
                    case TransactionStatus.Completed when hasWarnings:
                        status = $"<span style=\"color:green;font-weight:bold;\">{TransactionStatus.Completed}</span>";
                        subjectStatus = GetLocalisedResource(Localization.ResourceId.MailCompletedWithWarnings, languageCode);
                        break;
                    case TransactionStatus.TimedOut:
                        subjectStatus = GetLocalisedResource(Localization.ResourceId.MailFailed, languageCode);
                        status = $"<span style=\"color:orange;font-weight:bold;\">{TransactionStatus.TimedOut}</span>";
                        break;
                    case TransactionStatus.Canceled:
                        subjectStatus = GetLocalisedResource(Localization.ResourceId.MailFailed, languageCode);
                        status = $"<span style=\"color:red;font-weight:bold;\">{TransactionStatus.Canceled}</span>";
                        break;
                    case TransactionStatus.Queued:
                        break;
                    default:
                        status = $"<span style=\"color:grey;font-weight:bold;\">{TransactionStatus.Queued}</span>";
                        break;
                }

                var subject = string.Format(
                    GetLocalisedResource(Localization.ResourceId.MailSubject, languageCode),
                    subjectStatus,
                    transactionResult.Aftefact,
                    transactionResult.DestinationDataSpaceId,
                    transactionResult.TransactionId);

                var summary = string.Format(
                    GetLocalisedResource(Localization.ResourceId.EmailSummary, languageCode),
                    transactionResult.TransactionId,
                    transactionResult.TransactionType,
                    emailSummarySourceDataSpace,
                    transactionResult.DataSource,
                    transactionResult.DestinationDataSpaceId,
                    transactionResult.Aftefact,
                    transactionResult.User,
                    status);

                var body = string.Format(
                    GetLocalisedResource(Localization.ResourceId.MailBody, languageCode), 
                    summary,
                    transactionMessages);

                using var mailMessage = new MailMessage(_configuration.MailFrom, transactionResult.User, subject, body)
                {
                    IsBodyHtml = true
                };

                using var smtpClient = new SmtpClient(_configuration.SmtpHost, _configuration.SmtpPort)
                {
                    EnableSsl = _configuration.SmtpEnableSsl
                };

                if (!string.IsNullOrEmpty(_configuration.SmtpUserName) &&
                    !string.IsNullOrEmpty(_configuration.SmtpUserPassword) &&
                    !_configuration.SmtpUserName.Equals("na", StringComparison.InvariantCultureIgnoreCase) &&
                    !_configuration.SmtpUserPassword.Equals("na", StringComparison.InvariantCultureIgnoreCase))
                {
                    smtpClient.Credentials = new NetworkCredential(_configuration.SmtpUserName, _configuration.SmtpUserPassword);
                }

                mailMessage.Headers.Add("Message-Id",
                                        String.Format("<{0}@{1}>",
                                        Guid.NewGuid().ToString(),
                                        _configuration.MailFrom.Substring(_configuration.MailFrom.LastIndexOf('@') + 1)));

                if (!string.IsNullOrEmpty(_configuration.SmtpHFrom))
                {
                    mailMessage.Headers.Add("HFrom",_configuration.SmtpHFrom);
                }

                await smtpClient.SendMailAsync(mailMessage);
                Log.Notice(GetLocalisedResource(Localization.ResourceId.MailSent, languageCode));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }
    }
}
