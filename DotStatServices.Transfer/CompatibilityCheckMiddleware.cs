﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.DB;
using DotStat.Db.DB;
using DotStat.DB.Repository;
using Microsoft.AspNetCore.Http;
using DotStat.Db.Service;

namespace DotStatServices.Transfer
{
    public class CompatibilityCheckMiddleware
    {
        private bool _isDbVersionChecked = false;
        private readonly RequestDelegate _next;
        private readonly IDataspaceConfiguration _config;
        private readonly UnitOfWorkResolver _unitOfWorkResolver;
        private readonly DotStatDbResolver _dotStatDbResolver;


        public CompatibilityCheckMiddleware(
            RequestDelegate next,
            IDataspaceConfiguration config,
            UnitOfWorkResolver unitOfWorkResolver,
            DotStatDbResolver dotStatDbResolver
            )
        {
            _next = next;
            _config = config;
            _unitOfWorkResolver = unitOfWorkResolver;
            _dotStatDbResolver = dotStatDbResolver;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            if (!_isDbVersionChecked)
            {
                foreach (var space in _config.SpacesInternal)
                {

                    var dotStatDb = _dotStatDbResolver(space.Id);
                    var currentDbVersion = await dotStatDb.GetCurrentDbVersion(CancellationToken.None);
                    var supportedDbVersion = dotStatDb.GetSupportedDbVersion();

                    if (!SupportedDatabaseVersion.IsDataDbVersionSupported(currentDbVersion, supportedDbVersion))
                    {
                        throw new NotSupportedException(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DatabaseVersionNotSupported),
                            space.Id,
                            currentDbVersion,
                            supportedDbVersion
                        ));
                    }

                    //this should be done by the health check
                    var unitOfWork = _unitOfWorkResolver(space.Id);
                    await unitOfWork.ArtefactRepository.CheckManagementTables(CancellationToken.None);
                }

                _isDbVersionChecked = true;
            }
            
            await _next(httpContext);
        }
    }
}
